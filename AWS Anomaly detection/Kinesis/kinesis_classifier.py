import boto3
import time
import json
from classification import classifier

threshold = 4

c = classifier()

kinesis = boto3.client('kinesis')

shard_id = 'shardId-000000000000' #we only have one shard!
shard_it = kinesis.get_shard_iterator(
    StreamName="ExampleOutputStream",
    ShardId=shard_id,
    ShardIteratorType="LATEST")['ShardIterator']
while True:
    out = kinesis.get_records(ShardIterator=shard_it, Limit=2)
    shard_it = out["NextShardIterator"]
    for o in out["Records"]:
        #print(o["Data"])
        jdat = json.loads(o["Data"].decode('utf-8'))
        print(jdat)
        if float(jdat["ANOMALY_SCORE"]) >= threshold:
            f = open("anomalies.txt", 'a')
            #f.write('\n'.join(jdat))
            json.dump(jdat, f)
            arr = [jdat['sp'], jdat['dip'], jdat['dp'], jdat['pck'], jdat['bytes']]
            arr = list(map(int, arr))
            f.write("\nclassification is {} at scan time {}/{} \n".format(c.classify(arr), time.strftime("%X"), time.time()))
            f.close()

    time.sleep(0.2)