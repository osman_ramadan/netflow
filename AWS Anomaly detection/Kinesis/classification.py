# Classification module, contains all the code that classifies an anomaly after being detected
import numpy as np
class classifier:
    'This a classifier class that is used to classify anomalies'
    # Uncomment for the manual version of the classification (the weights updated manually by me rather than using
    # gradient decent algorithm)
    # weights = np.matrix([[10, 20, -10, 0, 0], [10, -10, 40, 0, 0], [-100, -10, -10, 3, 0.1], [100, -10, -10, 2, 0.1]])
    weights = np.matrix([[0.5332, 2.8139, -2.4373, -0.1408, -1.1599], [0.0764, 0.2829, 4.6248, 0.3817, -1.8022],
    [-4.1203, -1.1329, -1.7679, 0.5574, 0.8253], [7.9314, -5.6233, -2.4124, -2.0481, 0.1598]])
    def classify(self, v):
        #result = self.weights * np.matrix(v).transpose()
        result = self.weights * np.log(np.matrix(v).transpose()) # The algorithms weight require getting the log first
        return 4 - result.tolist().index(max(result))



#print (classifier().classify([46975, 88454, 13, 244930, 46313316]))
# 1: TCP/SYN Flooding
# 2: ICMP Flooding
# 3: Port Scan
# 4: Network Scan