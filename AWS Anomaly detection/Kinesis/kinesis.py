import pymysql
import time
import json
import boto3

kinesis = boto3.client('kinesis')
# TODO to be configured later
start_ip = 184147968
end_ip = 184156159

def get_points(row):
    data = {}
    data['sip'] = str(row[0])
    data['sp'] = int(row[1])
    data['dip'] = int(row[2])
    data['dp'] = int(row[3])
    data['pck'] = int(row[4])
    data['bytes'] = int(row[5])
    return data


def get_data(table_name, st) :
    global start_ip, end_ip

    conn = get_connection()
    cur = conn.cursor()

    cur.execute("SELECT inet_ntoa(`sip`) as ip, count(distinct sp), count(distinct dip), count(distinct dp), sum(pck),"
                " sum(bytes) From "+ table_name + " where sip between "+ str(start_ip)+ " and "+ str(end_ip)
                +" and  st >= " + str(st) + " group by sip")
    no_rows = cur.rowcount
    delay = float(300)/max(no_rows, 1)
    print("The number of rows is {} and the delay is {}".format(no_rows, delay))
    #if no_rows == 0: time.sleep(delay)

    for row in cur:
        data = json.dumps(get_points(row))
        print(data)
        kinesis.put_record(
            StreamName="InputStream",
            Data=data,
            PartitionKey="osmanio")
        time.sleep(delay)
    cur.close()
    conn.close()

def get_connection():
    return pymysql.connect(
        host = 'netflow-manx-cluster.cluster-ce7qna0ycuht.eu-west-1.rds.amazonaws.com',
        port = 3306,
        user = 'admin',
        passwd = 'oZ6gffNV6}vN;3t+',
        db = 'nfx'
    )

timestamp = int(time.time()) - 5 * 60
while(True) :
    table_name = "h1_" + time.strftime("%y%m%d%H", time.gmtime(time.time() + 3600))
    #table_name = "h1_16091513"

    print("Starting to log for table " + table_name + " time " + str(timestamp))
    #time.sleep(60)
    get_data(table_name, timestamp)
    timestamp += 5*60
    time.sleep(max(0, timestamp - (int(time.time()) - 5 * 60)))



