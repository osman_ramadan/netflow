# Get the queries ready
# Network scan 10.248.146.216/184062680, 2016/09/22 16:14
# Port scan 80.87.205.247/1347931639 to 10.248.144.56/184062008, 2016/07/16 03:23
# Port scan 10.240.117.99/183530851, 2016/09/22 14:05
# ICMP flooding 192.168.140.178/3232271538 to 10.252.8.8/184289288, 2016/08/19 10:24
# TCP/SYN flooding 10.254.77.173/184438189 to 192.168.79.163/3232255907, 2016/08/05 19:24
# Features are: sp, distinct dip and dp, sum pck and bytes in 5 minutes


import tensorflow as tf
import time
import numpy as np
import pymysql
# Data sets
IRIS_TRAINING = "netflowdata4.csv"
weights = np.matrix([[10, 20, -10, 0, 0], [10, -10, 40, 0, 0], [-100, -10, -10, 3, 0.1], [100, -10, -10, 2, 0.1]])


def classify(v):
    global weights
    result = weights * np.matrix(v).transpose()
    return result.tolist().index(max(result)) + 1

# Load datasets.
training_set = tf.contrib.learn.datasets.base.load_csv_without_header(filename=IRIS_TRAINING,
                                                       features_dtype=np.int,
                                                       target_dtype=np.int,
                                                       target_column=5)

# Specify that all features have real-value data
feature_columns = [tf.contrib.layers.real_valued_column("", dimension=5)]

# Build 3 layer DNN with 10, 20, 10 units respectively.
classifier = tf.contrib.learn.DNNClassifier(feature_columns=feature_columns,
                                            hidden_units=[3],
                                            n_classes=2,
                                            model_dir="/tmp/netflow_model")
# Fit model.
classifier.fit(x=training_set.data,
               y=training_set.target,
               steps=10000)

# Evaluate accuracy.
accuracy_score = classifier.evaluate(x=training_set.data,
                                     y=training_set.target)["accuracy"]
print('Accuracy: {0:f}'.format(accuracy_score))

# Classify two new flower samples.


def get_connection():
    return pymysql.connect(
        host = 'netflow-manx-cluster.cluster-ce7qna0ycuht.eu-west-1.rds.amazonaws.com',
        port = 3306,
        user = 'admin',
        passwd = 'oZ6gffNV6}vN;3t+',
        db = 'nfx'
    )
def get_data(table_name, st) :

    conn = get_connection()
    cur = conn.cursor()

    cur.execute("SELECT inet_ntoa(`sip`) as ip, count(distinct sp), count(distinct dip), count(distinct dp), sum(pck),"
                " sum(bytes) From "+ table_name + " where sip between 167772160 and 184549375 and  st >= " + str(st) +
                " group by sip")
    no_rows = cur.rowcount
    delay = float(300)/max(no_rows, 1)
    print("The number of rows is {} and the delay is {}".format(no_rows, delay))
    #if no_rows == 0: time.sleep(delay)

    for row in cur:
        print(row)
        new_samples = np.array([row[1:]], dtype=int)
        y = classifier.predict(new_samples)
        print('Predictions: {}'.format(str(y)))

    cur.close()
    conn.close()

timestamp = int(time.time()) - 5 * 60
while(True) :
    table_name = "h1_" + time.strftime("%y%m%d%H", time.gmtime(time.time() + 3600))
    #table_name = "h1_16091513"

    print("Starting to log for table " + table_name + " time " + str(timestamp))
    #time.sleep(60)
    get_data(table_name, timestamp)
    timestamp += 5*60
    time.sleep(max(0, timestamp - (int(time.time()) - 5 * 60)))
