/**
 * 
 */
package netflow.data.out;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.mllib.linalg.Vector;

import netflow.business.logic.MainApplication.MODULES;
import netflow.data.out.entities.Anomaly;
import scala.Tuple2;

/**
 * 
 * A helper class to manage storing data to the output database
 * @author Osman
 *
 */
public class OutputRepositoryHelper implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9021872514927786847L;

	/**
	 * Create a list of anomalies given the anomalous ips and the labelled data
	 * @param labelData in a form of value : label
	 * @param data the original data in a form of ip : values
	 * @param anomalies in a form of ip : times
	 * @param hourly_table the name of the hourly table
	 * @param module the detection module used to detect these anomalies
	 * @return a list of type Anomaly
	 */
	public static List<Anomaly> createAnomalies(JavaPairRDD<String, Iterable<Vector>> labelData,
			JavaPairRDD<Long, Vector> data, final Map<Long, long[]> anomalies,
			final String hourly_table, final MODULES module) {


		// Get the scan time
		final long scan_time = new Date().getTime()/1000;

		// Filter the data to select only anomalies
		final Map<Vector, Long> filteredData = data.filter((Tuple2<Long, Vector> val) -> anomalies.containsKey(val._1))
				.mapToPair((Tuple2<Long, Vector> t) -> t.swap()).collectAsMap();

		// Perform some filtering and mapping to transform the labelled data to list of type anomalies
		return labelData.filter((Tuple2<String, Iterable<Vector>> val) -> {

			return !val._1.equals("normal");

		}).flatMap((Tuple2<String, Iterable<Vector>> val) -> {

			String type = val._1;
			
			if (module == MODULES.VOLUMEANALYSIS) type = "traffic anomaly";
			
			if (module == MODULES.FLOODING) type = "Flooding attack";

			List<Anomaly> list = new ArrayList<>();

			for (Vector v : val._2) {
				if (!filteredData.containsKey(v)) continue;
				long ip = filteredData.get(v);
				long[] times = anomalies.get(ip);
				Anomaly anomaly = new Anomaly();
				anomaly.setHourly_table(hourly_table);
				anomaly.setScan_time(scan_time);
				anomaly.setSip(ip);
				anomaly.setType(type);
				anomaly.setStatus("Warning");
				anomaly.setVerified((short) 0);
				anomaly.setStart_time_signal(times[3]);
				anomaly.setStart_time_spc(times[0]);
				anomaly.setStart_time_improved(times[1]);
				anomaly.setStart_time_standard(times[2]);

				list.add(anomaly);
			}
			return list.iterator();
		}).collect();

	}

}
