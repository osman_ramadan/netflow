/**
 * 
 */
package netflow.data.out;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import netflow.business.logic.ConfigurationManager;
import netflow.data.out.entities.Anomaly;
import netflow.data.out.entities.ModelMetrics;
import netflow.data.out.entities.Task;


/**
 * 
 * This is a repository class to store data analysis output in the database
 * Implemented using Singleton design pattern
 * @author Osman
 *
 */
public class OutputRepository implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3080854467517147279L;
	private static OutputRepository instance = new OutputRepository();

	private Connection connector;


	/**
	 * Default constructor to establish a connection to MySQL database
	 */
	private OutputRepository() {
		updateConnection();
	}

	/**
	 * Get the only object available
	 * @return OutputRepository object
	 */
	public static OutputRepository getInstance() {
		return instance;
	}

	/**
	 * Load all the available anomalies from the analytics database
	 * @return a List of type anomalies
	 * @throws SQLException can throw a SQL exception
	 */
	public List<Anomaly> load()throws SQLException{

		return load("");

	}

	/**
	 * Update the connection with the database
	 */
	private void updateConnection() {
		// Initialise a database connection
		try {
			connector = DriverManager.getConnection("jdbc:mysql://" + 
					ConfigurationManager.getProperty("netflow.output.database.url"),
					ConfigurationManager.getProperty("netflow.nfx.username"),
					ConfigurationManager.getProperty("netflow.nfx.password"));
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Load all the available anomalies from the analytics database with some exceptions 
	 * specified by a SQL clause
	 * @param clause the SQL clause that specific the exception e.g. Where verified = 1
	 * @return a List of type anomalies
	 * @throws SQLException can throw a SQL exception
	 */
	public List<Anomaly> load(String clause)throws SQLException{

		updateConnection();

		List<Anomaly> anomalies = new ArrayList<>();

		try (Statement statement = connector.createStatement(); ) {

			// Execute the following query
			ResultSet rs = statement.executeQuery("SELECT * FROM anomalies " + clause);

			while (rs.next()) {
				Anomaly anomaly = new Anomaly();
				anomaly.setScan_time(rs.getLong("scan_time"));
				anomaly.setHourly_table(rs.getString("hourly_table"));
				anomaly.setSip(rs.getLong("sip"));
				anomaly.setType(rs.getString("type"));
				anomaly.setStatus(rs.getString("status"));
				anomaly.setStart_time_signal(rs.getLong("start_time_signal"));
				anomaly.setStart_time_spc(rs.getLong("start_time_spc"));
				anomaly.setStart_time_improved(rs.getLong("start_time_improved"));
				anomaly.setStart_time_standard(rs.getLong("start_time_standard"));
				anomaly.setVerified(rs.getShort("verified"));
				anomalies.add(anomaly);
			}
		}

		connector.close();

		return anomalies;
	}

	/**
	 * Save the following anomalies in the database
	 * @param anomalies a list of type anomalies to be saved in the database
	 * @throws SQLException can throw a SQL exception
	 */
	public void save (List<Anomaly> anomalies) throws SQLException {

		updateConnection();

		try (
				PreparedStatement statement = connector.prepareStatement("INSERT INTO anomalies (scan_time,"
						+ " hourly_table, sip, type, status, start_time_signal,"
						+ " start_time_spc, start_time_improved, start_time_standard, verified)"
						+ " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
				) {

			int i = 0;
			for (Anomaly anomaly : anomalies) {
				statement.setLong(1, anomaly.getScan_time());
				statement.setString(2, anomaly.getHourly_table());
				statement.setLong(3, anomaly.getSip());
				statement.setString(4, anomaly.getType());
				statement.setString(5, anomaly.getStatus());
				statement.setLong(6, anomaly.getStart_time_signal());
				statement.setLong(7, anomaly.getStart_time_spc());
				statement.setLong(8, anomaly.getStart_time_improved());
				statement.setLong(9, anomaly.getStart_time_standard());
				statement.setShort(10, anomaly.getVerified());

				statement.addBatch();

				i++;

				if (i % 1000 == 0 || i == anomalies.size()) {
					statement.executeBatch(); // Execute every 1000 items.
				}
			}
		}

		connector.close();
	}

	/**
	 * Save the following metrics in model_metrics table in analytics database
	 * @param metrics to be stored in the database
	 * @throws SQLException can throw a SQL exception
	 */
	public void save(ModelMetrics metrics) throws SQLException {

		updateConnection();

		try (
				PreparedStatement statement = connector.prepareStatement("INSERT INTO model_metrics (day,"
						+ " true_metrics, clustering_metrics) VALUES (?, ?, ?)");
				) {

			statement.setString(1, metrics.getDay());
			statement.setFloat(2, metrics.getTrue_metrics());
			statement.setFloat(3, metrics.getClustering_metrics());

			statement.execute();
		}

		connector.close();
	}

	/**
	 * Save the following task in task_performance table in analytics database
	 * @param task to be stored in the database
	 * @throws SQLException can throw a SQL exception
	 */
	public void save(Task task) throws SQLException {

		updateConnection();

		try (
				PreparedStatement statement = connector.prepareStatement("INSERT INTO task_performance (task_name,"
						+ " start_time, end_time, duration) VALUES (?, ?, ?, ?)");
				) {

			statement.setString(1, task.getTask_name());
			statement.setLong(2, task.getStart_time());
			statement.setLong(3, task.getEnd_time());
			statement.setLong(4, task.getDuration());

			statement.execute();
		}
		connector.close();
	}
}
