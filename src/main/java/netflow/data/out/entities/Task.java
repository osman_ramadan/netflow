/**
 * 
 */
package netflow.data.out.entities;

/**
 * 
 * This entity represent the task_performance table in the analytics database
 * The contains information about running time of each task
 * @author Osman
 *
 */
public class Task {
	
	private String task_name;
	private long start_time;
	private long end_time;
	private long duration;
	
	/**
	 * Overriding the default construct to pass the parameters
	 * @param task_name the task name
	 * @param start_time the start time
	 * @param end_time the end time
	 */
	public Task(String task_name, long start_time, long end_time) {
		this.task_name = task_name;
		this.start_time = start_time;
		this.end_time = end_time;
		this.duration = end_time - start_time;
	}
	
	/**
	 * @return the task_name
	 */
	public String getTask_name() {
		return task_name;
	}
	/**
	 * @param task_name the task_name to set
	 */
	public void setTask_name(String task_name) {
		this.task_name = task_name;
	}
	/**
	 * @return the start_time
	 */
	public long getStart_time() {
		return start_time;
	}
	/**
	 * @param start_time the start_time to set
	 */
	public void setStart_time(long start_time) {
		this.start_time = start_time;
	}
	/**
	 * @return the end_time
	 */
	public long getEnd_time() {
		return end_time;
	}
	/**
	 * @param end_time the end_time to set
	 */
	public void setEnd_time(long end_time) {
		this.end_time = end_time;
	}
	/**
	 * @return the duration
	 */
	public long getDuration() {
		return duration;
	}
	/**
	 * @param duration the duration to set
	 */
	public void setDuration(long duration) {
		this.duration = duration;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		
		return "[task_name=" + task_name + ", start_time=" + start_time + ", end_time=" + end_time + ", duration="
				+ duration + "]";
	}

}
