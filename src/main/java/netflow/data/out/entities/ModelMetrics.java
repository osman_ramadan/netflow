/**
 * 
 */
package netflow.data.out.entities;

import java.io.Serializable;

/**
 * An entity that holds values to be stored in the model_metrics table in the analytics database
 * 
 * @author Osman
 *
 */
public class ModelMetrics implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7105217271536559342L;
	
	private String day;
	private float true_metrics;
	private float clustering_metrics;
	
	/**
	 * Create a model metrics
	 * @param day of the model
	 * @param true_metrics calculated from verified data
	 * @param clustering_metrics calculated from unsupervisied learning/Clustering
	 */
	public ModelMetrics(String day, double true_metrics, double clustering_metrics) {
		this.day = day;
		this.true_metrics = new Double(true_metrics).floatValue();
		this.clustering_metrics = new Double(clustering_metrics).floatValue();
	}
	
	/**
	 * @return the day
	 */
	public String getDay() {
		return day;
	}
	/**
	 * @param day the day to set
	 */
	public void setDay(String day) {
		this.day = day;
	}
	/**
	 * @return the true_metrics
	 */
	public float getTrue_metrics() {
		return true_metrics;
	}
	/**
	 * @param true_metrics the true_metrics to set
	 */
	public void setTrue_metrics(float true_metrics) {
		this.true_metrics = true_metrics;
	}
	/**
	 * @return the clustering_metrics
	 */
	public float getClustering_metrics() {
		return clustering_metrics;
	}
	/**
	 * @param clustering_metrics the clustering_metrics to set
	 */
	public void setClustering_metrics(float clustering_metrics) {
		this.clustering_metrics = clustering_metrics;
	}

}
