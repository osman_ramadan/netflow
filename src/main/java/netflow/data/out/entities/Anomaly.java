/**
 * 
 */
package netflow.data.out.entities;

import java.io.Serializable;

/**
 * 
 * This class represent an entity to be stored in the database
 * @author Osman
 *
 */
public class Anomaly implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8423805443731328703L;
	private long scan_time;
	private String hourly_table;
	private long sip;
	private String type;
	private String status;
	private long start_time_signal;
	private long start_time_spc;
	private long start_time_improved;
	private long start_time_standard;
	private short verified;
	
	
	/**
	 * @return the scan_time
	 */
	public long getScan_time() {
		return scan_time;
	}
	/**
	 * @param scan_time the scan_time to set
	 */
	public void setScan_time(long scan_time) {
		this.scan_time = scan_time;
	}
	/**
	 * @return the hourly_table
	 */
	public String getHourly_table() {
		return hourly_table;
	}
	/**
	 * @param hourly_table the hourly_table to set
	 */
	public void setHourly_table(String hourly_table) {
		this.hourly_table = hourly_table;
	}
	/**
	 * @return the sip
	 */
	public long getSip() {
		return sip;
	}
	/**
	 * @param sip the sip to set
	 */
	public void setSip(long sip) {
		this.sip = sip;
	}
	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @return the occurrence_time
	 */
	public long getStart_time_signal() {
		return start_time_signal;
	}
	/**
	 * @param start_time_signal the start_time_signal to set
	 */
	public void setStart_time_signal(long start_time_signal) {
		this.start_time_signal = start_time_signal;
	}
	/**
	 * @return the verified
	 */
	public short getVerified() {
		return verified;
	}
	/**
	 * @param verified the verified to set
	 */
	public void setVerified(short verified) {
		this.verified = verified;
	}
	/**
	 * @return the start_time_spc
	 */
	public long getStart_time_spc() {
		return start_time_spc;
	}
	/**
	 * @param start_time_spc the start_time_spc to set
	 */
	public void setStart_time_spc(long start_time_spc) {
		this.start_time_spc = start_time_spc;
	}
	/**
	 * @return the start_time_improved
	 */
	public long getStart_time_improved() {
		return start_time_improved;
	}
	/**
	 * @param start_time_improved the start_time_improved to set
	 */
	public void setStart_time_improved(long start_time_improved) {
		this.start_time_improved = start_time_improved;
	}
	/**
	 * @return the start_time_standard
	 */
	public long getStart_time_standard() {
		return start_time_standard;
	}
	/**
	 * @param start_time_standard the start_time_standard to set
	 */
	public void setStart_time_standard(long start_time_standard) {
		this.start_time_standard = start_time_standard;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String txt = "(scan_time=" + this.scan_time + ", hourly_table=" + this.hourly_table + ", sip=" + this.sip
				+ ", type=" + this.type + ", status=" + this.status + ", start_time_signal=" + this.start_time_signal
				+ ", start_time_spc=" + this.start_time_spc + ", start_time_improved=" + this.start_time_improved 
				+ ", start_time_standard=" + this.start_time_standard + ", verified=" + this.verified + ")\n"; 
		return txt;
	}

}
