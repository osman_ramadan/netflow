/**
 * 
 */
package netflow.data.analysis;

import java.io.File;
import java.io.Serializable;
import java.util.Map;

import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.mllib.classification.LogisticRegressionModel;
import org.apache.spark.mllib.classification.LogisticRegressionWithLBFGS;
import org.apache.spark.mllib.evaluation.MulticlassMetrics;
import org.apache.spark.mllib.linalg.Vector;
import org.apache.spark.mllib.regression.LabeledPoint;

import netflow.business.logic.MainApplication;
import netflow.data.analysis.KMeansClustering.CLASSES;
import netflow.data.in.DataProcessing;
import scala.Tuple2;

/**
 * This class is for training labelled data to create a classifier model
 * The model can also be updated, saved and loaded from a local file
 * The algorithms using for supervised learning is Multiclass logistic regression see Spark documentation
 * 
 * 
 * @author Osman
 *
 */
public class MultiClassClassification implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3605942503963078428L;
	private LogisticRegressionModel model;
	private JavaRDD<LabeledPoint> training;
	private JavaRDD<LabeledPoint> test;
	private JavaRDD<Tuple2<Object, Object>> predictionAndLabels;
	private Map<Vector, Long> anomalies;
	
	private File modelPath =  new File("./resource/classifierModel");

	/**
	 * The constructor 
	 * @param data the label data set
	 * @param numClasses the number of classes of the model
	 */
	public MultiClassClassification(JavaRDD<LabeledPoint> data, int numClasses) {
		trainModel(data, numClasses);
	}

	/**
	 * Default constructor
	 */
	public MultiClassClassification() {

	}

	/**
	 * Train the classifier model
	 * @param data that is used to train the model. It must be labelled
	 * @param numClasses the number of different classes of the classifier model
	 */
	public void trainModel(JavaRDD<LabeledPoint> data, int numClasses) {

		// Split initial RDD into two... [90% training data, 10% testing data].
		JavaRDD<LabeledPoint>[] splits = data.randomSplit(new double[] {0.9, 0.1}, 11L);
		training = splits[0].cache();
		test = splits[1];

		// Run training algorithm to build the model.
		if (model == null) {
			model = new LogisticRegressionWithLBFGS()
					.setNumClasses(numClasses)
					.run(training.rdd());
			
		// Update the classifier model if there is one	
		} else {
			model = new LogisticRegressionWithLBFGS()
					.setNumClasses(numClasses)
					.run(training.rdd(), model.weights());
		}


	}

	/**
	 * Compute raw scores on the test set and get evaluation metrics.
	 * @return the accuracy of the model using F1 score
	 */
	public double evaluateModel() {
		predictionAndLabels = test.map((LabeledPoint p) -> {
			Double prediction = model.predict(p.features());
			return new Tuple2<Object, Object>(prediction, p.label());
		});

		// Get evaluation metrics.
		MulticlassMetrics metrics = new MulticlassMetrics(predictionAndLabels.rdd());
		return metrics.weightedFMeasure();
	}

	/**
	 * predict and classify for the set of data using the trained model 
	 * @param data to be classified 
	 * @return a classified data
	 */
	public JavaPairRDD<String, Iterable<Vector>> predict(JavaPairRDD<Long, Vector> data) {

		// Collect all the anomalies
		anomalies = (data.filter((Tuple2<Long, Vector> t) -> new Double(model.predict(t._2)).intValue() != 0)
				.mapToPair((Tuple2<Long, Vector> t) -> t.swap()).collectAsMap());

		return data.mapToPair((Tuple2<Long, Vector> v1) -> new Tuple2<String, Vector>(
				CLASSES.getIndex(new Double(model.predict( v1._2() )).intValue()).toString(), v1._2() )).groupByKey();

	}

	/**
	 *  Save the classifier model locally
	 */
	public void saveModel() {
		
		// Get the path
		File file = modelPath;
		
		// Delete any existing models
		DataProcessing.deleteDirectory(file);
		
		// Save model
		model.save(MainApplication.javaContext.sc(), file.getAbsolutePath());
	}

	/**
	 * Load the classifier model from the resource file
	 * @return a logistic regression model
	 */
	public LogisticRegressionModel loadModel() {
		
		if (!modelPath.exists() || modelPath.listFiles().length == 0) return null;
		
		model = LogisticRegressionModel.load(MainApplication.javaContext.sc(),
				modelPath.getAbsolutePath());
		return model;
	}

	/**
	 * @return the anomalies extracted from using the classifier model
	 */
	public Map<Vector, Long> getAnomalies() {
		return anomalies;
	}

	/**
	 * @param test the test to set
	 */
	public void setTest(JavaRDD<LabeledPoint> test) {
		this.test = test;
	}
	
	/**
	 * 
	 * @param path the name of the classifier model to be saved locally
	 */
	public void setModelPath(String path) {
		this.modelPath = new File("./resource/models/" + path);
	}

}
