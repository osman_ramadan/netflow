/**
 * 
 */
package netflow.data.analysis;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.math3.stat.descriptive.moment.Mean;
import org.apache.commons.math3.stat.descriptive.moment.StandardDeviation;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.mllib.linalg.Vector;
import org.apache.spark.mllib.linalg.Vectors;
import org.apache.spark.mllib.regression.LabeledPoint;
import org.apache.spark.mllib.stat.MultivariateStatisticalSummary;
import org.apache.spark.mllib.stat.Statistics;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;

import netflow.business.logic.MainApplication;
import scala.Tuple2;

/**
 * 
 * A Utility class to help extract features and normalise the data
 * and some other handful methods
 * 
 * @author Osman
 *
 */
public class FeatureExtraction {

	public static enum DataType {BYTE, PCK, VOLUME, FLOW, BYTEPERFLOW, PCKPERFLOW, VOLUMEPERFLOW,
		MAXBYTEPERFLOW, MAXPCKPERFLOW, MAXVOLUMEPERFLOW}

	private static final int numPartitions = 4;

	/**
	 * normalise the input signal to have a mean of 0 and variance of 1
	 * @param signal to be normalised
	 * @return a normalised signal
	 */
	public static double[] normalise(double[] signal) {

		double mean = new Mean().evaluate(signal);

		double stdv = new StandardDeviation(false).evaluate(signal);

		for (int i = 0; i < signal.length; i++) {
			signal [i] = (signal [i] - mean)/stdv;
		}


		return signal;
	}

	/**
	 * Transform a JavaRDD Row to JavaPairRDD Long, Vector using {@code index} as
	 * the index of the column to hold the keys and the rest will be values
	 * @param data to be transformed
	 * @param index the index of the column to be used as the keys of the JavaPairRDD
	 * @param excludeKey should the key be excluded from the values or not
	 * @return a JavaRDD pair
	 */
	@SuppressWarnings("serial")
	public static JavaPairRDD<Long, Vector> 
	transformToJavaPair(JavaRDD<Row> data, final int index, final boolean excludeKey) {

		// Convert the data from sql.Row to Vector by mapping as a JavaRDD
		return data.mapToPair(
				new PairFunction<Row, Long, Vector>() {

					public Tuple2<Long, Vector> call(Row r) {

						int size = excludeKey? r.size() - 1 : r.size();

						double[] values = new double[size]; // double is good to store values

						long key = 0;
						int i = 0;
						for(int j = 0; j < r.length(); j++) {

							Object obj = r.get(j); // Get the value from the row

							double value = 0;

							if (obj instanceof Integer) { 

								value = (int) obj; // Convert to int if it's an integer

							} else if (obj instanceof BigDecimal) {

								// Convert to long if it's a BigDecimal
								value = ((BigDecimal) obj).longValue(); 

							} else if (obj instanceof Long){

								value = (long) obj; // convert to long if it's a Long

							}

							if (j == index) {
								key = (long) value;
								if (excludeKey) continue;
							}

							values[i]= value;

							i++;
						}
						return new Tuple2<Long, Vector>(key, Vectors.dense(values));
					}
				});

	}

	/**
	 * Normalise the training set using mean normalisation technique
	 * @param parsedData to be normalised
	 * @return normalised data
	 */
	public static JavaPairRDD<Long, Vector> normalise(JavaPairRDD<Long, Vector> parsedData) {

		// Using spark statistics package
		MultivariateStatisticalSummary summary = Statistics.colStats(parsedData.values().rdd());

		// Get the variance of each column
		final double[] variance = summary.variance().toArray();

		System.out.println("The variances are " + Arrays.toString(variance));

		// Get the mean of each column
		final double[] mean = summary.mean().toArray();

		System.out.println("The means are " + Arrays.toString(mean));

		// Normalise by mapping
		@SuppressWarnings("serial")
		JavaPairRDD<Long, Vector> normalised = parsedData.mapValues(new Function<Vector, Vector>() {

			// For each row
			@Override
			public Vector call(Vector v1) throws Exception {

				// Convert the vector to array for processing
				double[] vec = v1.toArray();

				// Get the number of columns/size of the vector
				double[] norm = new double[vec.length];

				// For each value/column of the row/vector 
				// subtract the mean and divide by standard deviation
				for (int i = 0; i < vec.length; i++) {
					norm[i] = (vec[i] - mean[i])/Math.sqrt(variance[i]);
				}

				return Vectors.dense(norm); // Convert the final product into a vector
			}

		});

		return normalised;
	}

	/**
	 * Normalise the input signal to have a mean of 0 and variance of 1
	 * @param parsedData to be normalised
	 * @return a normalised signal
	 */
	public static JavaRDD<Vector> normalise(JavaRDD<Vector> parsedData) {

		// Using spark statistics package
		MultivariateStatisticalSummary summary = Statistics.colStats(parsedData.rdd());

		// Get the variance of each column
		final double[] variance = summary.variance().toArray();

		System.out.println("The variances are " + Arrays.toString(variance));

		// Get the mean of each column
		final double[] mean = summary.mean().toArray();

		System.out.println("The means are " + Arrays.toString(mean));

		// Normalise by mapping
		@SuppressWarnings("serial")
		JavaRDD<Vector> normalised = parsedData.map(new Function<Vector, Vector>() {

			// For each row
			@Override
			public Vector call(Vector v1) throws Exception {

				// Convert the vector to array for processing
				double[] vec = v1.toArray();

				// Get the number of columns/size of the vector
				double[] norm = new double[vec.length];

				// For each value/column of the row/vector 
				// subtract the mean and divide by standard deviation
				for (int i = 0; i < vec.length; i++) {
					norm[i] = (vec[i] - mean[i])/Math.sqrt(variance[i]);
				}

				return Vectors.dense(norm); // Convert the final product into a vector
			}

		});

		return normalised;
	}

	/**
	 * Utility method to convert from list of tuple2 to double[]
	 * @param list to be converted
	 * @return a static array of double
	 */
	public static double[] list2Array(List<Tuple2<Integer, Double>> list) {

		int size = list.size();

		double[] arr = new double[size];

		for (int i = 0; i < size; i++) {
			arr[i] = list.get(i)._2();
		}

		return arr;
	}

	/**
	 * Convert IP address to decimal
	 * @param ipAddress in dotted representation
	 * @return decimal representation of an IP
	 */
	public static long ipToLong(String ipAddress) {

		String[] ipAddressInArray = ipAddress.split("\\.");

		long result = 0;
		for (int i = 0; i < ipAddressInArray.length; i++) {

			int power = 3 - i;

			int ip = 0;

			ip = Integer.parseInt(ipAddressInArray[i]);

			result += ip * Math.pow(256, power);

		}

		return result;
	}

	/**
	 * Convert decimal to IP address
	 * @param ip in decimal
	 * @return dotted representation of an IP
	 */
	public static String longToIp(long ip) {

		return ((ip >> 24) & 0xFF) + "." 
				+ ((ip >> 16) & 0xFF) + "." 
				+ ((ip >> 8) & 0xFF) + "." 
				+ (ip & 0xFF);
	}

	/**
	 * A utility method to extract the features from a data set to be 
	 * used for Network scan and port scan detection via KMeans clustering
	 * @param data to get the features from, has to be in the form of
	 * 'sourceIp - destinationIp - DestinationPort' 
	 * @return the training data, in which each point/row represent distinct
	 * sourceIP and its dimensions are sourceIp, no. of distinct destinationIps and
	 * no. of distinct destinationPorts: Si = [sip ,xi1, xi2]
	 */
	@SuppressWarnings("serial")
	public static JavaRDD<Row> getNetworkPostScanFeatures(JavaRDD<Row> data) {


		// Step one: Group by sourceIp
		JavaPairRDD<Long, Iterable<Row>> dataByIp = data.groupBy(new Function<Row, Long>() {

			@Override
			public Long call(Row v1) throws Exception {

				Object value = v1.get(0);

				if (value instanceof String) {

					return ipToLong(String.valueOf(value));
				}

				return v1.getLong(0); // Get the sourceIp using its index
			}

		}, numPartitions);

		// Repeat for each ip group to get the no. of distinct destIps
		// and the no. of distinct destPorts by performing a map
		JavaRDD<Row> features = dataByIp.map(new Function<Tuple2<Long, Iterable<Row>>, Row>() {

			@Override
			public Row call(Tuple2<Long, Iterable<Row>> v1) throws Exception {

				// Convert a Iterator to ArrayList to generate a JavaRDD data type
				List<Row> list = new ArrayList<>();
				for (Row v : v1._2()) {
					list.add(v);
				}
				// Using the spark context object generate a JavaRDD for efficient parallelised processing
				JavaRDD<Row> rows = MainApplication.javaContext.parallelize(list);

				long[] count = new long[2];

				// Group by destIp and then destPort to get the count
				for (int i = 1; i < 3; i++) {

					final int j = i; // get the number of distinct keys
					count[j-1] = rows.groupBy(new Function<Row, Long>() {

						@Override
						public Long call(Row v1) throws Exception {

							Object value = v1.get(j);

							if (value instanceof Integer) {
								return Long.valueOf((int) value);

							} else if (value instanceof String) {

								try {

									return Long.parseLong(String.valueOf(value));

								} catch (NumberFormatException e) {

									return ipToLong(String.valueOf(value));

								}

							}

							return (long) value;

						}

					}).keys().count();

				}

				// Create a row of [sourceIp, no. of distinct destIp, no. of distinct destPort]
				return RowFactory.create(v1._1(), count[0], count[1]);
			}

		});

		return features;

	}


	/**
	 * Label a clustered data to be used in training a classifier model
	 * @param data to be labelled
	 * @return labelled data using LabelPoint type [Normal=0, Anomaly=1, portScan=2, NetworkScan=3]
	 */
	public static JavaRDD<LabeledPoint> labelClusteredData(JavaPairRDD<String, Iterable<Vector>> data) {

		// Use flatMap
		return data.flatMap((Tuple2<String, Iterable<Vector>> v1) -> {

			// List to hold the labelled points
			List<LabeledPoint> points = new ArrayList<>();
			int label = 0;
			// Classify
			switch (v1._1()) {
			case "anomaly" :
				label = 1;
				break;
			case "Port Scan" :
				label = 2;
				break;
			case "Network Scan" :
				label = 3;
				break;
			}
			for (Vector v : v1._2()) {
				points.add(new LabeledPoint(label, v));
			}
			
			return points.iterator();

		});

	}


	/**
	 * Extract the feature vector of the data in the form of 
	 * total kbytes per min/Mbyte per hour depending on the sampling rate
	 * The feature vector is used for volume-based analysis, i.e 
	 * Wavelets or Fast Fourier Transform
	 * @param data to be processed as [st, pck, bytes]
	 * @param samplingRate =&gt; 0 : for every min in an hour, 1 : for every hour in a day
	 * @param type is the type of data to be extracted {total volume, number of packets per flow,
	 * number of byte per packet
	 * @return an array [b1, b2, ..., bn] where bi the total byte at sample i
	 */
	@SuppressWarnings("serial")
	public static List<Tuple2<Integer, Double>> extractVolumeFeatures(JavaRDD<Row> data, int samplingRate, final DataType type) {


		final double divisible = Math.pow(1024.0, samplingRate + 1);

		// Group the data by the start time and add together all those with the same time
		// Then map into the required form
		JavaPairRDD<Integer, Double> mappedData = data.groupBy(new Function<Row, Integer>() {

			@Override
			public Integer call(Row v1) throws Exception {
				Object value = v1.get(0);

				if (value instanceof String) {

					return Integer.parseInt(String.valueOf(value));

				} else if (value instanceof Long) {

					return (int) v1.getLong(0);

				}

				return v1.getInt(0);
			}

		}, numPartitions).mapToPair(new PairFunction<Tuple2<Integer, Iterable<Row>>, Integer, Double>() {

			@Override
			public Tuple2<Integer, Double> call(Tuple2<Integer, Iterable<Row>> rows) throws Exception {

				double value = 0;

				int count = 0;

				for (Row row : rows._2()) {
					Object v1 = row.get(1);
					Object v2 = row.get(2);
					BigDecimal pck = (v1 instanceof String) ? new BigDecimal(String.valueOf(v1)) : (BigDecimal) v1;
					BigDecimal bytes = (v1 instanceof String) ? new BigDecimal(String.valueOf(v2)) : (BigDecimal) v2;

					// All the connection with the same start time should be summed together
					switch(type) {

					case BYTE:
						value += (bytes.doubleValue()/pck.doubleValue())/divisible;
						break;

					case PCK:
						value += pck.doubleValue();
						break;

					case VOLUME:
						value += bytes.doubleValue()/divisible;
						break;

					case FLOW:
						value++;
						break;

					case BYTEPERFLOW:
						value += bytes.doubleValue()/divisible;
						count++;
						break;

					case PCKPERFLOW:
						value += pck.doubleValue();
						count++;
						break;

					case VOLUMEPERFLOW:
						value += (pck.multiply(bytes).doubleValue())/divisible;
						count++;
						break;

					case MAXBYTEPERFLOW:
						if (bytes.doubleValue()/divisible >= value) value = bytes.doubleValue()/divisible;
						break;

					case MAXPCKPERFLOW:
						if (pck.doubleValue() >= value) value = pck.doubleValue();
						break;

					case MAXVOLUMEPERFLOW:
						if ((pck.multiply(bytes).doubleValue())/divisible >= value){
							value = (pck.multiply(bytes).doubleValue())/divisible;
						}
						break;
					}

				}

				if (count > 0) {
					value = value/(1.0*count);
				}

				return new Tuple2<Integer, Double>(rows._1(), value);
			}

		});

		List<Tuple2<Integer, Double>> list = mappedData.sortByKey().collect(); // collect the data as list

		// Convert the list<Double> to an array of double[] can be very costy
		return list;
	}

	/**
	 * Map the data from JavaRDD Row  to JavaRDD Vector for processing
	 * @param data as RDD Row
	 * @return data as RDD Vector
	 */
	public static JavaRDD<Vector> convertRowToVector(JavaRDD<Row> data) {

		// Map the data to the form JavaRDD<Vector> for processing
		@SuppressWarnings("serial")
		JavaRDD<Vector> mappedData = data.map(new Function<Row, Vector>() {

			@Override
			public Vector call(Row v1) throws Exception {

				double[] values = new double[v1.size()]; // double is good to store values

				for (int i = 0; i < values.length; i++) {

					Object value = v1.get(i); // Get the value from the row

					if (value instanceof Integer) { 

						values[i] = (int) value; // Convert to int if it's an integer

					} else if (value instanceof BigDecimal) {

						// Convert to long if it's a BigDecimal
						values[i] = ((BigDecimal) value).longValue(); 

					} else if (value instanceof Long){

						values[i] = (long) value; // convert to long if it's a Long

					} else if (value instanceof Double) {

						values[i] = (double) value;

					}

				}
				return Vectors.dense(values);
			}

		});

		return mappedData;

	}

}
