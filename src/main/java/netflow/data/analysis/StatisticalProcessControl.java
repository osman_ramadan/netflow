/**
 * 
 */
package netflow.data.analysis;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.math3.stat.descriptive.moment.Mean;
import org.apache.commons.math3.stat.descriptive.moment.Variance;

import scala.Tuple2;

/**
 * 
 * This class implements different control charts to time series anomalies
 * 
 * @author Osman
 *
 */
public class StatisticalProcessControl {
	
	private EWMAChart ewma;


	/**
	 * Class of the EWMA chart to store the previous average and
	 * update the current average accordingly
	 * @author Osman
	 *
	 */
	private class EWMAChart {

		private double preStdv = 0.0;
		private double preAverage = 0.0; // to save the previous average
		private double factor = 0; // It affects the weight of recent to old data
		private int noSubGroups = 0; // number of data points
		private int avgRange = 0;  // an increment used to limit the average result within data points
		private int stdvRange = 0; // an increment used to limit the standard deviation result within data points


		/**
		 * 
		 * @param factor
		 */
		EWMAChart(double factor, int subGroups) {
			this.factor = factor;
			this.noSubGroups = subGroups;
		}

		/**
		 * To reset the the chart if we want to reuse it
		 * @param noSubGroups
		 */
		@SuppressWarnings("unused")
		public void reset(int noSubGroups) {

			this.noSubGroups = noSubGroups;
			avgRange = 0;
			stdvRange = 0;
			preAverage = 0.0;

		}
		
		/**
		 * calculate the current standard deviation using the EWMA algorithm
		 * @param stdv
		 * @return
		 */
		public double calculateStdv(double stdv) {
			
			if (preStdv == 0) {
				preStdv = stdv;
				stdvRange++;
				return stdv;
			}

			stdv = factor * stdv + (1 - factor) * preStdv; // formula, check Wikipedia

			stdvRange++;

			preStdv = stdv; // update the previous standard deviation

			if (stdvRange > noSubGroups) { // don't exceed the limits or number of data points
				return 0.0;
			} 

			return stdv;
		}

		/**
		 * calculate the current average using the EWMA algorithm
		 * @param average
		 * @return
		 */
		public double calculateAvg(double average) {
			
			if (preAverage == 0) {
				preAverage = average;
				avgRange++;
				return average;
			}

			average = factor * average + (1 - factor) * preAverage; // formula, check Wikipedia

			avgRange++;

			preAverage = average; // update the previous average

			if (avgRange > noSubGroups) { // don't exceed the limits or number of data points
				return 0.0;
			} 

			return average;

		}

	}

	/**
	 * A method to calculate the the limits for the Shewhart chart (variable limits)
	 * @param i current index
	 * @param subSize size of the range 
	 * @param data the original data
	 * @param variance class to calculate the variance
	 * @return modified standard deviation
	 */
	private double computeVariableLimits(int i, int subSize, double[] data, Variance variance) {

		double c4 = computeC4(subSize); // required by the algorithm for the limits
		int offset = subSize/2;
		//-sub-array-for-calculating-the-variances--------------------------------------------
		int startIndex = i - offset;
		int endIndex = i + subSize - offset;

		if (startIndex < 0) {
			startIndex = 0;
			endIndex = subSize;
		} else if (endIndex > data.length) {
			endIndex = data.length;
			startIndex = endIndex - subSize;
		}

		double[] subData = Arrays.copyOfRange(data, startIndex, endIndex);
		//------------------------------------------------------------------------------------

		// Get a variable standard deviation
		double sigma = Math.sqrt(variance.evaluate(subData)) / c4;

		return 3*ewma.calculateStdv(sigma);
	}


	/**
	 * An Method to calculate the Exponentially Weighted Moving Average Chart
	 * for a set of data 
	 * @param subSize of the subgroup has to be divisible by the size of the data
	 * @param data representing the signal
	 * @param lambda a factor that value the effect of the historical data to new data
	 * @param L a parameter for anomaly detection
	 * @param improved  the improved version of the chart by combining it with Shewhart chart
	 * @return map of signal label: output Signal
	 */
	public Map<String, Object> computeEWMAChart(int subSize, List<Tuple2<Integer, Double>> data, double lambda, double L, boolean improved) {
		
		if (data.size() <= 2 * subSize) subSize = data.size()/2;

		// Create a map to hold the output data
		Map<String, Object> output = new HashMap<>();

		// Convert the input data to double[] to be processed
		double[] featureVector = FeatureExtraction.list2Array(data);

		System.out.println("The original signal is ");
		System.out.println(Arrays.toString(featureVector));

		// Add the original signal to the output map
		output.put("Original Signal", featureVector);

		int size = featureVector.length;

		Mean mean = new Mean(); // get a mean object to calculate means

		ewma = new EWMAChart(lambda, size); // initialise the chart

		// Get the variance object to calculate variances
		Variance variance = new Variance(false);

		// Initialise all the arrays needed to store data
		double[] avgData = new double[size];
		double[] UCL = new double[size];
		double[] LCL = new double[size];

		// required by the algorithm to get a variable limits
		double lambdaRatio = 10 * lambda/((2.0 - lambda) * subSize);

		// Get a variable standard deviation
		double sigma = Math.sqrt(variance.evaluate(featureVector));

		// For each lump of data depending on the size of the sample
		// data (subSize)
		for (int i = 0; i < size; i++) {

			//-sub-array-for-calculating-the-averages--------------------------------------------
			int startIndex = i;
			int endIndex = i + subSize;

			if (i + subSize > size) {
				startIndex = size - subSize;
				endIndex = size;
			}

			double[] subData = Arrays.copyOfRange(featureVector, startIndex, endIndex);

			double sampleMean = mean.evaluate(subData); // mean of sample data

			avgData[i] = ewma.calculateAvg(sampleMean); // update the mean using EWMA
			//------------------------------------------------------------------------------------

			// evaluate the improved version of the EWMA chart
			if (improved) {
				sigma = computeVariableLimits(i, subSize, featureVector, variance);
			}

			double ewmaVar = lambdaRatio * (1 - Math.pow(1 - lambda, 2*(i+1)));

			// Get the maximum offset from the mean
			double offset = L * sigma * Math.sqrt(ewmaVar);

			// Upper central limit
			UCL[i] = avgData[i] + offset;

			// Lower central limit
			LCL[i] = avgData[i] - offset;

		}

		System.out.println("The array of averages is ");
		System.out.println(Arrays.toString(avgData));

		// Add the averaged signal to the output map
		output.put("Averaged Signal", avgData);

		System.out.println("The upper central limits are ");
		System.out.println(Arrays.toString(UCL));

		// Add the Upper Central Limit signal to the output map
		output.put("Upper Central Limit", UCL);

		System.out.println("The lower central limits are ");
		System.out.println(Arrays.toString(LCL));

		// Add the Lower Central Limit signal to the output map
		output.put("Lower Central Limit", LCL);

		List<Date> anomalies = new ArrayList<>();

		// Implement a detection rule
		for (int i = 0; i < featureVector.length; i++) {
			if (featureVector[i] > UCL[i] || featureVector[i] < LCL[i]) {

				long time = data.get(i)._1();
				Date date = new Date(time * 1000);
				System.out.println("There is an anomaly at time " + time + " " + new Date(time * 1000));
				anomalies.add(date);
			}

		}

		output.put("Anomalies", anomalies.toArray(new Date[anomalies.size()]));

		return output;

	}

	/**
	 * quick method to print an array to be used in Excel
	 * @param arr to print
	 */
	public static void printArray(double[] arr) {
		for (int i = 0; i < arr.length; i++) {
			System.out.println(arr[i]);
		}
	}

	/**
	 * Quick method to calculate c4 used by EWMA algorithm
	 * @param n sub size
	 * @return
	 */
	private double computeC4(double n) {
		return Math.sqrt(2.0/(n - 1)) * factorial((n/2.0))/factorial((n-1.0)/2.0);
	}

	/**
	 * Quick method to calculate the factorial for non-integer numbers
	 * @param n subsize
	 * @return
	 */
	private double factorial(double n) {

		if(n == 0.5) {
			return n * Math.sqrt(Math.PI);
		} else if (n < 0.5) {
			return 1;
		}

		return n*factorial(n-1);
	}

	/**
	 * Compute the standard chart by normalising and limit by 3 sigma method
	 * @param data representation of the signal
	 * @return map of signal label: output Signal
	 */
	public Map<String, Object> computeStandardChart(List<Tuple2<Integer, Double>> data) {

		// Create a map to hold the output data
		Map<String, Object> output = new HashMap<>();

		double[] normalisedSignal = FeatureExtraction.normalise(FeatureExtraction.list2Array(data));

		System.out.println("The normalised normalised signal is ");
		System.out.println(Arrays.toString(normalisedSignal));

		// Add the Normalised signal to the output map
		output.put("Normalised Signal", normalisedSignal);

		List<Date> anomalies = new ArrayList<>();
		// Detection rule
		for (int i = 0; i < normalisedSignal.length; i++) {

			if (Math.abs(normalisedSignal[i]) > 3) {

				long time = data.get(i)._1();
				Date date = new Date(time * 1000);
				System.out.println("There is an anomaly at time " + time + " " + new Date(time * 1000));
				anomalies.add(date);

			}

		}

		output.put("Anomalies", anomalies.toArray(new Date[anomalies.size()]));

		return output;

	}


	/**
	 * An Method to calculate the Exponentially Weighted Moving Average Chart
	 * for a set of data 
	 * @param subSize of the subgroup has to be divisible by the size of the data
	 * @param data representatio of the signal
	 * @param L a parameter used in anomaly detection
	 * @param improved, the improved version of the chart by combining it with Shewhart chart
	 * @return map of signal label: output Signal
	 */
	public Map<String, Object> computeEWMAChart(int subSize, List<Tuple2<Integer, Double>> data, double L, boolean improved) {

		double lambda = 2.0/((data.size()/subSize) + 1);

		return computeEWMAChart(subSize, data, lambda, L, improved);
	}

}
