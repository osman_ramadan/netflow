/**
 * 
 */
package netflow.data.analysis;

import java.io.Serializable;

import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.mllib.feature.PCA;
import org.apache.spark.mllib.feature.PCAModel;
import org.apache.spark.mllib.linalg.Matrix;
import org.apache.spark.mllib.linalg.SingularValueDecomposition;
import org.apache.spark.mllib.linalg.Vector;
import org.apache.spark.mllib.linalg.Vectors;
import org.apache.spark.mllib.linalg.distributed.IndexedRow;
import org.apache.spark.mllib.linalg.distributed.IndexedRowMatrix;
import org.apache.spark.mllib.linalg.distributed.RowMatrix;

import scala.Tuple2;

/**
 * This class implements two different techniques that are commonly used for 
 * reducing the dimensions of a dataset: 
 * <h1>Principal Component Analysis</h1>
 * <p>A statistical procedure that uses an orthogonal transformation to convert a set of observations of
 * possibly correlated variables into a set of values of linearly uncorrelated variables called principal components.
 * The number of principal components is less than or equal to the number of original variables. 
 * This transformation is defined in such a way that the first principal component has the largest
 * possible variance (that is, accounts for as much of the variability in the data as possible),
 * and each succeeding component in turn has the highest variance possible under the constraint that
 * it is orthogonal to the preceding components. The resulting vectors are an uncorrelated orthogonal basis set.
 * PCA is sensitive to the relative scaling of the original variables.</p>
 * <h1>Singular Value Decomposition </h1>
 * <p>The singular value decomposition of an m × n real or complex matrix M  is a factorization of the form U ∑ V'
 * where U is an  m × m real or complex unitary matrix, ∑ is a m × n rectangular diagonal matrix with non-negative
 * real numbers on the diagonal, and V is an n × n real or complex unitary matrix. The diagonal entries σi of ∑ 
 * are known as the singular values of M. The columns of U and the columns of V are called the left-singular vectors
 * and right-singular vectors of M, respectively.</p>
 * 
 * 
 * @author Osman
 *
 */
public class DimensionalityReduction implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -669575675609173766L;

	/**
	 * Reduce the dimension of the data by using Principal Component Analysis
	 * 
	 * @param mappedData to reduce
	 * @param dimensions no of reduced dimensions
	 * @return the reduced data
	 */
	public JavaRDD<Vector> reduceWithPCA(JavaRDD<Vector> mappedData, int dimensions) {

		// Create a RowMatrix from JavaRDD<Vector>
		RowMatrix mat = new RowMatrix(mappedData.rdd());

		// Compute the top dimensions principal components
		Matrix pc = mat.computePrincipalComponents(dimensions);

		RowMatrix projected = mat.multiply(pc);

		JavaRDD<Vector> reducedData = projected.rows().toJavaRDD();

		return reducedData;

	}

	/**
	 * Reduce the dimension of the data by using Principal Component Analysis
	 * 
	 * @param data in the form of key to values (JavaPairRDD)
	 * @param dimensions no of reduced dimensions
	 * @return (JavaPairRDD)
	 */
	@SuppressWarnings("serial")
	public JavaPairRDD<Long, Vector> reduceWithPCA(JavaPairRDD<Long, Vector> data,
			int dimensions) throws breeze.linalg.NotConvergedException {
		
		// If the current number of dimensions is the same as the desired one return current data
		if (dimensions == data.first()._2().size()) return data;

		// Get a PCA model to be used to project the data
		final PCAModel pcaModel = new PCA(dimensions).fit(data.values());

		return data.mapToPair(new PairFunction<Tuple2<Long, Vector>, Long, Vector>() {

			@Override
			public Tuple2<Long, Vector> call(Tuple2<Long, Vector> t) throws Exception {
				return new Tuple2<Long, Vector>(t._1(), pcaModel.transform(t._2()));
			}

		});

	}


	/**
	 * Reduce the dimension of the data by using Singular Value Decomposition
	 * 
	 * @param data in the form of key to values (JavaPairRDD)
	 * @param dimensions no of reduced dimensions
	 * @return (JavaPairRDD)
	 */
	@SuppressWarnings("serial")
	public JavaPairRDD<Long, Vector> reduceWithSVD(JavaPairRDD<Long, Vector> data, int dimensions) {

		// Map to JavaRDD<indexedRow>
		JavaRDD<IndexedRow> mappedData = data.map(new Function<Tuple2<Long, Vector>, IndexedRow>() {

			@Override
			public IndexedRow call(Tuple2<Long, Vector> v1) throws Exception {
				return new IndexedRow(v1._1(), v1._2());
			}

		});

		// Create a RowMatrix from JavaRDD<Vector>
		IndexedRowMatrix mat = new IndexedRowMatrix(mappedData.rdd());

		// Compute the top dimensions singular values and corresponding 
		// singular vectors. U .* S .* V'
		SingularValueDecomposition<IndexedRowMatrix, Matrix> svd = 
				mat.computeSVD(dimensions, true, 1.0E-9d);
		
		IndexedRowMatrix U = svd.U();
		
		Vector s = svd.s();
		
		Matrix V = svd.V().transpose();

		final double[] SValues = s.toArray();

		// Compute the value of U .* S 
		IndexedRowMatrix US = new IndexedRowMatrix(U.rows().toJavaRDD()
				.map(new Function<IndexedRow, IndexedRow>() {

			@Override
			public IndexedRow call(IndexedRow v1) throws Exception {
				
				double[] row = v1.vector().toArray();

				for (int i = 0; i < row.length; i++) {
					row[i] *= SValues[i];
				}

				return new IndexedRow(v1.index(), Vectors.dense(row));
			}

		}).rdd());


		// Compute U .* S .* V'
		IndexedRowMatrix projected = US.multiply(V);
		
		// Transform the IndexedRowMatrix to JavaPairRDD<Long, Vector> by converting 
		// it to JavaRDD<IndexedRow> first then map it to JavaPairRDD<Long, Vector>
		return projected.rows().toJavaRDD().mapToPair(new PairFunction<IndexedRow, Long, Vector>() {

			@Override
			public Tuple2<Long, Vector> call(IndexedRow t) throws Exception {
				return new Tuple2<Long, Vector>(t.index(), t.vector());
			}
			
		});

	}


	/**
	 * Reduce the dimension of the data by using Singular Value Decomposition
	 * 
	 * @param mappedData to reduce
	 * @param dimensions no of reduced dimensions
	 * @return reduced data
	 */
	@SuppressWarnings("serial")
	public JavaRDD<Vector> reduceWithSVD(JavaRDD<Vector> mappedData, int dimensions) {


		// Create a RowMatrix from JavaRDD<Vector>
		RowMatrix mat = new RowMatrix(mappedData.rdd());

		// Compute the top dimensions singular values and corresponding singular vectors. U .* S .* V'
		SingularValueDecomposition<RowMatrix, Matrix> svd = mat.computeSVD(dimensions, true, 1.0E-9d);
		RowMatrix U = svd.U();
		Vector s = svd.s();
		Matrix V = svd.V().transpose();

		final double[] SValues = s.toArray();

		// Compute the value of U .* S 
		RowMatrix US = new RowMatrix(U.rows().toJavaRDD().map(new Function<Vector, Vector>() {

			@Override
			public Vector call(Vector v1) throws Exception {

				double[] row = v1.toArray();

				for (int i = 0; i < row.length; i++) {
					row[i] *= SValues[i];
				}

				return Vectors.dense(row);
			}

		}).rdd());


		// Compute U .* S .* V'
		RowMatrix projected = US.multiply(V);

		return projected.rows().toJavaRDD();

	}

}
