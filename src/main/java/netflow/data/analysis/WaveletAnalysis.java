/**
 * 
 */
package netflow.data.analysis;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.math3.complex.Complex;
import org.apache.commons.math3.stat.descriptive.moment.Variance;
import org.apache.commons.math3.transform.DftNormalization;
import org.apache.commons.math3.transform.FastFourierTransformer;
import org.apache.commons.math3.transform.TransformType;

import jwave.Transform;
import jwave.transforms.FastWaveletTransform;
import jwave.transforms.wavelets.haar.Haar1;
import scala.Tuple2;

/**
 * 
 * An implementation of the signal analysis techniques to detect network anomalies
 * using Fast Wavelet Transform and Fast Fourier Transform 
 * 
 * @author Osman
 *
 */
public class WaveletAnalysis {


	/**
	 * Apply wavelet analysis to detect an anomalies in the frequency domain
	 * The steps are as follow:
	 * 
	 * <p> (1) Add zero padding to signal to make the size of the signal array a power of 2 </p>
	 * <p> (2) Apply a window function (Hanning is suitable) to remove any artifacts created
	 * by the padding</p>
	 * <p> (3) Isolate only the wavelet coefficients at level 1 (n/2 : n-1) which is the H-part</p>
	 * <p> (4) Normalise the H-part to have a variance of 1</p>
	 * <p> (5) Add a moving window with a specific width/Sampling rate that depends on the duration of the
	 * anomaly to be detected</p>
	 * <p> (6) Calculate the local variability (variance) of the signal at each frame
	 *  by computing the variance of the data falling within a moving
	 *  window of specified size</p>
	 * <p> (7) Label any frame with local variance above a threshold to be an anomaly </p>
	 * <p> (8) Get the index of the window frame to locate the anomaly in the time domain </p>
	 * 
	 * @param data the signal to be analysed a list of (key, value) e.g (start time, value)
	 * @param threshold to be classified as anomaly
	 * @param frameSize the size/duration of the moving window that is used to calculate the local Variability
	 * @param applyWindow apply a window function to the signal (not recommended if the
	 *  anomalies are likely to be at near end of the signal)
	 * @return Map of key values as follow: "Original Signal": double[], "Band 1 Signal": double[],
	 * "Variability Signal": double[], "Anomalies: Date[]"
	 */
	public Map<String, Object> analyseSignal(List<Tuple2<Integer, Double>> data,
			double threshold, int frameSize, boolean applyWindow) {

		// Create a map to hold the output data
		Map<String, Object> output = new HashMap<>();
		
		Transform t = new Transform(new FastWaveletTransform(new Haar1()));

		// Convert the input data to double[] to be processed
		double[] featureVector = FeatureExtraction.list2Array(data);
		
		// Add the original signal to the output map
		output.put("Original Signal", featureVector);

		// Fast Wavelet Transform only accept sizes that are power of 2 so add padding 
		featureVector = padSignal(featureVector);

		// Padding could introduce artifacts so windowing is required
		if (applyWindow) featureVector = addWindow(featureVector, 0.3);

		int size = featureVector.length;

		System.out.println("The original signal is ");
		System.out.println(featureVector.length + " " + Arrays.toString(featureVector));

		// Isolate only the wavelet coefficients
		double[] composedSignal  = Arrays.copyOfRange(t.forward(featureVector, 1), size/2, size);

		// Normalise the H-part of the signal
		composedSignal = FeatureExtraction.normalise(composedSignal);

		System.out.println("The normalised decomposed signal is at level 1 ");
		System.out.println(Arrays.toString(composedSignal));
		
		output.put("Band 1 Signal", composedSignal);

		// Compute the variability signal
		double[] vSignal = computeLocalVariability(composedSignal, frameSize);

		System.out.println("The local variability signal is ");
		System.out.println(Arrays.toString(vSignal));
		
		output.put("Variability Signal", vSignal);

		int originalSize = data.size();
		
		List<Date> anomalies = new ArrayList<>();

		// Add a detection rule  to detect anomalies
		for (int i = 0; i < vSignal.length; i++) {

			if (Math.abs(vSignal[i]) >= threshold) {

				int offset = (originalSize - size)/2;
				long time = data.get(Math.max(0, 2*i + offset))._1();
				Date date = new Date(time * 1000);
				System.out.println("There is an anomaly at time " + time + " " + date);
				anomalies.add(date);
			}
		}
		
		output.put("Anomalies", anomalies.toArray(new Date[anomalies.size()]));
		
		return output;
	}

	/**
	 * Compute the local variability of the (normalized) H-parts
	 * by computing the variance of the data falling within a moving
	 * window of specified size. The length of this moving window
	 * should depend on the duration of the anomalies that we wish to
	 * captured
	 * @param signal to be analysed
	 * @param frameSize the duration of the frame/window in the time domain (e.g. duration in mins)
	 * @return variability signal
	 */
	private double[] computeLocalVariability(double[] signal, int frameSize) {

		int size = signal.length;

		int noFrames = (int) Math.ceil(size*1.0/frameSize);

		double[] vSignal = new double[size];

		int reminder = size%frameSize;

		Variance var = new Variance(false);

		for (int i = 0; i < size; i++) {

			if (i == noFrames - 1 && reminder > 0) {
				vSignal[i] = var.evaluate(Arrays.copyOfRange(signal, i, i + reminder));
			} else {
				vSignal[i] = var.evaluate(Arrays.copyOfRange(signal, i, i + frameSize));
			}

		}

		return vSignal;
	}

	/**
	 * Convert the signal to the frequency domain using fast Fourier transform
	 * and output the result
	 * @param originalSignal to be transformed to the frequency domain
	 */
	public void FourierTransformAnalysis(double[] originalSignal) {
		FastFourierTransformer fft = new FastFourierTransformer(DftNormalization.STANDARD);

		// Fast Fourier Transform only accept sizes that are power of 2 so add padding 
		// Padding could introduce artifacts so windowing is required
		originalSignal = padSignal(originalSignal);
		originalSignal = addWindow(originalSignal, 1);

		Complex[] fsignal = fft.transform(originalSignal, TransformType.FORWARD);

		System.out.println("The frequency domain of the signal is ");

		for (int i = 0; i < fsignal.length; i++) {
			System.out.println(fsignal[i].abs());

		}
		// TODO use the frequency domain to extract any anomalies in the frequency domain
		// Although we cann't localise in time we will be aware of an anomaly within that time frame
	}

	/**
	 * Add zero padding to make the array size a power of 2
	 * add the zeros at the start and at the end
	 * @param originalSignal to add padding to
	 */
	private double[] padSignal(double[] originalSignal) {

		int size = originalSignal.length;

		int exp = (int)Math.floor(Math.log(size)*1.0/Math.log(2));

		int newSize = (int) Math.pow(2,(exp + Math.round(size/Math.pow(2, exp)) - 1)) ;

		int diff = newSize - size;

		// Trim the array as it is better
		if (diff < 0) {

			diff = Math.abs(diff);

			int init = diff/2;

			originalSignal = Arrays.copyOfRange(originalSignal, init, init + newSize);

			return originalSignal;

		}

		int init = diff/2;

		int fin = diff - init;

		// Add zeros before
		originalSignal = ArrayUtils.addAll(new double[init], originalSignal);
		// Add zeros after
		originalSignal = ArrayUtils.addAll(originalSignal, new double[fin]);

		return originalSignal;

	}

	/**
	 * Apply a Hanning window function to remove any artifacts that can result
	 * from padding.
	 * Hanning window is suitable here as the start and end of the signal should be zero
	 * 
	 * @param signal_in to apply the window function to
	 * @return the windowed signal
	 */
	private double[] addWindow(double[] signal_in, double coeff) {

		int size = signal_in.length;

		for (int i = 0; i < size; i++){
			double window = 0.5 * (1.0 - Math.cos(2.0 * Math.PI * i / size));
			signal_in[i] = signal_in[i] * Math.pow(window, coeff);
		}

		return signal_in;

	}

}
