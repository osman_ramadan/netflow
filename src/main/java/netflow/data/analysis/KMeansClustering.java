/**
 * 
 */
package netflow.data.analysis;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.spark.Accumulator;
import org.apache.spark.AccumulatorParam;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.mllib.clustering.KMeans;
import org.apache.spark.mllib.clustering.KMeansModel;
import org.apache.spark.mllib.linalg.Vector;

import netflow.business.logic.MainApplication;
import scala.Tuple2;

/**
 * This class implements the KMeans clustering algorithm
 * that is weighted by entropy based on information theory.
 * It also performs anomaly detection
 * 
 * @author Osman
 *
 */
@SuppressWarnings("deprecation")
public class KMeansClustering implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7624377925429776326L;
	private static final int numPartitions = 1;

	public static enum CLASSES {NORMAL(0), ANOMALY(1), PORTSCAN(2), NETWORKSCAN(3);

		private int index;

		private CLASSES(int index) { this.index = index; }

		public static CLASSES getIndex(int index) {
			for (CLASSES l : CLASSES.values()) {
				if (l.index == index) return l;
			}
			throw new IllegalArgumentException("CLASS not found");
		}

		@Override
		public String toString() {
			String name = "";
			switch(this) {
			case NORMAL:
				name = "normal";
				break;
			case ANOMALY:
				name = "anomaly";
				break;
			case NETWORKSCAN:
				name = "Network Scan";
				break;
			case PORTSCAN:
				name = "Port Scan";
				break;
			}
			return name;
		}
		
	}


	private Map<Vector, Long> anomalies = new HashMap<>();
	private boolean portNetworkScanDetection = false;
	private double portScanThreshold = Math.tan((75.0*Math.PI)/180.0);
	private double networkScanThreshold = Math.tan((15.0*Math.PI)/180.0);
	private int anomalousMax = 2;
	private int maxNumClusters = 10;

	// Create an accumulator to save the values of the anomalies

	private final Accumulator<Map<Vector, Long>>  acc = 
			MainApplication.javaContext.accumulator(anomalies, new MapAccumulableParam());

	/**
	 * Initialise the KMeans algorithm
	 * @param parsedData the data to run the algorithm on
	 * @param normalised Is this data normalised or not
	 * @return labelled data as normal, anomaly, network scan or port scan
	 */
	@SuppressWarnings("serial")
	public JavaPairRDD<String, Iterable<Vector>> initialiseKMeans(
			JavaPairRDD<Long, Vector> parsedData, 
			boolean normalised) {

		if (!normalised) {
			// Normalise the data to avoid offsets absolute value of the data
			parsedData = FeatureExtraction.normalise(parsedData);
		}

		System.out.println("This is the first value " + parsedData.first());

		// Cache data in the memory for better performance when processing
		parsedData.cache();

		// Define the number of iterations
		int numIterations = 10;

		// Variable to hold the current entropy
		double currentEntropy = 1000;


		// Try k clusters
		for (int k = 1; k <= maxNumClusters; k += Math.ceil(Math.log10(maxNumClusters)) ) {
			// create KMeansModel
			KMeansModel clusters = KMeans.train(parsedData.values().rdd(), k, numIterations);

			// calculate the total entropy for that clustering, low entropy => good clustering
			double totalEntropy = calculateEntropy(parsedData, clusters);

			if (totalEntropy > currentEntropy && !anomalies.isEmpty()) {

				// Evaluate clustering by computing Within Set Sum of Squared Errors
				double WSSSE = clusters.computeCost(parsedData.values().rdd());
				System.out.println("Within Current(excluded), Sum of Squared Errors = " + WSSSE + " "
						+ "for number of clusters of " + k + " and with Entropy " + totalEntropy);
				break;
			}

			currentEntropy = totalEntropy;

			if (!acc.value().isEmpty()) anomalies = new HashMap<>(acc.value());

			acc.value().clear();

			// Evaluate clustering by computing Within Set Sum of Squared Errors
			double WSSSE = clusters.computeCost(parsedData.values().rdd());
			System.out.println("Within Set Sum of Squared Errors = " + WSSSE + " "
					+ "for number of clusters of " + k + " and with Entropy " + totalEntropy);

		}

		// label the the cluster as: normal, port scan, network scan, traffic anomaly
		return parsedData.values().groupBy(new Function<Vector, String>() {

			@Override
			public String call(Vector v1) throws Exception {
				String label = "normal";

				if (containsAnomaly(v1, anomalies)) {
					label = "anomaly";
					// Should the anomaly itself be classified 
					if (portNetworkScanDetection) {
						double value = v1.apply(1)/v1.apply(0);
						if (value <= networkScanThreshold) {
							label = "Network Scan";
						} else if (value >= portScanThreshold) {
							label = "Port Scan";
						}
					}
				}

				return label;
			}

		});

	}

	/**
	 * Utility class to be able to accumulate the anomalies detected during distributed computing.
	 * Spark requires a user-defined accumulator to store values during processing
	 * 
	 * 
	 * @author Osman
	 *
	 */
	public static class MapAccumulableParam implements AccumulatorParam<Map<Vector, Long>>, Serializable {

		/**
		 * 
		 */
		private static final long serialVersionUID = 2441930997178719246L;

		@Override
		public Map<Vector, Long> addInPlace(Map<Vector, Long> arg0, Map<Vector, Long> arg1) {



			for (Map.Entry<Vector, Long> entry : arg1.entrySet()) {

				if (!containsAnomaly(entry.getKey() ,arg0)) {
					arg0.put(entry.getKey(), entry.getValue());
				}

			}

			return arg0;
		}

		@Override
		public Map<Vector, Long> zero(Map<Vector, Long> arg0) {
			return new HashMap<Vector, Long>();
		}

		@Override
		public Map<Vector, Long> addAccumulator(Map<Vector, Long> arg0, Map<Vector, Long> arg1) {

			for (Map.Entry<Vector, Long> entry : arg1.entrySet()) {

				if (!containsAnomaly(entry.getKey() ,arg0)) {
					arg0.put(entry.getKey(), entry.getValue());
				}

			}

			return arg0;
		}


	}

	/**
	 * @return the anomalies
	 */
	public Map<Vector, Long> getAnomalies() {
		return anomalies;
	}


	/**
	 * Add the anomaly to the list of anomalies stored
	 * @param key
	 * @param values
	 */
	private void addAnomaly(long key, Vector values) {

		Map<Vector, Long> value = acc.localValue(); 

		value.put(values, key);

		acc.merge(value);

	}

	/**
	 * Does {@code anomalies} contain the following value
	 * @param values
	 * @return
	 */
	private static boolean containsAnomaly(Vector value, Map<Vector, Long> anomalies) {

		double[] anomaly = value.toArray();

		boolean contains = false;

		for (Vector v : anomalies.keySet()) {
			if (Arrays.equals(v.toArray(), anomaly)) {
				contains = true;
				break;
			}
		}

		return contains;

	}

	/**
	 * A method to calculate the entropy (-Sum(p(x)*log(p(x)))) of a clustering
	 * low entropy => good clustering
	 * @param parsedData the training data to be processed to calculate Entropy
	 * @param model the KMeans model to extract each cluster
	 * @return the total entropy of a clustering
	 */
	@SuppressWarnings("serial")
	private double calculateEntropy (JavaPairRDD<Long, Vector> parsedData, final KMeansModel model) {

		// total number of rows of the training set
		final long totalNoRows = parsedData.count();

		// Group the training set by cluster
		JavaPairRDD<Integer, Iterable<Tuple2<Long, Vector>>> groupedClusters = 
				parsedData.groupBy(new Function<Tuple2<Long, Vector>, Integer>() {

					@Override
					public Integer call(Tuple2<Long, Vector> v1) throws Exception {
						return model.predict(v1._2()); // for each data point/vector get its cluster index
					}

				}, numPartitions);

		// Map the grouped clusters into entropy of the cluster and the number of rows in that cluster
		JavaPairRDD<Double, Long> entropies = groupedClusters.values()
				.mapToPair(new PairFunction<Iterable<Tuple2<Long,Vector>>, Double, Long>() {

					@Override // Run for each cluster
					public Tuple2<Double, Long> call(Iterable<Tuple2<Long, Vector>> t) throws Exception {

						// Convert a Iterator to ArrayList to generate a JavaRDD data type
						List<Vector> list = new ArrayList<>();
						List<Long> sipList = new ArrayList<>();
						for (Tuple2<Long, Vector> v : t) {
							sipList.add(v._1());
							list.add(v._2());
						}

						// Using the spark context object generate a JavaRDD for efficient parallelised processing
						JavaRDD<Vector> rows = MainApplication.javaContext.parallelize(list);

						// The number of rows in each cluster
						final long numberOfrows = rows.count();

						System.out.println("Number of rows in a cluster is " + numberOfrows);

						if (numberOfrows <= anomalousMax) {

							System.out.println("This is an anomalous cluster ");

							for (int i = 0; i < list.size(); i++) {

								long sip = sipList.get(i);
								Vector values = list.get(i);

								// Add this anomaly to the list of anomalies
								addAnomaly(sip, values);

								System.out.println("\t" + FeatureExtraction.longToIp(sip)
								+ "/" + sip +  " : " + values);
							}


						}

						// The number of dimensions/features/column of each cluster
						int noDim = rows.first().size();

						// Initialise the entropy of the cluster
						double entropy = 0.0;

						// Loop through the dimensions to calculate the frequency of each dimension/column
						for (int i = 0; i < noDim; i++ ) {

							// To be used from mapping as it is a parallelised
							// computing so it requires a final value
							final int k = i;

							// Group the cluster by column k (eg. sip or dip) to get all the rows with the same 
							// k (eg. sip). Then count the number of occurrences for each k value (eg. sip)
							// It returns a map of <sip, occurrences>
							JavaPairRDD<Double, Iterable<Vector>> groupedRows = rows
									.groupBy(new Function<Vector, Double>() {

										@Override // This is called on every row in the cluster
										public Double call(Vector v1) throws Exception {
											// return the value of the k column of the vector to group by.
											return v1.toArray()[k];
										}

									}, numPartitions);

							// Get the weighted information content for each distinct value
							// of column k in the cluster
							JavaRDD<Double> frequencies = groupedRows
									.mapValues(new Function<Iterable<Vector>, Double>() {

										@Override
										public Double call(Iterable<Vector> v1) throws Exception {
											long occurrence = 0;
											for (@SuppressWarnings("unused") Vector v : v1) {
												occurrence++;
											}

											// Probability = no. of occurrences of this value/total number of values
											double p = (occurrence * 1.0)/numberOfrows; 

											// Weighted information Content = p * log(p)
											double infoContent = p * Math.log(p);

											return infoContent;
										}

									}).values();

							// Entropy = the expected value of the information content = 
							// sum of Weighted information Content 
							entropy += frequencies.reduce(new Function2<Double, Double, Double>() {

								@Override // for each distinct value sum the entropies
								public Double call(Double v1, Double v2) throws Exception {

									// Entropy = sum of p * log(p)
									return v1 + v2;
								}

							});

						}
						// Return a map of entropy of the cluster vs number of rows of the cluster
						return new Tuple2<Double, Long>(entropy, numberOfrows);
						// number of rows to calculate the probability of the cluster
					}

				});

		// Calculate the total entropy of the clustering by summing the product of the
		// entropy, of each cluster, and its probability of occurrence 
		double totalEntropy = entropies.map(new Function<Tuple2<Double,Long>, Double>() {

			@Override
			public Double call(Tuple2<Double, Long> v1) throws Exception {

				// The product of the entropy of the cluster with its probability.
				// Probability = no. of rows in a cluster divided by the total number of rows in the training set.
				double value = v1._1() * ((v1._2() * 1.0)/totalNoRows); 

				return value;
			}

		}).reduce(new Function2<Double,Double,Double>() { // Sum the product together

			// For each row, the first value is the accumulative value, the second is the row
			@Override
			public Double call(Double v1, Double v2) throws Exception {

				return v1 + v2; // sum the products together
			}

		});

		return totalEntropy * -1; // entropy is negative multiple by -1 to satisfy the definition

	}


	/**
	 * @return the portScanThreshold
	 */
	public double getPortScanThreshold() {
		return Math.atan(portScanThreshold) * (180.0/Math.PI);
	}


	/**
	 * @param portScanThreshold the portScanThreshold to set
	 */
	public void setPortScanThreshold(double portScanThreshold) {
		this.portScanThreshold = Math.tan((portScanThreshold*Math.PI)/180.0);
	}


	/**
	 * @return the networkScanThreshold
	 */
	public double getNetworkScanThreshold() {
		return Math.atan(networkScanThreshold) * (180.0/Math.PI);
	}


	/**
	 * @param networkScanThreshold the networkScanThreshold to set
	 */
	public void setNetworkScanThreshold(double networkScanThreshold) {
		this.networkScanThreshold = Math.tan((networkScanThreshold*Math.PI)/180.0);;
	}


	/**
	 * @return the portNetworkScanDetection
	 */
	public boolean isPortNetworkScanDetection() {
		return portNetworkScanDetection;
	}


	/**
	 * @param portNetworkScanDetection the portNetworkScanDetection to set
	 */
	public void setPortNetworkScanDetection(boolean portNetworkScanDetection) {
		this.portNetworkScanDetection = portNetworkScanDetection;
	}


	/**
	 * @return the anomalousMax
	 */
	public int getAnomalousMax() {
		return anomalousMax;
	}


	/**
	 * @param anomalousMax the anomalousMax to set
	 */
	public void setAnomalousMax(int anomalousMax) {
		this.anomalousMax = anomalousMax;
	}


	/**
	 * @return the maxNumClusters
	 */
	public int getMaxNumClusters() {
		return maxNumClusters;
	}


	/**
	 * @param maxNumClusters the maxNumClusters to set
	 */
	public void setMaxNumClusters(int maxNumClusters) {
		this.maxNumClusters = maxNumClusters;
	}

}
