/**
 * 
 */
package netflow.data.in;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.log4j.Level;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.DataFrameReader;
import org.apache.spark.sql.SQLContext;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import netflow.business.logic.TrafficFilter;
import netflow.notification.FTPAlert;

/**
 * 
 * Sample Test class to connect the database server
 * 
 * @author Osman
 *
 */

@Configuration
@ComponentScan(basePackages = {"netflow.business.logic", "netflow.data.analysis",
		"netflow.notification", "netflow.dataIn"})

public class DumpData {


	public static final String MYSQL_DRIVER = "com.mysql.jdbc.Driver";
	private static final String MYSQL_USERNAME = "oramadan";
	private static final String MYSQL_PWD = "oramadan@eseye.comSecret";
	public static final String MYSQL_CONNECTION_URL = "jdbc:mysql://rsnnfx.dev.redstation.eseye.net:3306/nfx?"
			+ "user=" + MYSQL_USERNAME + "&password=" + MYSQL_PWD;

	private static final org.apache.log4j.Logger LOGGER = org.apache.log4j.Logger.getLogger(DumpData.class);

	public static JavaSparkContext javaContext;
	/**
	 * @param args from the console
	 */
	public static void main(String[] args) {

		Logger.getLogger("org").setLevel(Level.OFF);
		Logger.getLogger("akka").setLevel(Level.OFF);

		javaContext = new JavaSparkContext(new SparkConf()
				.setAppName("Netflow").setMaster("local[*]"));

		SQLContext sqlContext = new SQLContext(javaContext);

		Map<String, String> options = new HashMap<>();
		options.put("driver", MYSQL_DRIVER);
		options.put("url", MYSQL_CONNECTION_URL);

		DataFrameReader sqlConnection = sqlContext.read().format("jdbc").options(options);

		@SuppressWarnings("resource")
		ApplicationContext context = 
		new AnnotationConfigApplicationContext(DumpData.class);

		TrafficFilter trafficFilter = context.getBean(TrafficFilter.class);

		FTPAlert ftpAlert = context.getBean(FTPAlert.class);

		trafficFilter.analyseData(sqlConnection);

		trafficFilter.filterFTP(sqlConnection, ftpAlert);

		LOGGER.info("Connection is successful");

	}

}
