/**
 * 
 */
package netflow.data.in;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;

import netflow.data.analysis.FeatureExtraction;

/**
 * 
 * Utility class to help process the data that is read from a local csv file
 * 
 * @author Osman
 *
 */
public class DataProcessing {

	private final static SimpleDateFormat formatter = new SimpleDateFormat("MMM dd yyyy HH:mm:ss");

	/**
	 * Utility method to convert the time format of a table from date to timestamp
	 * It also convert all Strings to Numbers
	 * @param data to be pre-processed
	 * @return pre-processed data
	 */
	@SuppressWarnings("serial")
	public static JavaRDD<Row> convertToTimestamp(JavaRDD<Row> data) {

		// Using one map
		return data.map(new Function<Row, Row>() {

			@Override
			public Row call(Row v1) throws Exception {

				// A list to hold values of the row
				Object[] values = new Object[v1.length()];

				for (int i = 0; i < v1.length(); i++) {

					if (i == 0) {

						String st = v1.getString(i);

						Date date = formatter.parse(st);

						values[i] = date.getTime()/1000;

					} else {

						String value = v1.getString(i);

						try {

							values[i] = Long.parseLong(String.valueOf(value));

						} catch (NumberFormatException e) {

							values[i] = FeatureExtraction.ipToLong(String.valueOf(value));

						}

					}
				}
				return RowFactory.create(values);
			}
		});

	}

	/**
	 * Force deletion of directory
	 * @param path the path of the file
	 * @return whether deletion was successful or not
	 */
	public static boolean deleteDirectory(File path) {
		if (path.exists()) {
			File[] files = path.listFiles();
			for (int i = 0; i < files.length; i++) {
				if (files[i].isDirectory()) {
					deleteDirectory(files[i]);
				} else {
					files[i].delete();
				}
			}
		}
		return (path.delete());
	}

	/**
	 * A utility method to get the path of the most recent model stored locally
	 * @param modelpath path representing the parent directory
	 * @return the path of the most recent model
	 */
	public static String getModelPath(String modelpath) {

		File path = new File("./resource/models/" + modelpath);

		List<String> modelPaths = new ArrayList<>();

		if (path.exists()) {
			File[] files = path.listFiles();

			for (int i = 0; i < files.length; i++) {
				if (files[i].isDirectory()) {
					modelPaths.add(files[i].getName());
				} 
			}
		}

		if (modelPaths.isEmpty()) return "";

		Collections.sort(modelPaths);

		return modelPaths.get(modelPaths.size() - 1);
	}

	/**
	 * Quick method to automatically rename files on saving
	 * @param path oldPath the existing path
	 * @return the new path
	 */
	public static String getPath(String path) {

		for (int i = 0; i < 1000; i++) {

			if (i == 0 && !Files.exists(Paths.get(path))) {
				return path;
			}

			if (Files.exists(Paths.get(path + i))) {
				continue;
			}

			return path + i;

		}

		return null;

	}

	/**
	 * Utility method to convert the String format of a data to Numbers (long)
	 * @param data to be processed
	 * @return processed data
	 */
	@SuppressWarnings("serial")
	public static JavaRDD<Row> convertToNumbers(JavaRDD<Row> data) {

		// Using one map
		return data.map(new Function<Row, Row>() {

			@Override
			public Row call(Row v1) throws Exception {

				// A list to hold values of the row
				Object[] values = new Object[v1.length()];

				for (int i = 0; i < v1.length(); i++) {

					String value = v1.getString(i);

					try {

						values[i] = Long.parseLong(String.valueOf(value));

					} catch (NumberFormatException e) {

						values[i] = FeatureExtraction.ipToLong(String.valueOf(value));

					}

				}
				return RowFactory.create(values);
			}
		});

	}

}
