package netflow.visualisation;

import java.awt.EventQueue;
import java.awt.FlowLayout;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import java.awt.Font;

import netflow.business.logic.MainApplication;
import netflow.data.analysis.DimensionalityReduction;
import netflow.data.analysis.FeatureExtraction;
import netflow.data.analysis.KMeansClustering;
import netflow.data.analysis.MultiClassClassification;
import netflow.data.analysis.StatisticalProcessControl;
import netflow.data.analysis.WaveletAnalysis;
import scala.Tuple2;

import javax.swing.JCheckBox;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.Component;
import java.awt.Desktop;

import javax.swing.JTextField;
import javax.swing.SpinnerDateModel;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import javax.swing.JOptionPane;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import javax.swing.border.LineBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.JSeparator;
import javax.swing.JRadioButton;

import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.mllib.linalg.Vector;
import org.apache.spark.sql.DataFrameReader;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SQLContext;

import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListCellRenderer;
import javax.swing.ImageIcon;
import javax.swing.JPasswordField;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.lang.reflect.Method;
import java.nio.file.Files;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.awt.event.ActionEvent;
import java.awt.SystemColor;
import java.awt.Toolkit;

import javax.swing.ButtonGroup;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingWorker;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import javax.swing.JProgressBar;
import netflow.data.analysis.FeatureExtraction.DataType;
import netflow.data.in.DataProcessing;
import netflow.data.in.DumpData;

import javax.swing.SwingConstants;
import netflow.business.logic.MainApplication.MODULES;
import netflow.business.logic.NetworkAnalysis;

/**
 * The main graphical user interface to interact with the different algoirthms and display the
 * output data
 * 
 * @author Osman
 *
 */
public class MainWindow {

	private JFrame frmNetflowAnalyser;
	private JTextField txtUsername;
	private JTextField txtDatabaseURL;
	private JPasswordField txtPassword;
	private JComboBox<String> cmBxtables;
	private JRadioButton rdbtnChooseFeatures;
	private JPanel chooseFeaturesPanel;
	private CardLayout navigator;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JSpinner limitSpinner;
	private JTextField txtSQLQuery;
	private JComboBox<FEATURES> cmBxLabel;
	private JComboBox<String> cmBxNoDimensions;
	private JTextField txtSipRange;
	private JPanel chartPanel;
	private JTextArea textAreaAnomalies;

	private DataFrameReader sqlConnection;
	private JavaPairRDD<Long, Vector> normalisedFeatures;
	private String currentAlgorithm = "";

	private static final String SEPERATOR = "$";
	private static final String KEY = "Bar12345Bar12345";

	private String[] tablesNames;
	private JCheckBox chckbxEnableDimensionalityReduction;
	private JCheckBox chckbxNormalise;
	private JSpinner spinnerVolumeFromTime;
	private JSpinner spinnerVolumeToTime;
	private JTextField txtVolumeSqlQuery;
	private JTextField txtWaveletThreshold;
	private JComboBox<DataType> cmBxVolumeFeatureType;
	private JComboBox<Date> cmBxVolumeChooseDate;
	private JSpinner volumeLimitSpinner;
	private JSpinner spinnerVolumeFrameSize;
	private JCheckBox chckbxApplyAWindow;
	private JTextField txtSPCLambda;
	private JTextField txtSPCThreshold;
	private JComboBox<String> cmbxSPCChartType;
	private JTextField txtPortScanThreshold;
	private JTextField txtNetworkScanThreshold;
	private JSpinner anomalMaxClusteringSpinner;
	private JCheckBox chckbxUpdateClassifierModel;
	private JTextField txtSipRangeClassifier;
	private JRadioButton rdbtnUseAClassifier;
	private JSpinner maxNoClustersSpinner;
	private JComboBox<MODULES> cmBxModule;

	private static enum FEATURES {st(0), bytes(1), pck(2), sip(3), dip(4), proto(5), app(6),
		sp(7), dp(8), nh(9), sif(10), dif(11);

		private int index;

		private FEATURES(int index) { this.index = index; }

		public static FEATURES getIndex(int index) {
			for (FEATURES l : FEATURES.values()) {
				if (l.index == index) return l;
			}
			throw new IllegalArgumentException("FEATURES not found");
		}
	}

	/**
	 * Launch the application.
	 * @param args any arguments from main
	 */
	public static void main(String[] args) {

		// Initialise Spark Engine to run locally
		Logger.getLogger("org").setLevel(Level.OFF);
		Logger.getLogger("akka").setLevel(Level.OFF);

		try {
			System.setOut(new PrintStream(new FileOutputStream(new File("./resource/output.txt").getAbsolutePath())));
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainWindow window = new MainWindow();
					window.frmNetflowAnalyser.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainWindow() {
		try {

			// Initialise the GUI
			initialize();

		} catch (Exception e) {

			e.printStackTrace();

			this.showMessage(e.getMessage(), "Application Error", JOptionPane.ERROR_MESSAGE);

		}

		// Check if login details is already saved locally
		checkLogin();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmNetflowAnalyser = new JFrame();
		frmNetflowAnalyser.setTitle("Netflow Analyser");
		frmNetflowAnalyser.setBounds(100, 100, 800, 600);
		frmNetflowAnalyser.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		navigator = new CardLayout(0, 0);
		frmNetflowAnalyser.getContentPane().setLayout(navigator);

		JPanel mainPanel = new JPanel();
		frmNetflowAnalyser.getContentPane().add(mainPanel, "name_1406273621381776");
		mainPanel.setLayout(null);

		JLabel lblUsername = new JLabel("Username");
		lblUsername.setBounds(153, 102, 132, 42);
		lblUsername.setFont(new Font("MS Reference Sans Serif", Font.PLAIN, 16));
		mainPanel.add(lblUsername);

		JLabel lblPassword = new JLabel("Password");
		lblPassword.setFont(new Font("MS Reference Sans Serif", Font.PLAIN, 16));
		lblPassword.setBounds(153, 162, 132, 42);
		mainPanel.add(lblPassword);

		JLabel lblDatabaseUrl = new JLabel("Database URL");
		lblDatabaseUrl.setFont(new Font("MS Reference Sans Serif", Font.PLAIN, 16));
		lblDatabaseUrl.setBounds(153, 232, 132, 29);
		mainPanel.add(lblDatabaseUrl);

		final JCheckBox chkSaveDetails = new JCheckBox("Save Details");
		chkSaveDetails.setFont(new Font("MS Reference Sans Serif", Font.PLAIN, 16));
		chkSaveDetails.setBounds(153, 327, 132, 23);
		mainPanel.add(chkSaveDetails);

		JButton btnLogin = new JButton("Login");
		btnLogin.addActionListener(new ActionListener() {

			// Performing the login sequence
			public void actionPerformed(ActionEvent arg0) {

				// Get login details
				String username = txtUsername.getText();
				String password = String.valueOf(txtPassword.getPassword());
				String url = txtDatabaseURL.getText();

				// Do the login
				if(!login(username, password, url)) return;

				// Login is successful by this point
				showMessage("You have logged in successfully", "Login", JOptionPane.INFORMATION_MESSAGE);

				// Check if saveDetails is ticked
				if (chkSaveDetails.isSelected()) {

					// Save Login details
					try {
						saveLogin(username, password, url);

					} catch (Exception e) {

						e.printStackTrace();

						showMessage(e.getMessage(), "Save Login Details Error", JOptionPane.ERROR_MESSAGE);

					}
				}

				// Go to the next Panel
				navigator.next(frmNetflowAnalyser.getContentPane());

			}
		});

		btnLogin.setForeground(new Color(139, 69, 19));
		btnLogin.setBackground(SystemColor.inactiveCaptionBorder);
		btnLogin.setFont(new Font("MS Reference Sans Serif", Font.PLAIN, 16));
		btnLogin.setBounds(346, 430, 117, 29);
		mainPanel.add(btnLogin);

		txtUsername = new JTextField();
		txtUsername.setBounds(377, 115, 246, 29);
		mainPanel.add(txtUsername);
		txtUsername.setColumns(10);

		txtDatabaseURL = new JTextField();
		txtDatabaseURL.setColumns(10);
		txtDatabaseURL.setBounds(377, 235, 246, 29);
		mainPanel.add(txtDatabaseURL);

		txtPassword = new JPasswordField();
		txtPassword.setBounds(377, 176, 246, 28);
		mainPanel.add(txtPassword);

		JPanel chooseAlgorithmPanel = new JPanel();
		frmNetflowAnalyser.getContentPane().add(chooseAlgorithmPanel, "name_1406288456621612");
		chooseAlgorithmPanel.setLayout(null);

		JButton btnFeatureAnalysis = new JButton("Feature-based Analysis");
		btnFeatureAnalysis.addActionListener(new ActionListener() {
			// Move to the clustering panel
			public void actionPerformed(ActionEvent arg0) {

				navigator.next(frmNetflowAnalyser.getContentPane());

				currentAlgorithm = "name_1407032050802588";

				cmBxtables.setModel(new DefaultComboBoxModel<String>(tablesNames));
			}
		});
		btnFeatureAnalysis.setFont(new Font("MS Reference Sans Serif", Font.PLAIN, 16));
		btnFeatureAnalysis.setBounds(254, 141, 276, 78);
		chooseAlgorithmPanel.add(btnFeatureAnalysis);

		JButton btnVolumeAnalysis = new JButton("Volume-based Analysis");
		btnVolumeAnalysis.addActionListener(new ActionListener() {
			// Go the Wavelet Analysis Panel
			public void actionPerformed(ActionEvent arg0) {
				navigator.show(frmNetflowAnalyser.getContentPane(), "name_1589154443837015");
				currentAlgorithm = "name_1589154443837015";
			}
		});
		btnVolumeAnalysis.setFont(new Font("MS Reference Sans Serif", Font.PLAIN, 16));
		btnVolumeAnalysis.setBounds(254, 297, 276, 78);
		chooseAlgorithmPanel.add(btnVolumeAnalysis);

		JLabel lblChooseAnAlgorithm = new JLabel("Choose an Algorithm");
		lblChooseAnAlgorithm.setForeground(Color.BLUE);
		lblChooseAnAlgorithm.setFont(new Font("MS Reference Sans Serif", Font.BOLD, 19));
		lblChooseAnAlgorithm.setBounds(267, 39, 256, 61);
		chooseAlgorithmPanel.add(lblChooseAnAlgorithm);

		JButton btnLogout = new JButton("Logout");
		btnLogout.addActionListener(new ActionListener() {

			// Log the user out and delete saved login details
			public void actionPerformed(ActionEvent arg0) {

				// Delete login details
				try {

					if(Files.deleteIfExists(new File("./resource/login").toPath())) {
						System.out.println("Saved Login details have been deleted");
					}

				} catch (IOException e) {

					e.printStackTrace();

					showMessage(e.getMessage(), "Logout Error", JOptionPane.ERROR_MESSAGE);

				}

				finally {
					// Clear all details
					sqlConnection = null;
					tablesNames = null;
					txtUsername.setText("");
					txtPassword.setText("");
					txtDatabaseURL.setText("");

					// Go back to login panel
					navigator.first(frmNetflowAnalyser.getContentPane());
				}

			}
		});
		btnLogout.setFont(new Font("MS Reference Sans Serif", Font.PLAIN, 16));
		btnLogout.setBounds(339, 463, 103, 29);
		chooseAlgorithmPanel.add(btnLogout);

		final JPanel clusteringPanel = new JPanel();
		clusteringPanel.setForeground(new Color(0, 0, 255));
		frmNetflowAnalyser.getContentPane().add(clusteringPanel, "name_1407032050802588");
		clusteringPanel.setLayout(null);

		JLabel lblEntropyWeightedKmeans = new JLabel("Entropy Weighted KMeans Clustering Algorithm");
		lblEntropyWeightedKmeans.setForeground(Color.BLUE);
		lblEntropyWeightedKmeans.setFont(new Font("MS Reference Sans Serif", Font.BOLD, 16));
		lblEntropyWeightedKmeans.setBounds(148, 11, 462, 32);
		clusteringPanel.add(lblEntropyWeightedKmeans);

		JLabel lblChooseTable = new JLabel("Choose Table");
		lblChooseTable.setFont(new Font("MS Reference Sans Serif", Font.PLAIN, 16));
		lblChooseTable.setBounds(83, 50, 120, 32);
		clusteringPanel.add(lblChooseTable);

		JSeparator separator = new JSeparator();
		separator.setBounds(30, 175, 723, 2);
		clusteringPanel.add(separator);

		rdbtnChooseFeatures = new JRadioButton("Choose Features");
		rdbtnChooseFeatures.addChangeListener(new ChangeListener() {

			// When selecting or unselecting choose features
			@Override
			public void stateChanged(ChangeEvent arg0) {

				JRadioButton rd =(JRadioButton) arg0.getSource();

				if (chooseFeaturesPanel == null) return;

				boolean enabled = rd.isSelected();

				// Enable or disable the chooseFeatures Panel accordingly
				chooseFeaturesPanel.setEnabled(enabled);

				Component[] components = chooseFeaturesPanel.getComponents();

				for(int i = 0; i < components.length; i++) {
					components[i].setEnabled(enabled);
				}

				if (!chckbxEnableDimensionalityReduction.isSelected()) {
					cmBxNoDimensions.setEnabled(false);
				}

			}
		});

		buttonGroup.add(rdbtnChooseFeatures);
		rdbtnChooseFeatures.setSelected(true);
		rdbtnChooseFeatures.setFont(new Font("MS Reference Sans Serif", Font.PLAIN, 16));
		rdbtnChooseFeatures.setBounds(83, 188, 181, 23);
		clusteringPanel.add(rdbtnChooseFeatures);

		chooseFeaturesPanel = new JPanel();
		chooseFeaturesPanel.setBorder(new LineBorder(new Color(0, 0, 255), 1, true));
		chooseFeaturesPanel.setBounds(83, 218, 670, 190);
		clusteringPanel.add(chooseFeaturesPanel);
		chooseFeaturesPanel.setLayout(null);

		JCheckBox chckbxStartTime = new JCheckBox("Start Time");
		chckbxStartTime.setBackground(new Color(224, 255, 255));
		chckbxStartTime.setFont(new Font("MS Reference Sans Serif", Font.PLAIN, 16));
		chckbxStartTime.setSelected(true);
		chckbxStartTime.setEnabled(true);
		chckbxStartTime.setBounds(27, 9, 130, 23);
		chooseFeaturesPanel.add(chckbxStartTime);

		JCheckBox chckbxBytes = new JCheckBox("Bytes");
		chckbxBytes.setSelected(true);
		chckbxBytes.setFont(new Font("MS Reference Sans Serif", Font.PLAIN, 16));
		chckbxBytes.setEnabled(true);
		chckbxBytes.setBackground(new Color(224, 255, 255));
		chckbxBytes.setBounds(182, 9, 130, 23);
		chooseFeaturesPanel.add(chckbxBytes);

		JCheckBox chckbxPackets = new JCheckBox("Packets");
		chckbxPackets.setSelected(true);
		chckbxPackets.setFont(new Font("MS Reference Sans Serif", Font.PLAIN, 16));
		chckbxPackets.setEnabled(true);
		chckbxPackets.setBackground(new Color(224, 255, 255));
		chckbxPackets.setBounds(334, 9, 130, 23);
		chooseFeaturesPanel.add(chckbxPackets);

		JCheckBox chckbxSourceIP = new JCheckBox("Source IP");
		chckbxSourceIP.setSelected(true);
		chckbxSourceIP.setFont(new Font("MS Reference Sans Serif", Font.PLAIN, 16));
		chckbxSourceIP.setEnabled(true);
		chckbxSourceIP.setBackground(new Color(224, 255, 255));
		chckbxSourceIP.setBounds(490, 9, 130, 23);
		chooseFeaturesPanel.add(chckbxSourceIP);

		JCheckBox chckbxDestinationIP = new JCheckBox("Destination IP");
		chckbxDestinationIP.setSelected(true);
		chckbxDestinationIP.setFont(new Font("MS Reference Sans Serif", Font.PLAIN, 14));
		chckbxDestinationIP.setEnabled(true);
		chckbxDestinationIP.setBackground(new Color(224, 255, 255));
		chckbxDestinationIP.setBounds(27, 37, 130, 23);
		chooseFeaturesPanel.add(chckbxDestinationIP);

		JCheckBox chckbxProtocol = new JCheckBox("Protocol");
		chckbxProtocol.setSelected(true);
		chckbxProtocol.setFont(new Font("MS Reference Sans Serif", Font.PLAIN, 16));
		chckbxProtocol.setEnabled(true);
		chckbxProtocol.setBackground(new Color(224, 255, 255));
		chckbxProtocol.setBounds(182, 37, 130, 23);
		chooseFeaturesPanel.add(chckbxProtocol);

		JCheckBox chckbxApplication = new JCheckBox("Application");
		chckbxApplication.setSelected(true);
		chckbxApplication.setFont(new Font("MS Reference Sans Serif", Font.PLAIN, 16));
		chckbxApplication.setEnabled(true);
		chckbxApplication.setBackground(new Color(224, 255, 255));
		chckbxApplication.setBounds(334, 37, 130, 23);
		chooseFeaturesPanel.add(chckbxApplication);

		JCheckBox chckbxSourcePort = new JCheckBox("Source Port");
		chckbxSourcePort.setSelected(true);
		chckbxSourcePort.setFont(new Font("MS Reference Sans Serif", Font.PLAIN, 16));
		chckbxSourcePort.setEnabled(true);
		chckbxSourcePort.setBackground(new Color(224, 255, 255));
		chckbxSourcePort.setBounds(490, 37, 130, 23);
		chooseFeaturesPanel.add(chckbxSourcePort);

		JCheckBox chckbxDestinationPort = new JCheckBox("Destination Port");
		chckbxDestinationPort.setSelected(true);
		chckbxDestinationPort.setFont(new Font("MS Reference Sans Serif", Font.PLAIN, 12));
		chckbxDestinationPort.setEnabled(true);
		chckbxDestinationPort.setBackground(new Color(224, 255, 255));
		chckbxDestinationPort.setBounds(27, 65, 130, 23);
		chooseFeaturesPanel.add(chckbxDestinationPort);

		JCheckBox chckbxNextHop = new JCheckBox("Next Hop");
		chckbxNextHop.setSelected(true);
		chckbxNextHop.setFont(new Font("MS Reference Sans Serif", Font.PLAIN, 16));
		chckbxNextHop.setEnabled(true);
		chckbxNextHop.setBackground(new Color(224, 255, 255));
		chckbxNextHop.setBounds(182, 65, 130, 23);
		chooseFeaturesPanel.add(chckbxNextHop);

		JCheckBox chckbxSourceInterface = new JCheckBox("Source Interface");
		chckbxSourceInterface.setSelected(true);
		chckbxSourceInterface.setFont(new Font("MS Reference Sans Serif", Font.PLAIN, 12));
		chckbxSourceInterface.setEnabled(true);
		chckbxSourceInterface.setBackground(new Color(224, 255, 255));
		chckbxSourceInterface.setBounds(334, 65, 130, 23);
		chooseFeaturesPanel.add(chckbxSourceInterface);

		JCheckBox chckbxDestinationInterface = new JCheckBox("Destination Interface");
		chckbxDestinationInterface.setSelected(true);
		chckbxDestinationInterface.setFont(new Font("MS Reference Sans Serif", Font.PLAIN, 9));
		chckbxDestinationInterface.setEnabled(true);
		chckbxDestinationInterface.setBackground(new Color(224, 255, 255));
		chckbxDestinationInterface.setBounds(490, 65, 133, 23);
		chooseFeaturesPanel.add(chckbxDestinationInterface);

		chckbxEnableDimensionalityReduction = new JCheckBox("Enable Dimensionality Reduction");
		chckbxEnableDimensionalityReduction.addActionListener(new ActionListener() {
			// Choose to reduce dimensions or not
			public void actionPerformed(ActionEvent arg0) {
				JCheckBox chBx = (JCheckBox) arg0.getSource();
				if (cmBxNoDimensions == null) return;
				cmBxNoDimensions.setEnabled(chBx.isSelected());
			}
		});
		chckbxEnableDimensionalityReduction.setForeground(new Color(0, 100, 0));
		chckbxEnableDimensionalityReduction.setFont(new Font("MS Reference Sans Serif", Font.PLAIN, 16));
		chckbxEnableDimensionalityReduction.setBackground(new Color(189, 183, 107));
		chckbxEnableDimensionalityReduction.setBounds(27, 98, 314, 23);
		chooseFeaturesPanel.add(chckbxEnableDimensionalityReduction);

		JCheckBox chckbxSqlQuery = new JCheckBox("Write a SQL clause");
		chckbxSqlQuery.addActionListener(new ActionListener() {

			// Choose to write a Sql clause or not
			public void actionPerformed(ActionEvent e) {
				JCheckBox chBx = (JCheckBox) e.getSource();
				if (txtSQLQuery == null) return;
				txtSQLQuery.setEditable(chBx.isSelected());
			}
		});
		chckbxSqlQuery.setForeground(new Color(0, 100, 0));
		chckbxSqlQuery.setFont(new Font("MS Reference Sans Serif", Font.PLAIN, 16));
		chckbxSqlQuery.setBackground(new Color(189, 183, 107));
		chckbxSqlQuery.setBounds(27, 128, 185, 23);
		chooseFeaturesPanel.add(chckbxSqlQuery);

		cmBxNoDimensions = new JComboBox<>();
		cmBxNoDimensions.setModel(new DefaultComboBoxModel<String>(
				new String[] {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11"}));
		cmBxNoDimensions.setEnabled(false);
		cmBxNoDimensions.setToolTipText("Choose number of dimensions");
		cmBxNoDimensions.setFont(new Font("MS Reference Sans Serif", Font.PLAIN, 16));
		cmBxNoDimensions.setBounds(361, 97, 113, 24);
		chooseFeaturesPanel.add(cmBxNoDimensions);

		cmBxLabel = new JComboBox<>();
		cmBxLabel.setModel(new DefaultComboBoxModel<FEATURES>(FEATURES.values()));
		cmBxLabel.setToolTipText("Choose the label");
		cmBxLabel.setFont(new Font("MS Reference Sans Serif", Font.PLAIN, 16));
		cmBxLabel.setBounds(508, 97, 113, 24);
		chooseFeaturesPanel.add(cmBxLabel);

		txtSQLQuery = new JTextField();
		txtSQLQuery.setText("where sip between 167772160 and 184549375 and sp != 80");
		txtSQLQuery.setEditable(false);
		txtSQLQuery.setFont(new Font("Courier New", Font.PLAIN, 11));
		txtSQLQuery.setBounds(218, 126, 402, 26);
		chooseFeaturesPanel.add(txtSQLQuery);
		txtSQLQuery.setColumns(10);

		chckbxNormalise = new JCheckBox("Normalise Data");
		chckbxNormalise.setForeground(new Color(0, 100, 0));
		chckbxNormalise.setFont(new Font("MS Reference Sans Serif", Font.PLAIN, 16));
		chckbxNormalise.setBackground(new Color(189, 183, 107));
		chckbxNormalise.setBounds(27, 158, 185, 23);
		chooseFeaturesPanel.add(chckbxNormalise);

		JRadioButton rdbtnExtractFeaturesFor = new JRadioButton("Extract Features for Network and Port Scan");
		buttonGroup.add(rdbtnExtractFeaturesFor);
		rdbtnExtractFeaturesFor.addChangeListener(new ChangeListener() {
			// Choose to select a range of source IPs
			public void stateChanged(ChangeEvent e)  {
				JRadioButton rdBt = (JRadioButton) e.getSource();
				if (txtSipRange == null || txtNetworkScanThreshold == null || txtPortScanThreshold == null) return;
				txtSipRange.setEditable(rdBt.isSelected());
				txtNetworkScanThreshold.setEditable(rdBt.isSelected());
				txtPortScanThreshold.setEditable(rdBt.isSelected());

			}
		});
		rdbtnExtractFeaturesFor.setFont(new Font("MS Reference Sans Serif", Font.PLAIN, 16));
		rdbtnExtractFeaturesFor.setBounds(83, 421, 384, 24);
		clusteringPanel.add(rdbtnExtractFeaturesFor);

		cmBxtables = new JComboBox<>();
		cmBxtables.setBounds(202, 50, 227, 32);
		clusteringPanel.add(cmBxtables);

		JButton btnRun = new JButton("Run");
		btnRun.addActionListener(new ActionListener() {

			// Run the Clustering Algorithm
			public void actionPerformed(ActionEvent e) {
				runAlgorithm("runClustering", "Kmeans Clustering");
			}
		});

		btnRun.setIcon(new ImageIcon(MainWindow.class
				.getResource("/com/sun/javafx/webkit/prism/resources/mediaPlayDisabled.png")));
		btnRun.setBackground(new Color(245, 255, 250));
		btnRun.setForeground(new Color(0, 102, 255));
		btnRun.setToolTipText("Run the algorithm with the selected features");
		btnRun.setFont(new Font("MS Reference Sans Serif", Font.PLAIN, 16));
		btnRun.setBounds(664, 521, 89, 29);
		clusteringPanel.add(btnRun);

		JButton btnClusterBack = new JButton("Back");
		btnClusterBack.addActionListener(new ActionListener() {

			// Go back to choose Algorithm Panel
			public void actionPerformed(ActionEvent e) {
				navigator.show(frmNetflowAnalyser.getContentPane(), "name_1406288456621612");
			}
		});
		btnClusterBack.setFont(new Font("MS Reference Sans Serif", Font.PLAIN, 16));
		btnClusterBack.setBounds(10, 521, 89, 29);
		clusteringPanel.add(btnClusterBack);

		limitSpinner = new JSpinner();
		limitSpinner.setFont(new Font("MS Reference Sans Serif", Font.PLAIN, 16));
		limitSpinner.setModel(new SpinnerNumberModel(10000, 1, 10000000, 1));
		limitSpinner.setBounds(599, 50, 154, 32);
		clusteringPanel.add(limitSpinner);

		JLabel lblMaximumRows = new JLabel("Maximum Rows");
		lblMaximumRows.setFont(new Font("MS Reference Sans Serif", Font.PLAIN, 16));
		lblMaximumRows.setBounds(460, 50, 129, 32);
		clusteringPanel.add(lblMaximumRows);

		txtSipRange = new JTextField();
		txtSipRange.setText("between 167772160 and 184549375 and sp != 80");
		txtSipRange.setEditable(false);
		txtSipRange.setToolTipText("Specify the range of source IPs by using sql syntax");
		txtSipRange.setFont(new Font("Courier New", Font.PLAIN, 13));
		txtSipRange.setBounds(477, 422, 276, 23);
		clusteringPanel.add(txtSipRange);
		txtSipRange.setColumns(10);

		txtPortScanThreshold = new JTextField();
		txtPortScanThreshold.setEditable(false);
		txtPortScanThreshold.setText("85");
		txtPortScanThreshold.setFont(new Font("MS Reference Sans Serif", Font.PLAIN, 16));
		txtPortScanThreshold.setBounds(315, 452, 41, 23);
		clusteringPanel.add(txtPortScanThreshold);
		txtPortScanThreshold.setColumns(10);

		txtNetworkScanThreshold = new JTextField();
		txtNetworkScanThreshold.setEditable(false);
		txtNetworkScanThreshold.setText("6");
		txtNetworkScanThreshold.setFont(new Font("MS Reference Sans Serif", Font.PLAIN, 16));
		txtNetworkScanThreshold.setBounds(609, 452, 41, 23);
		clusteringPanel.add(txtNetworkScanThreshold);
		txtNetworkScanThreshold.setColumns(10);

		JLabel lblPortScanThreshold = new JLabel("Port Scan threshold:");
		lblPortScanThreshold.setFont(new Font("MS Reference Sans Serif", Font.PLAIN, 16));
		lblPortScanThreshold.setBounds(129, 452, 170, 23);
		clusteringPanel.add(lblPortScanThreshold);

		JLabel lblNetworkScanThreshold = new JLabel("Network Scan threshold:");
		lblNetworkScanThreshold.setFont(new Font("MS Reference Sans Serif", Font.PLAIN, 16));
		lblNetworkScanThreshold.setBounds(381, 452, 206, 23);
		clusteringPanel.add(lblNetworkScanThreshold);

		JSeparator separator_5 = new JSeparator();
		separator_5.setOrientation(SwingConstants.VERTICAL);
		separator_5.setBounds(366, 452, 3, 23);
		clusteringPanel.add(separator_5);

		JSeparator separator_7 = new JSeparator();
		separator_7.setOrientation(SwingConstants.VERTICAL);
		separator_7.setBounds(447, 50, 3, 32);
		clusteringPanel.add(separator_7);

		JLabel lblAnomalousClusterMax = new JLabel("Anomalous cluster maximum points:");
		lblAnomalousClusterMax.setFont(new Font("MS Reference Sans Serif", Font.PLAIN, 16));
		lblAnomalousClusterMax.setBounds(83, 93, 309, 32);
		clusteringPanel.add(lblAnomalousClusterMax);

		anomalMaxClusteringSpinner = new JSpinner();
		anomalMaxClusteringSpinner.setToolTipText("The maximum number of points in an anomalous cluster");
		anomalMaxClusteringSpinner.setModel(new SpinnerNumberModel(5, 1, 20, 1));
		anomalMaxClusteringSpinner.setFont(new Font("MS Reference Sans Serif", Font.PLAIN, 16));
		anomalMaxClusteringSpinner.setBounds(388, 93, 41, 32);
		clusteringPanel.add(anomalMaxClusteringSpinner);

		chckbxUpdateClassifierModel = new JCheckBox("Update Classifier Model");
		chckbxUpdateClassifierModel.setFont(new Font("MS Reference Sans Serif", Font.PLAIN, 16));
		chckbxUpdateClassifierModel.setBounds(460, 93, 293, 32);
		clusteringPanel.add(chckbxUpdateClassifierModel);

		JSeparator separator_10 = new JSeparator();
		separator_10.setOrientation(SwingConstants.VERTICAL);
		separator_10.setBounds(447, 93, 3, 32);
		clusteringPanel.add(separator_10);

		rdbtnUseAClassifier = new JRadioButton("Use a Classifier Model");
		buttonGroup.add(rdbtnUseAClassifier);
		rdbtnUseAClassifier.addChangeListener(new ChangeListener() {
			// Use the classifier model
			public void stateChanged(ChangeEvent arg0) {
				JRadioButton rdBt = (JRadioButton) arg0.getSource();
				if (txtSipRangeClassifier == null) return;
				txtSipRangeClassifier.setEditable(rdBt.isSelected());
			}
		});
		rdbtnUseAClassifier.setFont(new Font("MS Reference Sans Serif", Font.PLAIN, 16));
		rdbtnUseAClassifier.setBounds(83, 493, 217, 24);
		clusteringPanel.add(rdbtnUseAClassifier);

		txtSipRangeClassifier = new JTextField();
		txtSipRangeClassifier.setToolTipText("Specify the range of source IPs by using sql syntax");
		txtSipRangeClassifier.setText("between 167772160 and 184549375 and sp != 80");
		txtSipRangeClassifier.setFont(new Font("Courier New", Font.PLAIN, 13));
		txtSipRangeClassifier.setEditable(false);
		txtSipRangeClassifier.setColumns(10);
		txtSipRangeClassifier.setBounds(313, 493, 276, 23);
		clusteringPanel.add(txtSipRangeClassifier);

		JSeparator separator_11 = new JSeparator();
		separator_11.setBounds(30, 482, 723, 2);
		clusteringPanel.add(separator_11);

		JLabel lblMaxNumberOf = new JLabel("Max number of clusters:");
		lblMaxNumberOf.setFont(new Font("MS Reference Sans Serif", Font.PLAIN, 16));
		lblMaxNumberOf.setBounds(83, 132, 258, 32);
		clusteringPanel.add(lblMaxNumberOf);

		maxNoClustersSpinner = new JSpinner();
		maxNoClustersSpinner.setModel(new SpinnerNumberModel(new Integer(10), new Integer(1), null, new Integer(1)));
		maxNoClustersSpinner.setToolTipText("The maximum number of points in an anomalous cluster");
		maxNoClustersSpinner.setFont(new Font("MS Reference Sans Serif", Font.PLAIN, 16));
		maxNoClustersSpinner.setBounds(388, 132, 41, 32);
		clusteringPanel.add(maxNoClustersSpinner);
		
		cmBxModule = new JComboBox<>();
		cmBxModule.setToolTipText("Select the module required");
		cmBxModule.setModel(new DefaultComboBoxModel<MODULES>(MODULES.values()));
		cmBxModule.setBounds(599, 493, 154, 23);
		clusteringPanel.add(cmBxModule);

		JPanel OutputPanel = new JPanel();
		frmNetflowAnalyser.getContentPane().add(OutputPanel, "name_1409905780166629");
		OutputPanel.setLayout(null);

		chartPanel = new JPanel();
		chartPanel.setBackground(new Color(255, 255, 255));
		chartPanel.setBounds(10, 11, 620, 437);
		OutputPanel.add(chartPanel);

		JButton btnOutputBack = new JButton("Back");
		btnOutputBack.addActionListener(new ActionListener() {
			// Go back to the clustering panel
			public void actionPerformed(ActionEvent e) {
				chartPanel.removeAll();
				textAreaAnomalies.setText("");
				navigator.show(frmNetflowAnalyser.getContentPane(), currentAlgorithm);
			}
		});
		btnOutputBack.setFont(new Font("MS Reference Sans Serif", Font.PLAIN, 16));
		btnOutputBack.setBounds(10, 521, 89, 29);
		OutputPanel.add(btnOutputBack);

		JLabel lblAnomalies = new JLabel("Anomalies:");
		lblAnomalies.setFont(new Font("MS Reference Sans Serif", Font.PLAIN, 16));
		lblAnomalies.setBounds(52, 472, 107, 29);
		OutputPanel.add(lblAnomalies);

		JButton btnConsoleOuput = new JButton("Console ouput");
		btnConsoleOuput.addActionListener(new ActionListener() {
			// Open console output externally 
			public void actionPerformed(ActionEvent e) {
				try {
					Desktop.getDesktop().open(new File("./resource/output.txt"));
				} catch (IOException e1) {
					showMessage(e1.getMessage(), "Show Console Output Error", JOptionPane.ERROR_MESSAGE);
					e1.printStackTrace();
				}
			}
		});
		btnConsoleOuput.setFont(new Font("MS Reference Sans Serif", Font.PLAIN, 15));
		btnConsoleOuput.setBounds(625, 471, 149, 37);
		OutputPanel.add(btnConsoleOuput);

		textAreaAnomalies = new JTextArea();
		textAreaAnomalies.setEditable(false);
		textAreaAnomalies.setBounds(184, 459, 431, 64);
		JScrollPane scrollPane = new JScrollPane(textAreaAnomalies);
		scrollPane.setSize(431, 64);
		scrollPane.setLocation(184, 459);
		OutputPanel.add(scrollPane);

		JPanel WaveletPanel = new JPanel();
		frmNetflowAnalyser.getContentPane().add(WaveletPanel, "name_1589154443837015");
		WaveletPanel.setLayout(null);

		spinnerVolumeFromTime = new JSpinner(new SpinnerDateModel());
		spinnerVolumeFromTime.addChangeListener(new ChangeListener() {
			// Set the limit for the finish time
			public void stateChanged(ChangeEvent arg0) {
				JSpinner fromTime =(JSpinner) arg0.getSource();
				if (spinnerVolumeToTime == null) return;
				SpinnerDateModel toModel = (SpinnerDateModel) spinnerVolumeToTime.getModel();
				toModel.setStart((Date) fromTime.getValue());
				toModel.setValue((Date) fromTime.getValue());
			}
		});
		spinnerVolumeFromTime.setToolTipText("Start Hour");
		spinnerVolumeFromTime.setFont(new Font("MS Reference Sans Serif", Font.PLAIN, 16));
		spinnerVolumeFromTime.setBounds(462, 53, 112, 30);
		JSpinner.DateEditor de_spinnerVolumeFromTime = new JSpinner.DateEditor(spinnerVolumeFromTime, "HH");
		spinnerVolumeFromTime.setEditor(de_spinnerVolumeFromTime);
		spinnerVolumeFromTime.setValue(new Date());
		WaveletPanel.add(spinnerVolumeFromTime);

		//------------------------------------------------------------------------------------------------------------
		// Create Date Renderer for formatting Date
		class DateComboBoxRenderer extends DefaultListCellRenderer {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1502386118461987645L;
			// desired format for the date
			private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

			@SuppressWarnings("rawtypes")
			public Component getListCellRendererComponent(JList list, Object value, int index,
					boolean isSelected, boolean cellHasFocus) {
				Object item = value;

				// if the item to be rendered is date then format it
				if (item instanceof Date) {
					item = dateFormat.format((Date) item );
				}
				return super.getListCellRendererComponent(list, item, index, isSelected, cellHasFocus);
			}
		}
		//-------------------------------------------------------------------------------------------------------------

		cmBxVolumeChooseDate = new JComboBox<>();
		Date date = new Date();
		for (int i = 0; i < 30; i++) {
			cmBxVolumeChooseDate.addItem(DateUtils.addDays(date, -i));
		}
		cmBxVolumeChooseDate.setRenderer(new DateComboBoxRenderer());
		spinnerVolumeToTime = new JSpinner(new SpinnerDateModel());
		spinnerVolumeToTime.setToolTipText("End Hour (inclusive)");
		spinnerVolumeToTime.setFont(new Font("MS Reference Sans Serif", Font.PLAIN, 16));
		spinnerVolumeToTime.setBounds(662, 53, 112, 30);
		JSpinner.DateEditor de_spinnerVolumeToTime = new JSpinner.DateEditor(spinnerVolumeToTime, "HH");
		spinnerVolumeToTime.setEditor(de_spinnerVolumeToTime);
		spinnerVolumeToTime.setValue(new Date());
		WaveletPanel.add(spinnerVolumeToTime);
		cmBxVolumeChooseDate.setBounds(180, 53, 178, 30);
		WaveletPanel.add(cmBxVolumeChooseDate);

		JLabel lblChooseDate = new JLabel("Choose Date:");
		lblChooseDate.setFont(new Font("MS Reference Sans Serif", Font.PLAIN, 17));
		lblChooseDate.setBounds(31, 53, 133, 30);
		WaveletPanel.add(lblChooseDate);

		JLabel lblFrom = new JLabel("From");
		lblFrom.setFont(new Font("MS Reference Sans Serif", Font.PLAIN, 17));
		lblFrom.setBounds(391, 53, 55, 30);
		WaveletPanel.add(lblFrom);

		JLabel lblTo = new JLabel("To");
		lblTo.setFont(new Font("MS Reference Sans Serif", Font.PLAIN, 17));
		lblTo.setBounds(619, 53, 33, 30);
		WaveletPanel.add(lblTo);

		JLabel lblSqlClauseQuery = new JLabel("SQL Clause Query:");
		lblSqlClauseQuery.setToolTipText("Write a query to specify the range of values");
		lblSqlClauseQuery.setFont(new Font("MS Reference Sans Serif", Font.PLAIN, 15));
		lblSqlClauseQuery.setBounds(31, 164, 141, 28);
		WaveletPanel.add(lblSqlClauseQuery);

		txtVolumeSqlQuery = new JTextField();
		txtVolumeSqlQuery.setText("WHERE sip = ''");
		txtVolumeSqlQuery.setFont(new Font("Courier New", Font.PLAIN, 16));
		txtVolumeSqlQuery.setBounds(180, 164, 518, 28);
		WaveletPanel.add(txtVolumeSqlQuery);
		txtVolumeSqlQuery.setColumns(10);

		JLabel lblFeature = new JLabel("Feature:");
		lblFeature.setToolTipText("Choose the type of the feature to extract");
		lblFeature.setFont(new Font("MS Reference Sans Serif", Font.PLAIN, 17));
		lblFeature.setBounds(31, 214, 139, 31);
		WaveletPanel.add(lblFeature);

		cmBxVolumeFeatureType = new JComboBox<>();
		cmBxVolumeFeatureType.setModel(new DefaultComboBoxModel<DataType>(DataType.values()));
		cmBxVolumeFeatureType.setBounds(180, 214, 230, 31);
		WaveletPanel.add(cmBxVolumeFeatureType);

		JLabel lblThreshold = new JLabel("Threshold:");
		lblThreshold.setToolTipText("Choose a threshold to classify anomalies");
		lblThreshold.setFont(new Font("MS Reference Sans Serif", Font.PLAIN, 17));
		lblThreshold.setBounds(300, 312, 96, 30);
		WaveletPanel.add(lblThreshold);

		JLabel lblWaveletAnalysis = new JLabel("Wavelet Analysis");
		lblWaveletAnalysis.setFont(new Font("MS Reference Sans Serif", Font.BOLD, 17));
		lblWaveletAnalysis.setBounds(308, 280, 175, 31);
		WaveletPanel.add(lblWaveletAnalysis);

		txtWaveletThreshold = new JTextField();
		txtWaveletThreshold.setText("3");
		txtWaveletThreshold.setFont(new Font("Courier New", Font.PLAIN, 16));
		txtWaveletThreshold.setBounds(420, 312, 86, 30);
		WaveletPanel.add(txtWaveletThreshold);
		txtWaveletThreshold.setColumns(10);

		JButton btnRunWavelet = new JButton("RUN");
		btnRunWavelet.addActionListener(new ActionListener() {
			// Run the wavelet analysis
			public void actionPerformed(ActionEvent e) {
				runAlgorithm("runWavelet", "Analysing Signal");
			}
		});
		btnRunWavelet.setIcon(new ImageIcon(MainWindow.class
				.getResource("/com/sun/javafx/webkit/prism/resources/mediaPlayDisabled.png")));
		btnRunWavelet.setFont(new Font("MS Reference Sans Serif", Font.PLAIN, 16));
		btnRunWavelet.setBounds(607, 312, 139, 30);
		WaveletPanel.add(btnRunWavelet);

		JButton btnVolumeBack = new JButton("Back");
		btnVolumeBack.addActionListener(new ActionListener() {
			// Go back to choose algorithm Panel
			public void actionPerformed(ActionEvent e) {
				navigator.show(frmNetflowAnalyser.getContentPane(), "name_1406288456621612");
			}
		});
		btnVolumeBack.setFont(new Font("MS Reference Sans Serif", Font.PLAIN, 16));
		btnVolumeBack.setBounds(10, 519, 96, 31);
		WaveletPanel.add(btnVolumeBack);

		volumeLimitSpinner = new JSpinner();
		volumeLimitSpinner.setModel(new SpinnerNumberModel(10000, 1, 10000000, 1));
		volumeLimitSpinner.setFont(new Font("MS Reference Sans Serif", Font.PLAIN, 16));
		volumeLimitSpinner.setBounds(180, 116, 139, 28);
		WaveletPanel.add(volumeLimitSpinner);

		JLabel lblWaveletMaximumRows = new JLabel("Maximum Rows:");
		lblWaveletMaximumRows.setFont(new Font("MS Reference Sans Serif", Font.PLAIN, 16));
		lblWaveletMaximumRows.setBounds(31, 116, 139, 28);
		WaveletPanel.add(lblWaveletMaximumRows);

		chckbxApplyAWindow = new JCheckBox("Apply a window function");
		chckbxApplyAWindow.setFont(new Font("MS Reference Sans Serif", Font.PLAIN, 16));
		chckbxApplyAWindow.setBounds(31, 312, 230, 30);
		WaveletPanel.add(chckbxApplyAWindow);

		JLabel lblFrameSize = new JLabel("Frame size:");
		lblFrameSize.setToolTipText("Choose a threshold to classify anomalies");
		lblFrameSize.setFont(new Font("MS Reference Sans Serif", Font.PLAIN, 17));
		lblFrameSize.setBounds(530, 214, 112, 31);
		WaveletPanel.add(lblFrameSize);

		spinnerVolumeFrameSize = new JSpinner();
		spinnerVolumeFrameSize.setModel(new SpinnerNumberModel(5, 1, 30, 1));
		spinnerVolumeFrameSize.setFont(new Font("MS Reference Sans Serif", Font.PLAIN, 16));
		spinnerVolumeFrameSize.setBounds(647, 214, 47, 31);
		WaveletPanel.add(spinnerVolumeFrameSize);

		JLabel lblVolumebasedAnalysis = new JLabel("Volume-based Analysis");
		lblVolumebasedAnalysis.setForeground(Color.BLUE);
		lblVolumebasedAnalysis.setFont(new Font("MS Reference Sans Serif", Font.BOLD, 17));
		lblVolumebasedAnalysis.setBounds(274, 11, 247, 31);
		WaveletPanel.add(lblVolumebasedAnalysis);

		JSeparator separator_1 = new JSeparator();
		separator_1.setBounds(10, 267, 764, 2);
		WaveletPanel.add(separator_1);

		JSeparator separator_2 = new JSeparator();
		separator_2.setBounds(10, 377, 764, 2);
		WaveletPanel.add(separator_2);

		JLabel lblStatisticalProcessControl = new JLabel("Statistical Process Control Analysis");
		lblStatisticalProcessControl.setFont(new Font("MS Reference Sans Serif", Font.BOLD, 17));
		lblStatisticalProcessControl.setBounds(210, 390, 369, 31);
		WaveletPanel.add(lblStatisticalProcessControl);

		JLabel lblControlChart = new JLabel("Control Chart:");
		lblControlChart.setToolTipText("Choose a threshold to classify anomalies");
		lblControlChart.setFont(new Font("MS Reference Sans Serif", Font.PLAIN, 17));
		lblControlChart.setBounds(10, 435, 141, 28);
		WaveletPanel.add(lblControlChart);

		cmbxSPCChartType = new JComboBox<>();
		cmbxSPCChartType.setFont(new Font("MS Reference Sans Serif", Font.PLAIN, 16));
		cmbxSPCChartType.setModel(new DefaultComboBoxModel<String>(new String[] {"EWMA Chart",
				"EWMA and Shewhart-syle Chart", "Standard Control Chart"}));
		cmbxSPCChartType.setBounds(161, 435, 268, 28);
		WaveletPanel.add(cmbxSPCChartType);

		JLabel lblLambda = new JLabel("λ:  ");
		lblLambda.setToolTipText("The ratio of weigth of new data to historical data");
		lblLambda.setFont(new Font("MS Reference Sans Serif", Font.PLAIN, 17));
		lblLambda.setBounds(468, 435, 30, 28);
		WaveletPanel.add(lblLambda);

		txtSPCLambda = new JTextField();
		txtSPCLambda.setText("0.2");
		txtSPCLambda.setFont(new Font("Courier New", Font.PLAIN, 16));
		txtSPCLambda.setColumns(10);
		txtSPCLambda.setBounds(507, 435, 55, 28);
		WaveletPanel.add(txtSPCLambda);

		JLabel label = new JLabel("Threshold:");
		label.setToolTipText("Choose a threshold to classify anomalies");
		label.setFont(new Font("MS Reference Sans Serif", Font.PLAIN, 17));
		label.setBounds(585, 435, 96, 28);
		WaveletPanel.add(label);

		txtSPCThreshold = new JTextField();
		txtSPCThreshold.setText("3");
		txtSPCThreshold.setFont(new Font("Courier New", Font.PLAIN, 16));
		txtSPCThreshold.setColumns(10);
		txtSPCThreshold.setBounds(691, 435, 55, 28);
		WaveletPanel.add(txtSPCThreshold);

		JButton btnSPCRun = new JButton("RUN");
		btnSPCRun.addActionListener(new ActionListener() {
			// Run the SPC algorithm
			public void actionPerformed(ActionEvent e) {
				runAlgorithm("runSPC", "Analysing Signal");
			}
		});
		btnSPCRun.setIcon(new ImageIcon(MainWindow.class
				.getResource("/com/sun/javafx/webkit/prism/resources/mediaPlayDisabled.png")));
		btnSPCRun.setFont(new Font("MS Reference Sans Serif", Font.PLAIN, 16));
		btnSPCRun.setBounds(607, 490, 139, 30);
		WaveletPanel.add(btnSPCRun);

		JSeparator separator_3 = new JSeparator();
		separator_3.setOrientation(SwingConstants.VERTICAL);
		separator_3.setBounds(607, 53, 2, 30);
		WaveletPanel.add(separator_3);

		JSeparator separator_4 = new JSeparator();
		separator_4.setOrientation(SwingConstants.VERTICAL);
		separator_4.setBounds(379, 53, 2, 30);
		WaveletPanel.add(separator_4);

		JSeparator separator_6 = new JSeparator();
		separator_6.setOrientation(SwingConstants.VERTICAL);
		separator_6.setBounds(274, 312, 2, 30);
		WaveletPanel.add(separator_6);

		JSeparator separator_8 = new JSeparator();
		separator_8.setOrientation(SwingConstants.VERTICAL);
		separator_8.setBounds(572, 433, 2, 30);
		WaveletPanel.add(separator_8);

		JSeparator separator_9 = new JSeparator();
		separator_9.setOrientation(SwingConstants.VERTICAL);
		separator_9.setBounds(444, 432, 2, 30);
		WaveletPanel.add(separator_9);
	}

	/**
	 * Run the Statistical Control Process analysis and display the output
	 */
	public void runSPC() {

		Tuple2<long[], List<Tuple2<Integer, Double>>> data = extractVolumeData();

		// Get the processed data to be used
		List<Tuple2<Integer, Double>> featureVector = data._2();

		// Get the x-axis data that represents the time
		long[] times = data._1();

		// Initialise the spc algorithm
		StatisticalProcessControl spc = new StatisticalProcessControl();

		Map<String, Object> output;

		// Get the chart type selected
		String chartType = String.valueOf(cmbxSPCChartType.getSelectedItem());

		// The output points to be plotted
		Map<String, double[]> points = new HashMap<>();

		// Get the correct Control chart used
		boolean improved = true;
		if (chartType.equals("EWMA Chart")) {
			improved = false;
		} 

		output = spc.computeEWMAChart((int) spinnerVolumeFrameSize.getValue(), featureVector,
				Double.parseDouble(txtSPCLambda.getText()), Double.parseDouble(txtSPCThreshold.getText()), improved);

		// Extract original and normalised Signals from the output
		points.put("Original Signal", (double[]) output.get("Original Signal"));


		// Depending on the chart type
		if (chartType.equals("Standard Control Chart")) {

			output = spc.computeStandardChart(featureVector);

			points.put("Normalised Signal", (double[]) output.get("Normalised Signal"));

		} else {

			// Extract Averaged, UCL and LCL Signals from the output
			points.put("Averaged Signal", (double[]) output.get("Averaged Signal"));
			points.put("Upper Central Limitl", (double[]) output.get("Upper Central Limit"));
			points.put("Lower Central Limit", (double[]) output.get("Lower Central Limit"));

		}

		// Get the anomalies
		Date[] anomalies = (Date[]) output.get("Anomalies");

		// Format the anomalies to be displayed
		String anomaliesTxt = "";

		for (Date date : anomalies) {
			anomaliesTxt += date.toString() + "\n";
		}

		// Display the anomalies
		textAreaAnomalies.setText(anomaliesTxt);

		// Clear the chartPanel to plot a new chart
		chartPanel.removeAll();

		// Create a chartPlotter to plot the anomalies
		ChartPlotter chart = new ChartPlotter("Statistical Process Control Analysis", 600, 400);

		DataType type = (FeatureExtraction.DataType) cmBxVolumeFeatureType.getSelectedItem();

		// Get the y-axis label
		String yLabel = (type == DataType.BYTE) ? "K" + type.toString() : type.toString();

		// Add the chart to the chart panel
		chartPanel.add(chart.sketchTimeSeries(points, yLabel, times,
				"Statistical Process Control Analysis", false), BorderLayout.CENTER);

		chartPanel.repaint();

	}

	/**
	 * Extract the data required by volume-base analysis algorithms
	 * @return
	 */
	private Tuple2<long[], List<Tuple2<Integer, Double>>> extractVolumeData() {
		String day =  new SimpleDateFormat("yy MM dd")
				.format((Date) cmBxVolumeChooseDate.getSelectedItem()).replace(" ", "");
		SimpleDateFormat hourFormatter = new SimpleDateFormat("HH");
		int startHour = Integer.parseInt(hourFormatter.format((Date) spinnerVolumeFromTime.getValue()));
		int endHour = Integer.parseInt(hourFormatter.format((Date) spinnerVolumeToTime.getValue()));
		String sqlClause = txtVolumeSqlQuery.getText();

		int limit = (int) volumeLimitSpinner.getValue();

		// Get the specified data
		Dataset<Row> sqlData = sqlConnection.option("dbtable", "(select st, pck, bytes from h1_" + day + 
				String.format("%02d", startHour) + " " + sqlClause + " limit 0, " + limit 
				+ ") as wavlet_data" ).load();

		limit -= sqlData.count();

		for (int i = startHour + 1; i <= endHour; i++) {
			if (limit < 1) break;
			Dataset<Row> tempData = sqlConnection.option("dbtable", "(select st, pck, bytes from h1_" + day + 
					String.format("%02d", i) + " " + sqlClause + " limit 0, " + limit 
					+ ") as wavlet_data" ).load();
			sqlData = sqlData.union(tempData);
			limit -= sqlData.count();
		}

		sqlData.show();

		System.out.println("The total number of rows in the data " + sqlData.count());

		// Get the feature type
		DataType type = (FeatureExtraction.DataType) cmBxVolumeFeatureType.getSelectedItem();

		List<Tuple2<Integer, Double>> featureVector = FeatureExtraction
				.extractVolumeFeatures(sqlData.toJavaRDD(), 0, type);

		System.out.println("The number of points in the training set " + featureVector.size());
		System.out.println("The first training point " + featureVector.get(0));

		// Create the time vector
		long[] times = new long[featureVector.size()];

		for (int i = 0; i < times.length; i++) {
			times[i] = featureVector.get(i)._1();
		}

		return new Tuple2<long[], List<Tuple2<Integer, Double>>>(times, featureVector);
	}

	/**
	 * Run the Wavelet analysis and display the output
	 */
	public void runWavelet() {

		Tuple2<long[], List<Tuple2<Integer, Double>>> data = extractVolumeData();

		// Get the processed data to be used
		List<Tuple2<Integer, Double>> featureVector = data._2();

		// Get the x-axis data that represents the time
		long[] times = data._1();

		// Initialise the wavelet algorithm
		WaveletAnalysis wavelet = new WaveletAnalysis();

		Map<String, Object> output = wavelet.analyseSignal(featureVector,
				Double.parseDouble(txtWaveletThreshold.getText()), (int) spinnerVolumeFrameSize.getValue(),
				chckbxApplyAWindow.isSelected());
		double[] originalSignal = (double[]) output.get("Original Signal");
		double[] composedSignal = (double[]) output.get("Band 1 Signal");
		double[] vSignal = (double[]) output.get("Variability Signal");
		Date[] anomalies = (Date[]) output.get("Anomalies");

		Map<String, double[]> points = new HashMap<>();
		points.put("Original Signal", originalSignal);

		String anomaliesTxt = "";

		for (Date date : anomalies) {
			anomaliesTxt += date.toString() + "\n";
		}

		// Display the anomalies
		textAreaAnomalies.setText(anomaliesTxt);

		// Clear the chartPanel to plot a new chart
		chartPanel.removeAll();

		// Create a chartPlotter to plot the anomalies
		ChartPlotter chart = new ChartPlotter("Wavelet Analysis", 600, 200);
		chartPanel.setLayout(new FlowLayout());

		DataType type = (FeatureExtraction.DataType) cmBxVolumeFeatureType.getSelectedItem();

		// Get the y-axis label
		String yLabel = (type == DataType.BYTE) ? "K" + type.toString() : type.toString();

		chartPanel.add(chart.sketchTimeSeries(points, yLabel, times, "Original Signal", false));

		chart.setSize(300, 200);

		points.clear();
		points.put("Band 1 Signal", composedSignal);
		chartPanel.add(chart.sketchTimeSeries(points, yLabel, times, "Band 1 Signal", false));

		points.clear();
		points.put("Variability Signal", vSignal);
		chartPanel.add(chart.sketchTimeSeries(points, yLabel, times, "Variability Signal", false));
	}

	/**
	 * Run a specify algorithm by invoking the method of the algorithm
	 * on the background and display a progress bar
	 * @param methodName of the algorithm to be run
	 * @param keyword that describe the algorithm e.g clustering
	 */
	private void runAlgorithm(String methodName, String keyword) {

		Method method;

		// Try getting the algorithm method
		try {
			method = this.getClass().getMethod(methodName);	
		} catch (NoSuchMethodException | SecurityException e) {
			e.printStackTrace();
			showMessage(e.getMessage(), "Get Algorithm Method Error", JOptionPane.ERROR_MESSAGE);
			return;
		}

		final Method algorithm = method;

		final MainWindow mainWindow = this;

		// A dialog to show a progress bar while waiting
		final JDialog dialog = new JDialog(frmNetflowAnalyser, keyword + "...", true);

		// Create a task worker to do the task
		class Task extends SwingWorker<Void, Void> {

			@Override // May throw an exception
			protected Void doInBackground() throws Exception {
				setProgress(0);
				// The actual task
				algorithm.invoke(mainWindow);
				// End of the task
				setProgress(100);
				return null;
			}

			@Override
			public void done() {
				// When done
				Toolkit.getDefaultToolkit().beep();
				// Dispose the progress bar
				dialog.dispose();
			}
		}

		// Try to execute the task
		Task task = new Task();
		task.execute();

		// Build the GUI of the progress bar and the dialog
		final JProgressBar aJProgressBar = new JProgressBar(0, 100);
		aJProgressBar.setStringPainted(false);
		aJProgressBar.setForeground(Color.BLUE);
		aJProgressBar.setIndeterminate(true);
		aJProgressBar.setVisible(true);
		JPanel panel = new JPanel();
		panel.add(aJProgressBar, BorderLayout.SOUTH);

		dialog.setContentPane(panel);
		dialog.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		dialog.setLocationRelativeTo(null);
		dialog.setSize(300, 100);
		dialog.setVisible(true);

		// Go the Output Panel if no error
		try {
			task.get();
		} catch (Exception e1) { // All exceptions are handled here
			e1.printStackTrace();
			showMessage(e1.getCause().getCause().getMessage(), keyword + " Error", JOptionPane.ERROR_MESSAGE);
			return;
		} 
		navigator.show(frmNetflowAnalyser.getContentPane(), "name_1409905780166629");

	}

	/**
	 * Run the clustering algorithm and display the output
	 * @throws Exception related to SQLException or from one of the algorithms
	 */
	public void runClustering() throws Exception {
		// Get the selected table
		String table = (String) cmBxtables.getSelectedItem();

		// No of dimensions of the output data
		int noDimensions = 0;

		// Are features chosen manually?
		boolean manualFeatures = rdbtnChooseFeatures.isSelected();

		String sqlClause = "";

		String features = "";
		int labelIndex = 0;
		boolean excludeLabel = false;

		// Components required to get the user preferences
		JCheckBox reduceDimension = null;
		JCheckBox sqlQuery = null;
		JComboBox<String> dimension = cmBxNoDimensions;
		JComboBox<FEATURES> labels = cmBxLabel;
		
		// Pick the label to be used to categorise the data
		String label = ((FEATURES) labels.getSelectedItem()).name();
		
		// If it's volume analysis
		boolean volumeAnalysis = false;

		// Get the chosen features
		if(manualFeatures) {

			Component[] components = chooseFeaturesPanel.getComponents();

			// Add the label to the sql query
			features += label + ", ";

			for(int i = 0; i < components.length; i++) {

				if(components[i].getClass().getName() == "javax.swing.JCheckBox") {

					JCheckBox feature = (JCheckBox) components[i];

					if (i == 12) reduceDimension = feature;
					if (i == 13) sqlQuery = feature;

					if (!feature.isSelected()) continue;

					if (i < 12) {

						noDimensions++; // increase the number of dimension

						// Get the columns/feature name selected
						String name = FEATURES.getIndex(i).name();

						// If the column name is pck or bytes we want the sum
						if (name.equals("pck") || name.equals("bytes")) {

							features += "max(p"+name+"), ";
							
							volumeAnalysis = true;

							// Otherwise we want the number of distinct elements	
						} else {

							features += "count(DISTINCT " + name + "), ";

						}
					}
				}
			}

			// Remove the comma and space at the end of the string
			features = features.substring(0, features.length() - 2);

			// Is dimensionality reduction enabled?
			if (reduceDimension.isSelected()) {
				noDimensions = Integer.parseInt((String) dimension.getSelectedItem());
			}

			for (String txt : features.split(", ")) {
				if (txt.equals(label)) break;
				labelIndex++;
			}

			// Set the index of the label and make sure it is not part of the values 
			labelIndex = 0;
			excludeLabel = true;

			if (sqlQuery.isSelected()) sqlClause = txtSQLQuery.getText();

		} else {

			// Get the source ip range from the classifier panel or feature extraction panel
			String txt = "";
			if (rdbtnUseAClassifier.isSelected()) {
				txt = txtSipRangeClassifier.getText();
			} else {
				txt = txtSipRange.getText();
			}

			sqlClause = (txt == null || txt.equals("")) ? "" : "WHERE sip " + txt;
			features = "sip, dip, dp";
		}

		JavaRDD<Row> data = null;

		String xAxis = "";

		String yAxis = "";

		// Now Initialise the KMeans algorithms
		KMeansClustering kmeans = new KMeansClustering();

		// Set the maximum number of points in an anomalous cluster
		kmeans.setAnomalousMax((int) anomalMaxClusteringSpinner.getValue());

		// Set the maximum number of clusters
		kmeans.setMaxNumClusters((int) maxNoClustersSpinner.getValue());

		// Use feature extraction utility method to get the required port scan and network scan features
		if (!manualFeatures) {
			noDimensions = 2;
			//data = FeatureExtraction.getNetworkPostScanFeatures(data);
			data = sqlConnection.option("dbtable", "(select sip, count(DISTINCT dip), count(DISTINCT dp) from " 
					+ table + " " + sqlClause + " group by sip limit 0, " + limitSpinner.getValue() + ") as kmeans_data")
					.load().toJavaRDD();
			System.out.println("The total number of rows in the data " + data.count());

			// Get axes labels for port and network scan
			excludeLabel = true;
			xAxis = " - Destination IPs";
			yAxis = " - Destination Ports";

			// set port and network scan classification to true
			kmeans.setPortNetworkScanDetection(true); 

			// Set threshold for network and port scan
			kmeans.setNetworkScanThreshold(Double.parseDouble(txtNetworkScanThreshold.getText())); 
			kmeans.setPortScanThreshold(Double.parseDouble(txtPortScanThreshold.getText()));
		} else {
			
			// Add some logic if it's volume analysis
			if (volumeAnalysis) {
				table = "(SELECT sip, sum(bytes) as pbytes, sum(pck) as ppck FROM " + table + " " + sqlClause 
						+ " group by " + label + ", st order by ppck desc) A";
				
				sqlClause = "";
			}
			
			String query = "select " + features + " from " + table 
					+ " " + sqlClause + " group by " + label + " limit 0, " +
					limitSpinner.getValue();
			
			// Get the specified data
			Dataset<Row> sqlData = sqlConnection.option("dbtable", "(" + query + ") as kmeans_data" ).load();

			System.out.println("The total number of rows in the data " + sqlData.count());

			data = sqlData.toJavaRDD();
		}


		// Set a label for the unsupervised data to be the source ip
		JavaPairRDD<Long, Vector> labeledFeatures = FeatureExtraction
				.transformToJavaPair(data, labelIndex, excludeLabel);

		// Normalise the data if requires so
		if (manualFeatures && chckbxNormalise.isSelected()) {
			labeledFeatures = FeatureExtraction.normalise(labeledFeatures);
		}

		System.out.println("The number of points in the training set " + labeledFeatures.count());
		System.out.println("The first training point " + labeledFeatures.first());

		// Try reducing the number features/dimensions of the data if requires so
		try {
			normalisedFeatures = new DimensionalityReduction()
					.reduceWithPCA(labeledFeatures, noDimensions);
		} catch (Exception e) {
			throw new Exception("Data does not converge", e);
		}

		// Run the clustering algorithm
		JavaPairRDD<String, Iterable<Vector>> output = null;

		Map<Vector, Long> anomalies = null;

		// If a classifier model is used
		if (rdbtnUseAClassifier.isSelected()) {

			MultiClassClassification MCC = new MultiClassClassification();
			
			// Set the path of the model to take the most recent one
			String path = "";
			
			// Get the selected module
			MODULES module = (MODULES) cmBxModule.getSelectedItem();
			
			if (MainApplication.sqlConnection == null) MainApplication.sqlConnection = this.sqlConnection;
			String tableName = table.split("_")[1];
			
			// Load features depending on the module chosen
			switch (module) {
			case NETWORKPORTSCAN:
				normalisedFeatures = NetworkAnalysis.loadData("count(DISTINCT dip), count(DISTINCT dp)", null, tableName);
				path = "feature/" + DataProcessing.getModelPath(path + "feature/");
				break;

			case VOLUMEANALYSIS:
				normalisedFeatures = NetworkAnalysis.loadData("pck, bytes", null, tableName);
				// Normalise the data
				//featureData = FeatureExtraction.normalise(featureData);
				path = "volume/" + DataProcessing.getModelPath(path + "volume/");
				break;
			case FLOODING:
				String query = "(SELECT sip, max(ppck), pbytes FROM (SELECT sip, sum(bytes) as pbytes, sum(pck)"
						+ " as ppck FROM h1_$ where sip between 167772160 and 184549375 and  sp != 80"
						+ " group by sip, st order by ppck desc) A GROUP BY sip) as kmeans_data";
				normalisedFeatures = NetworkAnalysis.loadData("", query, tableName);
				path = "flooding/" + DataProcessing.getModelPath(path + "flooding/");
				break;
			}

			// Try loading the model from a local file
			try {
				MCC.setModelPath(path);
				MCC.loadModel();
			} catch (Exception e) {
				showMessage("Cannot load the model from a local file " + e.getMessage(),
						"Loading classifier model", JOptionPane.ERROR_MESSAGE);
				return;
			}
			output = MCC.predict(normalisedFeatures);

			// Get the results of the clusters
			anomalies = MCC.getAnomalies();

			// Or using KMeans
		} else {
			output = kmeans.initialiseKMeans(normalisedFeatures, true);

			// Get the results of the clusters
			anomalies = kmeans.getAnomalies();
		}

		String anomaliesTxt = "";

		// Format the anomaly to the appropriate type
		FEATURES feature = (FEATURES) cmBxLabel.getSelectedItem();
		
		// Convert decimal to ip address
		if (!manualFeatures || feature == FEATURES.sip || feature == FEATURES.dip) {

			for (Map.Entry<Vector, Long> entry : anomalies.entrySet()) {
				anomaliesTxt += "Label: " + FeatureExtraction.longToIp(entry.getValue()) + "/" + entry.getValue()
				+ "\tValues: " + entry.getKey() + "\n"; 
			}
			// Convert timestamp to date
		} else if (feature == FEATURES.st) {

			for (Map.Entry<Vector, Long> entry : anomalies.entrySet()) {
				anomaliesTxt += "Label: " + new Date(entry.getValue()*1000)
						+ "\tValues: " + entry.getKey() + "\n"; 
			}

		} else {

			for (Map.Entry<Vector, Long> entry : anomalies.entrySet()) {
				anomaliesTxt += "Label: " + entry.getValue()
				+ "\tValues: " + entry.getKey() + "\n"; 
			}

		}
		
		textAreaAnomalies.setText(anomaliesTxt);

		// If the output data has two dimensions plot them in a scatter
		if (noDimensions == 2) {
			chartPanel.removeAll();
			// Create a chartPlotter to plot the anomalies
			ChartPlotter chart = new ChartPlotter("KMeans Clustering", 600, 400);

			chartPanel.add(chart.scatter2D(output, "KMeans Clustering", "X" + xAxis, "Y" + yAxis, false)
					, BorderLayout.CENTER);
		}

		if (noDimensions == 3) {
			chartPanel.removeAll();
			// Create a chartPlotter to plot the anomalies
			ChartPlotter chartPlotter = new ChartPlotter("KMeans Clustering", 600, 400);

			Component chart = (Component)chartPlotter.scatter3D(output, "KMeans Clustering",
					"X", "Y", "Z", false).getCanvas();

			chart.setSize(600, 400);

			chartPanel.add(chart, BorderLayout.CENTER);
		}

		// update the chart panel
		chartPanel.repaint();

		// Train and save a classifier model if requires so
		if (chckbxUpdateClassifierModel.isSelected()) {

			MultiClassClassification MCC = new MultiClassClassification();

			// Try loading the model from a local file
			try {
				MCC.loadModel();
			} catch (Exception e) {
				showMessage("Cannot load the model from a local file, creating a new model ",
						"Loading classifier model", JOptionPane.OK_OPTION);
			}

			// Update the classifier model
			MCC.trainModel(FeatureExtraction.labelClusteredData(output), 4);

			showMessage("The precision of the model " + MCC.evaluateModel(),
					"Classifier model", JOptionPane.INFORMATION_MESSAGE);
			// Save model locally
			MCC.saveModel();
		}

	}

	/**
	 * Log the user in an existing database using their username and password
	 * and database url
	 * @param username
	 * @param password
	 * @param url
	 * @return true if login is successful, false otherwise
	 */
	private boolean login(String username, String password, String url) {

		// Check if any of the fields is empty
		if (username == null || username.equals("") || password == null
				|| password.equals("") || url == null || url.equals("")) {
			showMessage("Some fields are empty", "Login Error", JOptionPane.ERROR_MESSAGE);
		}

		// Initialise a database connection
		SQLContext sqlContext = new SQLContext(MainApplication.javaContext);

		Map<String, String> options = new HashMap<>();
		options.put("driver", DumpData.MYSQL_DRIVER);
		options.put("url", "jdbc:mysql://" + url + "?"
				+ "user=" + username + "&password=" + password);

		try {
			sqlConnection = sqlContext.read().format("jdbc").options(options);

			// Load all the hourly tables' names
			Row[] tables = (Row[]) sqlConnection.option("dbtable", "(select TABLE_NAME from INFORMATION_SCHEMA.TABLES"
					+ " where TABLE_NAME like 'h1_%') as ftp_connections" ).load().collect();

			// Convert them to String[] and store them
			tablesNames = new String[tables.length];

			for (int i = 0; i < tables.length; i++) {

				tablesNames[i] = tables[i].getString(0);

			}


		} catch (Exception e) {

			e.printStackTrace();

			showMessage(e.getMessage(), "Login Error", JOptionPane.ERROR_MESSAGE);

			return false;
		}

		return true;
	}

	private void checkLogin() {

		String[] loginDetails;
		try {
			// Try to load login details from a local file
			loginDetails = loadLogin();
		} catch (Exception e) {
			System.out.println("No saved login details");
			return;
		} 

		// If successfully loaded try to use these details to login
		if(login(loginDetails[0], loginDetails[1], loginDetails[2])) {
			navigator.next(frmNetflowAnalyser.getContentPane());
			System.out.println("Login is successful through saved details");
		}
	}

	/**
	 * Save login details in a local file after encrypting them use AES
	 * Encryption algorithm
	 * @param username
	 * @param password
	 * @param url
	 * @throws Exception
	 */
	private static void saveLogin(String username, String password, String url) throws Exception {

		// Text to be encrypted
		String txt = username + SEPERATOR + password + SEPERATOR + url;

		// Create key and cipher
		Key aesKey = new SecretKeySpec(KEY.getBytes(), "AES");
		Cipher cipher;

		try {
			cipher = Cipher.getInstance("AES");

			// encrypt the text
			cipher.init(Cipher.ENCRYPT_MODE, aesKey);

			byte[] encrypted = cipher.doFinal(txt.getBytes());

			// Save the encrypted text locally
			Files.write(new File("./resource/login").toPath(), encrypted);

		} catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException
				| IllegalBlockSizeException | BadPaddingException | IOException e) {

			e.printStackTrace();

			throw e;

		} 

	}

	/**
	 * Load login details from a local file and try to decrypt them
	 * @return
	 * @throws IOException
	 * @throws InvalidKeyException
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 */
	private static String[] loadLogin() throws IOException, InvalidKeyException,
	NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException {

		// Create key and cipher
		Key aesKey = new SecretKeySpec(KEY.getBytes(), "AES");
		Cipher cipher = Cipher.getInstance("AES");

		// Get the encryption
		byte[] encrypted = Files.readAllBytes(new File("./resource/login").toPath());

		// decrypt the text
		cipher.init(Cipher.DECRYPT_MODE, aesKey);
		String decrypted = new String(cipher.doFinal(encrypted));

		return decrypted.split("\\" + SEPERATOR);

	}

	/**
	 * Utility method to display error messages
	 * @param message
	 * @param title
	 * @param messageType
	 */
	private void showMessage(String message, String title, int messageType) {
		JOptionPane.showMessageDialog(frmNetflowAnalyser, message, title, messageType);
	}
	
}
