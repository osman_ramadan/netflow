/**
 * 
 */
package netflow.visualisation;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.mllib.linalg.Vector;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.time.Minute;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.ApplicationFrame;
import org.jzy3d.chart.Chart;
import org.jzy3d.chart.factories.AWTChartComponentFactory;
import org.jzy3d.maths.Coord3d;
import org.jzy3d.plot3d.primitives.Scatter;
import org.jzy3d.plot3d.rendering.canvas.Quality;

import scala.Tuple2;

/**
 * ChartPlotter is used to plot different charts and graphs based on JFreeChart API
 * 
 * @author Osman
 *
 */
public class ChartPlotter extends ApplicationFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2685711195333781810L;
	private int width;
	private int height;
	private Random rand = new Random();

	/**
	 * The constructor
	 * @param applicationTitle title of the application
	 * @param width the width of the chart
	 * @param height the height of the chart
	 */
	public ChartPlotter(String applicationTitle, int width, int height) {
		super(applicationTitle);
		this.width = width;
		this.height = height;
	}

	/**
	 * Set the size/dimension of the chart
	 * @param width of the chart
	 * @param height of the chart
	 */
	public void setSize(int width, int height) {
		this.width = width;
		this.height = height;
	}
	/**
	 * plot a 2D scatter graph
	 * @param data the points to be plotted {@code [String, [x1, y1]]}
	 * @param chartTitle the title of the plot
	 * @param xLabel the label of the x-axis
	 * @param yLabel the label of the y-axis
	 * @param selfContained Should this chart run as independent application or as part of GUI
	 * @return chart panel that contains the chart
	 */
	@SuppressWarnings("serial")
	public ChartPanel scatter2D(JavaPairRDD<String, Iterable<Vector>> data, String chartTitle,
			String xLabel, String yLabel, boolean selfContained) {

		// Map data to List<XYSeries>
		List<XYSeries> groups = data.map(new Function<Tuple2<String, Iterable<Vector>>, XYSeries>() {

			@Override // for each label ("anomaly", "normal")
			public XYSeries call(Tuple2<String, Iterable<Vector>> v1) throws Exception {

				final XYSeries group = new XYSeries(v1._1());

				for (Vector point : v1._2()) {

					double[] values = point.toArray();

					group.add(values[0], values[1]);

				}

				return group;
			}

		}).collect();

		// Add all series to the dataSet
		final XYSeriesCollection dataset = new XYSeriesCollection(); 
		for (XYSeries x : groups) {
			dataset.addSeries(x);
		}

		JFreeChart xylineChart = ChartFactory.createXYLineChart(
				chartTitle,
				xLabel,
				yLabel,
				dataset,
				PlotOrientation.VERTICAL ,
				true , true , false);

		// Constructs a panel that displays the specified chart
		ChartPanel chartPanel = new ChartPanel(xylineChart);

		// Set the size of the panel
		chartPanel.setPreferredSize(new java.awt.Dimension(width, height));

		// Generate a plot from the chart object
		final XYPlot plot = xylineChart.getXYPlot();

		// Add shape renderer to color the points of different groups
		XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();

		int noGroups = groups.size();

		for (int i = 0; i < noGroups; i++) {
			renderer.setSeriesPaint(i, generateRandomColor());
			renderer.setSeriesLinesVisible(i, false);
		}

		plot.setRenderer(renderer); 

		chartPanel.setMouseZoomable(true, true);

		if (selfContained) setContentPane(chartPanel); 

		return chartPanel;

	}

	/**
	 * plot a time series graph
	 * @param data the points to be plotted {@code [String, [y1]]}
	 * @param yLabel the label of the y-axis
	 * @param time the axis points which represent the time
	 * @param chartTitle the title of the chart
	 * @param selfContained Should this chart run as independent application or as part of GUI
	 * @return chart panel that contains the chart
	 */
	public ChartPanel sketchTimeSeries(Map<String, double[]> data, String yLabel, long[] time, String chartTitle, boolean selfContained) {

		// Create a time series collection to hold all the graphs
		TimeSeriesCollection timeSeries = new TimeSeriesCollection();
		
		// For each collection of data
		for (Map.Entry<String, double[]> entry : data.entrySet()) {
			// Create a new time series
			TimeSeries series = new TimeSeries(entry.getKey());
			// Add the points to the graph
			int j = 0;
			for (double value : entry.getValue()) {
				series.add(new Minute(new Date(time[j]*1000)), value);
				j++;
			}
			
			// Add this data series to the collection
			timeSeries.addSeries(series);
		}
		
		// Create the chart panel
		ChartPanel chartPanel = new ChartPanel(ChartFactory.createTimeSeriesChart(chartTitle, "Time in Minutes",
				yLabel, timeSeries, true, false, false));
		
		// Adjust the settings of the chart panel
		chartPanel.setPreferredSize(new java.awt.Dimension(this.width, this.height));
		chartPanel.setMouseZoomable(true, true);

		// Show to the chart if selfContained is true
		if (selfContained) setContentPane(chartPanel);

		return chartPanel;

	}


	/**
	 * plot a 3D scatter graph
	 * @param data the points to be plotted {@code [String, [x1, y1, z1]]}
	 * @param chartTitle the title of the plot
	 * @param xLabel the label of the x-axis
	 * @param yLabel the label of the y-axis
	 * @param zLabel the label of the z-axis
	 * @param selfContained Should this chart run as independent application or as part of GUI
	 * @return chart panel that contains the chart
	 */
	public Chart scatter3D(JavaPairRDD<String, Iterable<Vector>> data, String chartTitle,
			String xLabel, String yLabel, String zLabel,  boolean selfContained) {

		// Map data to List<XYSeries>
		List<Coord3d[]> groups = new ArrayList<>();

		List<String> labels = new ArrayList<>();

		for (Tuple2<String, Iterable<Vector>> v1 : data.collect()) {

			final List<Coord3d> points = new ArrayList<>();

			for (Vector point : v1._2()) {

				double[] values = point.toArray();

				points.add(new Coord3d(values[0], values[1], values[2]));

			}

			groups.add(points.toArray(new Coord3d[points.size()]));

			labels.add(v1._1());

		}

		// Create a chart and add the surface
		Chart chart = AWTChartComponentFactory.chart(Quality.Advanced);
		for (int i = 0; i < groups.size(); i++) {
			Coord3d[] x = groups.get(i);

			org.jzy3d.colors.Color[] colors = new org.jzy3d.colors.Color[x.length];

			Arrays.fill(colors, org.jzy3d.colors.Color.random());

			Scatter scatter = new Scatter(x, colors);

			scatter.setWidth(10f);

			chart.addDrawable(scatter);
		}

		chart.addMouseController();
		if(selfContained) chart.open(chartTitle, width, height);

		return chart;

	}

	/**
	 * Generate random colours
	 * @return random colour
	 */
	private Color generateRandomColor() {

		// Java 'Color' class takes 3 floats, from 0 to 1.
		float r = rand.nextFloat();
		float g = rand.nextFloat();
		float b = rand.nextFloat();

		return new Color(r, g, b);
	}

}
