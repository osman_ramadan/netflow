/**
 * 
 */
package netflow.business.logic;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * This is a utility class to load and store values in the configuration file
 * 
 * @author Osman
 *
 */
public class ConfigurationManager {

	private static Properties prop = new Properties();

	/**
	 * Store a property in the configuration file
	 * @param key of the property
	 * @param value of the property
	 */
	public static void setProperty(String key, String value) {

		try (FileOutputStream output = new FileOutputStream("config.properties")) {

			// save properties to project root folder
			prop.store(output, null);

			// set the properties value
			prop.setProperty(key, value);

		} catch (IOException ex) {
			
			ex.printStackTrace();
			
		}
	}

	/**
	 * 
	 * Get a value of a property from the configuration file given its key
	 * 
	 * @param key of the property
	 * @return value of the property
	 */
	public static String getProperty(String key) {
		
		String value = null;

		try (FileInputStream input = new FileInputStream("config.properties")) {

			// load a properties file
			prop.load(input);

			// get the property value and print it out
			value = prop.getProperty(key);

		} catch (IOException ex) {
			
			ex.printStackTrace();
			
		}
		
		return value;

	}

}
