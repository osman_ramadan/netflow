/**
 * 
 */
package netflow.business.logic;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.mllib.linalg.Vector;
import org.apache.spark.sql.Row;

import netflow.business.logic.MainApplication.MODULES;
import netflow.data.analysis.FeatureExtraction;
import netflow.data.analysis.KMeansClustering;
import netflow.data.analysis.MultiClassClassification;
import netflow.data.in.DataProcessing;
import netflow.data.out.OutputRepository;
import netflow.data.out.entities.Anomaly;
import netflow.data.out.entities.ModelMetrics;
import netflow.data.out.entities.Task;
import scala.Tuple2;

/**
 * A scheduled job that :
 * <p> 1 - Get data from database</p>
 * <p> 2 - Train all models</p> 
 * <p> 3 - Save models locally</p>
 * 
 * @author Osman
 *
 */
public class ModelTraining implements Runnable {

	private MultiClassClassification MCC;
	private static final SimpleDateFormat formatter = new SimpleDateFormat("yy MM dd");
	private static final Logger logger = Logger.getLogger(ModelTraining.class);
	private static int retries;
	private static String day;
	private double[] overallMetrics;

	/**
	 * Default constructor - Update the current day
	 */
	public ModelTraining() {
		// Get Yesterday as a String
		day = formatter.format(DateUtils.addDays(new Date(), -1)).replace(" ", "");
	}

	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {

		// Run analysis each module
		for (MODULES module : MainApplication.modules) {

			MCC = new MultiClassClassification();
			overallMetrics = new double[]{0.0, 0.0};

			// Get start time of the task
			long startTime = System.currentTimeMillis()/1000;

			ScheduledFuture<?> task  = MainApplication.scheduledExecutorService.schedule(() -> {

				// Train the model using the clustering algorithm
				runClustering(module);

				// Evaluate the model using the labelled data from the analytics database
				evaluateModel(module);

			}
			, 0, TimeUnit.SECONDS);

			// Keep attempting to perform this task
			boolean keepTrying = true;
			int limit = Integer.parseInt(ConfigurationManager.getProperty("netflow.task.retries"));
			while (keepTrying){

				try {

					logger.info("About to run a task for module " + module.name() + " for the " + retries + " time");

					task.get();

					keepTrying = false;

				} catch (Exception e2) {
					logger.error("An uncaught exception while running the task, retrying " + retries, e2);

					// Try again 
					if (retries < limit) {

						retries++;
						try {
							int mins = Integer.parseInt(ConfigurationManager.getProperty("netflow.task.waiting.min"));
							logger.info("Waiting for " + mins);
							Thread.sleep(mins * 60000);
						} catch (NumberFormatException | InterruptedException e1) {
							logger.error("Waiting failed", e1);
						}

					} else {
						logger.error("Couldn't solve the error, rescheduling for next day...");
						retries = 0;
						keepTrying = false;
					}
				}
			}

			// Get the end time of the task
			long endTime = System.currentTimeMillis()/1000;

			// Try to save performance of the task
			OutputRepository outputRepository;
			try {
				outputRepository = OutputRepository.getInstance();
				outputRepository.save(new Task("daily " + module.name(), startTime, endTime));
			} catch (SQLException e) {
				logger.error("Couldn't save this task in the analytics database", e);
			}



		}

	}

	/**
	 * Evaluate the classifier model using labelled data and save the metrics in the analytics database
	 */
	private void evaluateModel(MODULES module) {
		// First get the data from the analytics database
		OutputRepository outputRepository = OutputRepository.getInstance();
		List<Anomaly> anomalies = null;

		String clause = "";
		String features = "";
		String anaylsisType = "";
		String customQuery = null;

		// Load the data depending on the required module
		switch (module) {

		case NETWORKPORTSCAN:
			features = "count(DISTINCT dip), count(DISTINCT dp)";
			clause = "WHERE verified = 1 and type != 'traffic anomaly' or type != 'flooding attack'";
			anaylsisType = "feature-";
			break;

		case VOLUMEANALYSIS:
			features = "pck, bytes";
			clause = "WHERE verified = 1 and type = 'traffic anomaly'";
			anaylsisType = "volume-";
			break;
		case FLOODING:
			customQuery = "(SELECT sip, max(ppck), pbytes FROM (SELECT sip, sum(bytes) as pbytes, sum(pck)"
					+ " as ppck FROM $ where sip between 167772160 and 184549375 and  sp != 80"
					+ " group by sip, st order by ppck desc) A GROUP BY sip) as kmeans_data";
			clause = "WHERE verified = 1 and type = 'flooding attack'";
			anaylsisType = "flooding-";
			break;
		}

		try {
			// Get all the verified data
			anomalies = outputRepository.load(clause);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		// Create a map of sip : label
		final Map<Long, String> labelledSip = new HashMap<>();
		final Set<String> tables = new HashSet<>();

		// For each anomaly
		for (Anomaly anomaly : anomalies) {
			// Get the source ip, type and the table name
			tables.add(anomaly.getHourly_table());
			labelledSip.put(anomaly.getSip(), anomaly.getType());
		}

		// Store the average metrics
		double metrics  = 0;
		int count = 0;

		// For each hourly table
		for (String tableName : tables) {

			count++;

			String query = "(select sip,"
					+ features + " from " + tableName + " where sip"
					+ " between 167772160 and 184549375 and sp != 80 group by sip ) as kmeans_data";
			if (customQuery != null) {
				query = customQuery.replaceAll("\\$", tableName);
			}
			JavaPairRDD<Long, Vector> featureVector = null;
			try {
				featureVector = FeatureExtraction
						.transformToJavaPair(MainApplication.sqlConnection.option("dbtable", query)
								.load().toJavaRDD(), 0, true);
			} catch (Exception e) {
				logger.error("Failed to load " + tableName + " hourly table", e);
				logger.info("Continuing to the next table");
				continue;
			}

			// Get that table, transform it to pair ip : value, map it to label : value, then group it by label
			JavaPairRDD<String, Iterable<Vector>> labeledFeatures = featureVector
					.mapToPair((Tuple2<Long, Vector> t) -> {
						String label = "normal";
						long sip = t._1;
						if (labelledSip.containsKey(sip)) label = labelledSip.get(sip);
						return new Tuple2<String, Vector>(label, t._2);
					}).groupByKey();

			MCC.setTest(FeatureExtraction.labelClusteredData(labeledFeatures));

			// Evaluate the model using F score
			double hourMetrics  = MCC.evaluateModel();

			// Add the metrics
			metrics += hourMetrics;

			logger.info("For this hour " + tableName + "The F score is " + hourMetrics);
		}

		// Store the average metrics 
		if (count > 0) overallMetrics[0] = metrics/(1.0 * count);

		// Save metrics in the database
		try {
			OutputRepository.getInstance().save(new ModelMetrics(anaylsisType + day, overallMetrics[0], overallMetrics[1]));
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Use the clustering algorithm and the hourly tables for 24 hours 
	 * for the past day to train the classifier model
	 */
	private void runClustering(MODULES module) {

		// Get the previous day (Yesterday)
		day = formatter.format(DateUtils.addDays(new Date(), -1)).replace(" ", "");

		// A double to hold the average evaluation metrics
		double metrics = 0;

		String features = "";
		String path = "";
		String savePath = "";
		String customQuery = null;

		int numOfclasses = 2;

		// Now Initialise the KMeans algorithms
		KMeansClustering kmeans = new KMeansClustering();

		// Load the data depending on the required module
		switch (module) {

		case NETWORKPORTSCAN:
			features = "count(DISTINCT dip), count(DISTINCT dp)";
			path = "feature/" + DataProcessing.getModelPath(path + "feature/");
			savePath = "feature/";

			// set port and network scan classification to true
			kmeans.setPortNetworkScanDetection(true); 

			// Set threshold for network and port scan
			kmeans.setNetworkScanThreshold(Integer.parseInt(ConfigurationManager
					.getProperty("netflow.kmeans.networkScan.threshold"))); 
			kmeans.setPortScanThreshold(Integer.parseInt(ConfigurationManager
					.getProperty("netflow.kmeans.portScan.threshold")));
			numOfclasses = 4;
			break;

		case VOLUMEANALYSIS:
			features = "pck, bytes";
			path = "volume/" + DataProcessing.getModelPath(path + "volume/");
			savePath = "volume/";
			numOfclasses = 2;
			break;
		case FLOODING:
			customQuery = "(SELECT sip, max(ppck), pbytes FROM (SELECT sip, sum(bytes) as pbytes, sum(pck)"
					+ " as ppck FROM $ where sip between 167772160 and 184549375 and  sp != 80"
					+ " group by sip, st order by ppck desc) A GROUP BY sip) as kmeans_data";
			path = "flooding/" + DataProcessing.getModelPath(path + "flooding/");
			savePath = "flooding/";
			numOfclasses = 2;
			break;
		}

		// Load the classifier model for Network and Port Scan
		MCC.setModelPath(path);
		MCC.loadModel();
		int counter = 0;
		// For all the 24 hours in that day
		for (int i = 0; i < 24; i++) {

			// Get the hourly table name
			String table = "h1_" + day + String.format("%02d", i);

			String query = "(select sip, " + features +
					" from " + table + " Where sip between 167772160 and 184549375 "
					+ "and sp != 80 group by sip) as kmeans_data";

			if (customQuery != null) {
				query = customQuery.replaceAll("\\$", table);
			}
			
			// Try load the hourly table
			JavaRDD<Row> data = null;
			int limit = Integer.parseInt(ConfigurationManager.getProperty("netflow.task.retries"));
			boolean keepTrying = true;
			int waitingTime = 1;
			
			while(keepTrying) {
				
				try {
					// Load data from the database corresponding to the i hourly table
					data = MainApplication.sqlConnection.option("dbtable", query).load().toJavaRDD();
					logger.info("The total number of rows in the data " + data.count());
					keepTrying  = false;
				} catch (Exception e) {
					logger.error("Failed to load " + table + " hourly table", e);
					retries++;
					
					// Try to wait
					try {
						logger.info("Waiting for " + waitingTime + " Second");
						Thread.sleep(Math.round(waitingTime * 1000));
					} catch (NumberFormatException | InterruptedException e1) {
						logger.error("Waiting failed", e1);
					}
					
				}
				
				if (retries >= limit) keepTrying = false;
			} 
			
			retries = 0;
			
			if (data == null) {
				logger.info("Failed retrying");
				logger.info("Continuing to the next table");
				continue;
			}
			
			// Set the maximum number of points in an anomalous cluster
			kmeans.setAnomalousMax(Integer.parseInt(ConfigurationManager
					.getProperty("netflow.kmeans.anomalousCluster.numOfPoints.max")));

			// Set the maximum number of clusters
			kmeans.setMaxNumClusters(Integer.parseInt(ConfigurationManager
					.getProperty("netflow.kmeans.numOfClusters.max")));

			// Set a label for the unsupervised data to be the source ip
			JavaPairRDD<Long, Vector> labeledFeatures = FeatureExtraction.transformToJavaPair(data, 0, true);

			// Classified data to be used for training the classifier model
			JavaPairRDD<String, Iterable<Vector>> output = kmeans.initialiseKMeans(labeledFeatures, true);

			// Output the anomalies in the console for debugging
			for (Map.Entry<Vector, Long> entry : kmeans.getAnomalies().entrySet()) {
				logger.info("Label: " + FeatureExtraction.longToIp(entry.getValue()) + " / " + entry.getValue()
				+ "\t Values: " + entry.getKey()); 
			}

			// Update the model
			MCC.trainModel(FeatureExtraction.labelClusteredData(output), numOfclasses);

			// Get the evaluation metrics for this hour
			double hourMetrics = MCC.evaluateModel();

			logger.info("The evaluation metrics for table " + table + " is " + hourMetrics);

			// Add to the total evaluation metrics 
			metrics += hourMetrics;
			
			counter++;

		}

		// Get the average 
		overallMetrics[1] = metrics/Math.max(counter, 1);

		// Set the path to save the model at
		savePath += day;
		MCC.setModelPath(savePath);

		// Save the updated model locally
		MCC.saveModel();

	}

}
