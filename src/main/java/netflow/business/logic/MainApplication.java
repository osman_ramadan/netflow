/**
 * 
 */
package netflow.business.logic;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.DataFrameReader;
import org.apache.spark.sql.SQLContext;

/**
 * 
 * This the main class for the Netflow analyser system
 * It runs scheduled jobs asynchronously to analyse Netflow traffic and detect
 * and classify network anomalies
 * 
 * @author Osman
 *
 */
public class MainApplication {
	
	public enum MODULES {NETWORKPORTSCAN, VOLUMEANALYSIS, FLOODING}

	public static JavaSparkContext javaContext = new JavaSparkContext(new SparkConf()
			.setAppName("Netflow").setMaster("local[*]"));
	
	private static final String MYSQL_DRIVER = "com.mysql.jdbc.Driver";
	private static final Logger logger = Logger.getLogger(MainApplication.class);
	
	public static MODULES[] modules;
	public static DataFrameReader sqlConnection;
	public static ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(5);

	/**
	 * Main method from the application runs
	 * @param args from the terminal
	 * @throws InterruptedException if been interrupted during sleeping
	 */
	public static void main(String[] args) throws InterruptedException {

		// Suppress all Spark logs
		Logger.getLogger("org").setLevel(Level.OFF);
		Logger.getLogger("akka").setLevel(Level.OFF);
		
		// Get the required modules from the input
		modules = new MODULES[args.length];
		int  i = 0;
		for (String module : args) {
			int mod = Integer.parseInt(module);
			switch (mod) {
			case 0:
				modules[i] = MODULES.NETWORKPORTSCAN;
				break;
			case 1:
				modules[i] = MODULES.FLOODING;
				break;
			case 2:
				modules[i] = MODULES.VOLUMEANALYSIS;
				break;
			}
			i++;
		}

		// Connect to the database
		// Initialise a database connection
		SQLContext sqlContext = new SQLContext(javaContext);

		Map<String, String> options = new HashMap<>();
		options.put("driver", MYSQL_DRIVER);
		options.put("url", "jdbc:mysql://" + ConfigurationManager.getProperty("netflow.nfx.url") + "?"
				+ "user=" +  ConfigurationManager.getProperty("netflow.nfx.username") +
				"&password=" +  ConfigurationManager.getProperty("netflow.nfx.password"));
		
		logger.info("Initialising a connection to the database...");

		try {

			sqlConnection = sqlContext.read().format("jdbc").options(options);

			// Load all the hourly tables' names
			sqlConnection.option("dbtable", "(select TABLE_NAME from INFORMATION_SCHEMA.TABLES"
					+ " where TABLE_NAME like 'h1_%') as ftp_connections" ).load().show();


		} catch (Exception e) {

			logger.error("Couldn't connect to the database", e);

			return;
		}
		
		// TODO Change the time configuration so that the task runs at a specific time in the day or hour rather than
		// an interval
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.MINUTE, Integer.parseInt(ConfigurationManager
				.getProperty("netflow.timer.start.analysis.min")));
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		Date startTime = cal.getTime();

		// TODO Find a way of reducing the noise in the anomaly detection system especially with port scan
		// Instantiate NetworkAnalysis class
		NetworkAnalysis networkAnalysis = new NetworkAnalysis();
		
		// If the starting time (Min in Hour) has already passed reschedule it to next hour
		if (startTime.getTime() <  System.currentTimeMillis()) startTime = DateUtils.addHours(startTime, 1);
		
		// compute the delay
		long delay = startTime.getTime() - System.currentTimeMillis();

		// Create Repetitively task for every x minutes
		scheduledExecutorService.scheduleAtFixedRate(networkAnalysis, delay, Integer.parseInt(ConfigurationManager
				.getProperty("netflow.timer.interval.analysis.min"))*60000, TimeUnit.MILLISECONDS); 
		
		logger.info("Network Analysis task has been scheduled to run at " + startTime);

		// Instantiate ModelTraining class
		ModelTraining modelTraining = new ModelTraining();

		// Create a Repetitively task for every y day starts after y day
		int time =  Integer.parseInt(ConfigurationManager
				.getProperty("netflow.timer.interval.training.days"))*86400000;
		
		// Get the start time for the daily routine
		cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(ConfigurationManager
				.getProperty("netflow.timer.start.training.hour")));
		cal.set(Calendar.MINUTE, Integer.parseInt(ConfigurationManager
				.getProperty("netflow.timer.start.training.min")));
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND,0);
		
		startTime = cal.getTime();
		
		// If the starting time (Hour in Day) has already passed reschedule it to next day
		if (startTime.getTime() < System.currentTimeMillis()) startTime = DateUtils.addDays(startTime, 1);
		
		// Compute the delay for the second task
		delay = startTime.getTime() - System.currentTimeMillis();

		scheduledExecutorService.scheduleAtFixedRate(modelTraining, delay, time, TimeUnit.MILLISECONDS); 
		
		logger.info("Model Training task has been scheduled to run at " + startTime);
		
		logger.info("Finished Scheduling.");

	}

}
