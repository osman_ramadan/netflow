/**
 * 
 */
package netflow.business.logic;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.log4j.Logger;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.mllib.linalg.Vector;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

import netflow.business.logic.MainApplication.MODULES;
import netflow.data.analysis.FeatureExtraction;
import netflow.data.analysis.FeatureExtraction.DataType;
import netflow.data.in.DataProcessing;
import netflow.data.analysis.MultiClassClassification;
import netflow.data.analysis.StatisticalProcessControl;
import netflow.data.analysis.WaveletAnalysis;
import netflow.data.out.OutputRepository;
import netflow.data.out.OutputRepositoryHelper;
import netflow.data.out.entities.Anomaly;
import netflow.data.out.entities.Task;
import scala.Tuple2;

/**
 * 
 * A scheduled job that :
 * <p> 1 - Get data from database</p>
 * <p> 2 - Run data analytics</p> 
 * <p> 3 - Save results</p>
 * <p> 4 - Notify support</p>
 * 
 * @author Osman
 *
 */
public class NetworkAnalysis implements Runnable {


	private static final SimpleDateFormat formatter = new SimpleDateFormat("yy MM dd HH");
	private static final Logger logger = Logger.getLogger(NetworkAnalysis.class);
	private static int retries = 0;

	private Date endTime;
	public int endHour;
	private Date startTime;
	public int startHour;
	private RulesManager rulesManager;

	/**
	 * Default constructor to instantiate RulesManager;
	 */
	public NetworkAnalysis() {
		rulesManager = new RulesManager();
	}

	/**
	 * Run the specified analysis depending on the module that is passed as an argument
	 * @param module the detection module to be used 
	 */
	private void runAnalysis(MODULES module) {

		// Query data from the database
		JavaPairRDD<Long, Vector> featureData = null;

		// Load Classifier Model for featureData
		MultiClassClassification MCC = new MultiClassClassification();

		// Set the path of the model to take the most recent one
		String path = "";
		
		// Get the right table 
		String table = formatter.format(startTime).replace(" ", "");
		
		// Load the data depending on the required module
		switch (module) {

		case NETWORKPORTSCAN:
			featureData = loadData("count(DISTINCT dip), count(DISTINCT dp)", null, table);
			path = "feature/" + DataProcessing.getModelPath(path + "feature/");
			break;

		case VOLUMEANALYSIS:
			featureData = loadData("pck, bytes", null, table);
			path = "volume/" + DataProcessing.getModelPath(path + "volume/");
			break;
		case FLOODING:
			String query = "(SELECT sip, max(ppck), pbytes FROM (SELECT sip, sum(bytes) as pbytes, sum(pck)"
					+ " as ppck FROM h1_$ where sip between 167772160 and 184549375 and  sp != 80"
					+ " group by sip, st order by ppck desc) A GROUP BY sip) as kmeans_data";
			featureData = loadData("", query, table);
			path = "flooding/" + DataProcessing.getModelPath(path + "flooding/");
			break;
		}

		MCC.setModelPath(path);
		MCC.loadModel(); // Can throw an exception if the model is not found

		// Classify and extract anomalies
		JavaPairRDD<String, Iterable<Vector>> labelData = MCC.predict(featureData);

		Map<Long, long[]> output = new HashMap<>();

		// Get the feature time (PCK, FLOW ..etc)
		DataType feature = DataType.valueOf(ConfigurationManager.getProperty("netflow.volume.feature"));

		// Update Rules from the database
		rulesManager.updateRules();

		// Get the hourly table name
		String tableName = "h1_" + formatter.format(startTime).replace(" ", "");

		logger.info("Selecting table " + tableName);

		// For each anomaly get the start time and other volume-based features
		for (long ip : MCC.getAnomalies().values()) {

			// Only if the ip is not excluded by evaluating the rules
			if (rulesManager.exclude(ip, tableName)) {
				logger.info("Excluding this IP " + ip + " / " + FeatureExtraction.longToIp(ip));
				continue;
			}

			long[] times = runSPC(ip, feature);

			// [ EWMA, improved EWMA, standard, wavelet]
			times = ArrayUtils.add(times, runWavelet(ip, feature));

			output.put(ip, times);

		}

		List<Anomaly> anomaliesList = OutputRepositoryHelper.createAnomalies(labelData, featureData, output,
				tableName, module);

		// Filter the list of anomalies according to the rules specified in the database
		anomaliesList = rulesManager.applyRules(anomaliesList, tableName);

		logger.info(anomaliesList);

		OutputRepository outputRepository;

		// Keep attempting to perform this task
		boolean keepTrying = true;
		int limit = Integer.parseInt(ConfigurationManager.getProperty("netflow.task.retries"));
		while (keepTrying){

			try {
				outputRepository = OutputRepository.getInstance();
				outputRepository.save(anomaliesList);
				keepTrying = false;

			} catch (SQLException e) {

				logger.error("Couldn't save the anomalies list in the analytics database, retrying " + retries, e);
				
				// Try again 
				if (retries < limit) {
					retries++;
					
					try {
						int mins = Integer.parseInt(ConfigurationManager.getProperty("netflow.task.waiting.min"));
						logger.info("Waiting for " + mins + " minutes");
						Thread.sleep(mins * 60000);
					} catch (NumberFormatException | InterruptedException e1) {
						logger.error("Waiting failed", e1);
					}

				} else {
					logger.error("Couldn't solve the error, rescheduling for next hour...");
					retries = 0;
					keepTrying = false;
				}
			}
		}

		retries = 0;

	}

	/**
	 * Load data for volume-based analysis to get the start time
	 * @param ip the required source IP
	 * @param type the type of the features extracted
	 * @return volume-based data
	 */
	private List<Tuple2<Integer, Double>> loadVolumeData(long ip, DataType type) {

		Dataset<Row> data = null;

		String table =  formatter.format(startTime).replace(" ", "");

		data = MainApplication.sqlConnection.option("dbtable", "(select st, pck, bytes from h1_" 
				+ table + " where sip = " + ip + " ) as volume_data").load();

		logger.info("The total number of rows in the data " + data.count());

		data = data.cache();

		// Get the volume features based on the type specified per min
		List<Tuple2<Integer, Double>> featureVector = FeatureExtraction
				.extractVolumeFeatures(data.toJavaRDD(), 0, type);

		logger.info("The number of points in the training set " + featureVector.size());
		logger.info("The first training point " + featureVector.get(0));

		return featureVector;

	}

	/**
	 * Run Statistical Process Control to get the start time
	 * @param ip the source ip
	 * @param type the feature of the volume-based analysis
	 * @return the an array of times depending on the chart type
	 */
	private long[] runSPC(long ip, DataType type) {
		
		logger.info("Starting SPC analysis...");

		// Extract the data
		List<Tuple2<Integer, Double>> featureVector = loadVolumeData(ip, type);

		// Get the total start time
		long[] stTime = {0, 0, 0};

		// If there is only one data point
		if (featureVector.size() < 2) return stTime;

		// Initialise the spc algorithm
		StatisticalProcessControl spc = new StatisticalProcessControl();

		Date[] anomalies;

		// Try the EMWA version
		anomalies = (Date[]) spc.computeEWMAChart(Integer.parseInt(ConfigurationManager
				.getProperty("netflow.volume.frame.size")), featureVector,
				Double.parseDouble(ConfigurationManager.getProperty("netflow.spc.lambda")),
				Double.parseDouble(ConfigurationManager.getProperty("netflow.spc.threshold")), false).get("Anomalies");

		// Add the start time to compute the average
		if (anomalies != null && anomalies.length != 0) {
			stTime[0] = anomalies[0].getTime()/1000;
		}

		anomalies = null;

		// Try improved version
		anomalies = (Date[]) spc.computeEWMAChart(Integer.parseInt(ConfigurationManager
				.getProperty("netflow.volume.frame.size")), featureVector,
				Double.parseDouble(ConfigurationManager.getProperty("netflow.spc.lambda")),
				Double.parseDouble(ConfigurationManager.getProperty("netflow.spc.threshold")), true).get("Anomalies");

		// Add the start time to compute the average
		if (anomalies != null && anomalies.length != 0) {
			stTime[1] = anomalies[0].getTime()/1000;
		}

		anomalies = null;

		// Try the standard version
		anomalies = (Date[]) spc.computeStandardChart(featureVector).get("Anomalies");

		// Add the start time to compute the average
		if (anomalies != null && anomalies.length != 0) {
			stTime[2] = anomalies[0].getTime()/1000;
		}
		
		logger.info("Finished SPC analysis");

		return stTime;

	}

	/**
	 * Run Wavelet analysis to get the start time of the anomaly
	 * @param ip the source ip required
	 * @param type the features extracted for the analysis
	 * @return the start time of the anomaly caused by that source ip
	 */
	private long runWavelet(long ip, DataType type) {
		
		logger.info("Starting wavelet analysis...");

		// Extract the data
		List<Tuple2<Integer, Double>> featureVector = loadVolumeData(ip, type);

		// If there is only one data point
		if (featureVector.size() < 2) return 0;

		// Instantiate the wavelet algorithm
		WaveletAnalysis wavelet = new WaveletAnalysis();

		Map<String, Object> output = wavelet.analyseSignal(featureVector,
				Double.parseDouble(ConfigurationManager.getProperty("netflow.wavelet.threshold")),
				Integer.parseInt(ConfigurationManager.getProperty("netflow.volume.frame.size")),
				Boolean.getBoolean(ConfigurationManager.getProperty("netflow.wavelet.window")));

		Date[] anomalies = (Date[]) output.get("Anomalies");

		if (anomalies == null || anomalies.length == 0) return 0;

		// Return the start time 
		return anomalies[0].getTime()/1000;

	}

	/**
	 * Update the current time and start and end times of the queried data from
	 * the net flow database to be used throughout
	 */
	private void updateTime() {

		// Get a calendar instance
		Calendar cal = GregorianCalendar.getInstance();

		// Get the hourly table and the required range
		int interval = Integer.parseInt(ConfigurationManager.getProperty("netflow.timer.interval.analysis.min"));

		// Get the current time
		endTime = new Date();
		cal.setTime(endTime);
		endHour = cal.get(Calendar.HOUR_OF_DAY);
		startTime = DateUtils.addMinutes(endTime, -interval);
		cal.setTime(startTime);
		startHour = cal.get(Calendar.HOUR_OF_DAY);


	}


	/**
	 * Load the required data from the database for network scan and port scan
	 * @param sql the name of the features to be queried from the database
	 * @param customQuery custom query to be specified
	 * @param table the name of the hourly table 
	 * @return The required data in a form of IP : values
	 */
	public static JavaPairRDD<Long, Vector> loadData(String sql, String customQuery, String table) {

		Dataset<Row> data = null;

		String query = "(select sip, " + sql + " from h1_" 
				+ table + " where sip between 167772160 and 184549375"
				+ " and sp != 80 group by sip ) as kmeans_data";

		if (customQuery != null) query = customQuery.replaceAll("\\$", table);

		data = MainApplication.sqlConnection.option("dbtable", query).load();

		logger.info("The total number of rows in the data " + data.count());
		logger.info("The first data point is " + data.first());

		// Set a label for the unsupervised data to be the source ip
		JavaPairRDD<Long, Vector> labeledFeatures = FeatureExtraction
				.transformToJavaPair(data.toJavaRDD(), 0, true);
		
		logger.info("Finished wavelet analysis");
		
		return labeledFeatures;

	}
	
	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {

		logger.info("Network Analysis is about to start...");

		// Update the time
		updateTime();

		// Run analysis each module
		for (MODULES module : MainApplication.modules) {
			// TODO add more modules
			// Get start time of the task
			long startTime = System.currentTimeMillis()/1000;

			// Keep attempting to perform this task
			boolean keepTrying = true;
			while (keepTrying){

				ScheduledFuture<?> task  = MainApplication.scheduledExecutorService.schedule(() -> runAnalysis(module), 0, TimeUnit.SECONDS);
				
				try {
					
					logger.info("About to run a task for module " + module.name() + " for the " + retries + " time");
					
					task.get();
					
					keepTrying = false;

				} catch (Exception e2) {

					logger.error("An uncaught exception while running the task retrying " + retries, e2);

					// Try again 
					if (retries < Integer.parseInt(ConfigurationManager.getProperty("netflow.task.retries"))) {

						retries++;

						try {
							int mins = Integer.parseInt(ConfigurationManager.getProperty("netflow.task.waiting.min"));
							logger.info("Waiting for " + mins + " minutes");
							Thread.sleep(mins * 60000);
						} catch (NumberFormatException | InterruptedException e1) {
							logger.error("Waiting failed", e1);
						}

					} else {
						logger.error("Couldn't solve the error, rescheduling for next hour...");
						retries = 0;
						keepTrying = false;
					}
				}
			}
			// Get the end time of the task
			long endTime = System.currentTimeMillis()/1000;

			// Try to save performance of the task
			OutputRepository outputRepository;
			try {
				outputRepository = OutputRepository.getInstance();
				outputRepository.save(new Task("hourly " + module.name(), startTime, endTime));
			} catch (SQLException e) {
				logger.error("Couldn't save this task in the analytics database", e);
			}

		}

		logger.info("Successfully saved in the database and the task is done");
		
	}

}
