/**
 * 
 */
package netflow.business.logic;

import java.util.List;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.sql.DataFrameReader;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.springframework.stereotype.Component;

import netflow.data.analysis.FeatureExtraction;
import netflow.data.analysis.KMeansClustering;
import netflow.data.analysis.StatisticalProcessControl;
import netflow.data.analysis.WaveletAnalysis;
import netflow.notification.FTPAlert;
import scala.Tuple2;

/**
 * @author Osman
 *
 */
@Component
public class TrafficFilter {

	public void filterFTP(DataFrameReader inputData, FTPAlert ftpAlert) {

		Dataset<Row> ftpConnections = inputData.option("dbtable",
				"(select * from h1_16061413 where app = '300003' limit 0, 15) as ftp_connections" ).load();

		ftpConnections.show();

		ftpAlert.sendAlert(ftpConnections);
	}

	public void analyseData(DataFrameReader inputData) {
		
		Dataset<Row> featureData = inputData.option("dbtable",
				"(select sip, dip, dp from d1_160613 Where sip BETWEEN '168000000' AND '169000000' limit 0, 1000000) as traffic" ).load();

		runClustering(featureData);
		
		Dataset<Row> volumeData = inputData.option("dbtable",
				"(select st, pck, bytes from h1_16061413 Where sip = '167885388' or dip = '167885388' limit 0, 100000) as traffic" ).load();
		
		runWaveletAnalysis(volumeData);

	}

	public void runClustering(Dataset<Row> sqlData) {
		
		sqlData.show();

		System.out.println("The total number of rows in the data " + sqlData.count());

		JavaRDD<Row> features = FeatureExtraction.getNetworkPostScanFeatures(sqlData.toJavaRDD());

		System.out.println("The number of points in the training set " + features.count());
		System.out.println("The first training point " + features.first());

		KMeansClustering kmeans = new KMeansClustering();

		kmeans.initialiseKMeans(FeatureExtraction.transformToJavaPair(features, 0, true), false);
	}
	
	public void runWaveletAnalysis(Dataset<Row> sqlData) {

		sqlData.show();

		System.out.println("The total number of rows in the data " + sqlData.count());
		
		WaveletAnalysis wavelet = new WaveletAnalysis();
		
		List<Tuple2<Integer, Double>> featureVector = FeatureExtraction.extractVolumeFeatures(sqlData.toJavaRDD(), 1,
				FeatureExtraction.DataType.VOLUME);
		
		System.out.println("The number of points in the training set " + featureVector.size());
		System.out.println("The first training point " + featureVector.get(0));
		
		wavelet.analyseSignal(featureVector, 5, 5, true);

	}
	
	public void runStatisticalProcessControl(Dataset<Row> sqlData) {
		
		sqlData.show();

		System.out.println("The total number of rows in the data " + sqlData.count());
		
		List<Tuple2<Integer, Double>> featureVector = FeatureExtraction.extractVolumeFeatures(sqlData.toJavaRDD(), 1,
				FeatureExtraction.DataType.VOLUME);
		
		System.out.println("The number of points in the training set " + featureVector.size());
		System.out.println("The first training point " + featureVector.get(0));
		
		StatisticalProcessControl SPC = new StatisticalProcessControl();
		
		SPC.computeEWMAChart(5, featureVector, 0.5, 3, false);
		
		SPC.computeStandardChart(featureVector);
		
	}

}
