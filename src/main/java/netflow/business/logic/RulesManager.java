/**
 * 
 */
package netflow.business.logic;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.spark.sql.DataFrameReader;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SQLContext;

import netflow.data.out.entities.Anomaly;

/**
 * This class is responsible of applying custom rules queried from the database
 * It loads all the rules from the database and apply them
 * Those rules range from simple filtering to specific classification
 * 
 * @author Osman
 *
 */
public class RulesManager {

	private DataFrameReader sqlConnection;
	private Row[] rules; // id, sip_range, custom_query, condition, compared_value, rule

	/**
	 * Default constructor
	 * Establish a connection to the database 
	 */
	public RulesManager() {

		// Initialise a database connection
		SQLContext sqlContext = new SQLContext(MainApplication.javaContext);

		Map<String, String> options = new HashMap<>();
		options.put("driver", ConfigurationManager.getProperty("netflow.nfx.driver"));
		options.put("url", "jdbc:mysql://" + ConfigurationManager.getProperty("netflow.output.database.url") + "?"
				+ "user=" + ConfigurationManager.getProperty("netflow.nfx.username") 
				+ "&password=" + ConfigurationManager.getProperty("netflow.nfx.password"));

		try {

			sqlConnection = sqlContext.read().format("jdbc").options(options);

			// Load all rules
			rules = (Row[]) sqlConnection.option("dbtable", "(select * from rules) as sql_connections").load().collect();

		} catch (Exception e) {

			e.printStackTrace();

			return;
		}

	}

	/**
	 * Do you exclude this IP because of a rule
	 * @param sip the source ip to check the rule agains
	 * @param tableName the name of the hourly table
	 * @return whether it should be excluded
	 */
	public boolean exclude(long sip, String tableName) {
		
		// evaluate the condition
		boolean condition = false;

		// For each rule
		for (Row rule : rules) {
			
			String ruleStr = rule.getString(5).replace(" ", "");
			
			if (ruleStr.equals("exclude")) {
				
				condition = true;
				
				long startIP = 167772160L;
				long endIP = 184549375L;

				// Get the parameters from the rule
				// First get the range of source ips and remove all spaces
				String range = rule.getString(1).replace(" ", "");
				String query = rule.getString(2);
				String conditionStr = rule.getString(3).replace(" ", "");
				int compared_value = rule.getInt(4);
				
				// If star * it means all ips
				if (!range.equals("*")) {

					// For each ip range i.e. 168034560-168103845
					for (String ipRange : range.split(",")) {

						// Get start and end ips
						String[] ips = ipRange.split("-");
						startIP = Long.parseLong(ips[0]);
						endIP = startIP;
						if (ips.length > 1) endIP = Long.parseLong(ips[1]);
					}
				}

				// If it's in the range
				if (sip >= startIP && sip <= endIP) {

					// Now apply the query to get the number
					if (query != null && !query.equals("")) {

						// Replace the $ with the IP
						String query_ = query.replaceAll("\\$", "" + sip);

						// Replace the % with table name
						query_ = query_.replaceAll("\\%", tableName);

						int value = 0;

						Dataset<Row> val = MainApplication.sqlConnection
								.option("dbtable", "(" + query_ + ") as sql_connections").load();

						if (val.count() == 1) {
							Object obj = val.first().get(0);
							if (obj instanceof Integer) value = (int) obj;
							if (obj instanceof Long) value = new Long((long) obj).intValue();
							if (obj instanceof BigDecimal) value = ((BigDecimal) obj).intValue();
							if (obj instanceof Float) value = new Float((float) obj).intValue();
						} else {
							condition = false;
							continue;
						}

						condition = compare(conditionStr, value, compared_value);
						if(condition) return condition;
					}
				}
			}
		}
		return condition;
	}

	/**
	 * Simple Utility method to evaluate a comparison expression as a String 
	 * @param conditionStr the expression as String
	 * @param value to be compared
	 * @param compared_value to be compared against
	 * @return a result of the comparsion
	 */
	private static boolean compare(String conditionStr, int value, int compared_value) {

		boolean condition = false;

		switch(conditionStr) {
		case "==":
			condition = value == compared_value;
			break;
		case ">=":
			condition = value >= compared_value;
			break;
		case "<=":
			condition = value <= compared_value;
			break;
		case "!=":
			condition = value <= compared_value;
			break;

		}

		return condition;
	}

	/**
	 * Apply the set of rules loading from the database that follow the ruling convention as follows;
	 * <p><b>id</b> should be unique for each rule</p>
	 * <p><b>sip_range</b> the range of source ips, * means all and is each range is separated by comma
	 * and within the range a hyphen (-) is required with no spaces</p>
	 * <p><b>query</b> a complete sql query to be applied, where $ indicates
	 * the range of ips and % indicates the table that outputs a number</p>
	 * <p><b>condition</b> the condition to apply to the number queried [==, &lt;=, \&gt;=, !=]</p>
	 * <p><b>compared_value</b> the value to compare the number with using the condition</p>
	 * <p><b>rule</b> the specific rule regarding all the ips that satisfy the condition:
	 * ['exclude', '{column_name} : {replaced_value}, {column_name} : {replaced_value}' </p>
	 *  
	 * @param data the list of anomalies to be filtered
	 * @param tableName the name of the hourly table
	 * @return the filtered list
	 */
	public List<Anomaly> applyRules(List<Anomaly> data, String tableName) {

		List<Anomaly> filteredData = new ArrayList<>();

		// If there is no rule, return the original data
		if (rules == null || rules.length == 0)  return data;

		// For each rule
		for (Row rule : rules) {

			String ruleStr = rule.getString(5).replace(" ", "");
			
			if (ruleStr.equals("exclude")) {
				continue;
			}
			
			long startIP = 167772160L;
			long endIP = 184549375L;

			// Get the parameters from the rule
			// First get the range of source ips and remove all spaces
			String range = rule.getString(1).replace(" ", "");
			String query = rule.getString(2);
			String conditionStr = rule.getString(3).replace(" ", "");
			int compared_value = rule.getInt(4);

			// If star * it means all ips
			if (!range.equals("*")) {

				// For each ip range i.e. 168034560-168103845
				for (String ipRange : range.split(",")) {

					// Get start and end ips
					String[] ips = ipRange.split("-");
					startIP = Long.parseLong(ips[0]);
					endIP = startIP;
					if (ips.length > 1) endIP = Long.parseLong(ips[1]);
				}
			}
			// Filter the required IP range and apply the rules
			for (Anomaly anomaly : data) {

				boolean add2Array = true;

				long sip = anomaly.getSip();

				// If it's in the range
				if (sip >= startIP && sip <= endIP) {

					// evaluate the condition
					boolean condition = true;

					// Now apply the query to get the number
					if (query != null && !query.equals("")) {

						// Replace the $ with the IP
						String query_ = query.replaceAll("\\$", "" + sip);

						// Replace the % with table name
						query_ = query_.replaceAll("\\%", tableName);

						int value = 0;

						Dataset<Row> val = MainApplication.sqlConnection
								.option("dbtable", "(" + query_ + ") as sql_connections").load();

						if (val.count() == 1) {
							Object obj = val.first().get(0);
							if (obj instanceof Integer) value = (int) obj;
							if (obj instanceof Long) value = new Long((long) obj).intValue();
							if (obj instanceof BigDecimal) value = ((BigDecimal) obj).intValue();
							if (obj instanceof Float) value = new Float((float) obj).intValue();
						}

						condition = compare(conditionStr, value, compared_value);

					}

					// Now evaluate the rule
					if (condition) {

						// Replace values according to the column names
						for (String pair : ruleStr.split(",")) {

							String colName = pair.split(":")[0];
							String val = pair.split(":")[1];

							switch (colName) {
							case "type":
								anomaly.setType(val);
								break;
							case "status":
								anomaly.setStatus(val);
								break;
							}
						}
					}
				}

				// If not excluded add it to the new array
				if (add2Array) filteredData.add(anomaly);
			}			
		}

		return filteredData;
	}

	/**
	 * Update the current set of rules by checking the database
	 */
	public void updateRules() {

		// Load all rules
		rules = (Row[]) sqlConnection.option("dbtable", "(select * from rules) as sql_connections" ).load().collect();
	}



}
