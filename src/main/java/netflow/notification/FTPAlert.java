/**
 * 
 */
package netflow.notification;

import java.io.File;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.commons.lang3.time.DateUtils;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.FilterFunction;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.apache.spark.storage.StorageLevel;
import org.springframework.stereotype.Component;

import netflow.data.in.DataProcessing;
import netflow.data.in.DumpData;

/**
 * 
 * FTP Alerting System that filters FTP traffic from a certain IP ranges
 * and alert it to the support team
 * @author Osman
 *
 */

@Component
public class FTPAlert implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = 7512475727019187900L;
	private static final String path = new File("./resource/data.rdd").getAbsolutePath();

	/**
	 * A method that will update the Ftp traffic stored locally 
	 * If it has been more than a week for IP it will be deleted
	 */
	@SuppressWarnings("serial")
	public void updateFtp() {

		Dataset<Row> data = loadStoredData().cache();
		
		final Date lastWeek = DateUtils.addDays(new Date(), -7);
		
		data.show();
		
		if (data != null) {
			
			Dataset<Row> filteredData = data.filter(new FilterFunction<Row>() {

				@Override
				public boolean call(Row value) throws Exception {
					
					// Valid only if the start time is greater/more recent than last week
					return value.getLong(0) > (lastWeek.getTime()/1000);
				}
				
			});
			
			filteredData.show();
			
			// Delete the old data as HDFS can not overwrite
			DataProcessing.deleteDirectory(new File(path));
			
			filteredData.toJavaRDD().coalesce(1).saveAsTextFile(path);
			
		}

	}

	/**
	 * Load the stored data in the resource folder
	 * @return
	 */
	@SuppressWarnings("serial")
	private Dataset<Row> loadStoredData() {

		Dataset<Row> sendData = null;

		if(Files.exists(Paths.get(path))) {

			JavaRDD<Row> dataRDD = DumpData.javaContext.textFile(path).map(new Function<String, Row>() {

				public Row call(String line) throws Exception {
					String[] parts = line.split(",");

					Row row = RowFactory.create(
							Long.parseLong(parts[0].substring(1)),
							Long.parseLong(parts[1]),
							Long.parseLong(parts[2]),
							Long.parseLong(parts[3]),
							Long.parseLong(parts[4]),
							Long.parseLong(parts[5]),
							Long.parseLong(parts[6]),
							Long.parseLong(parts[7]),
							Long.parseLong(parts[8]),
							Long.parseLong(parts[9]),
							Long.parseLong(parts[10]),
							Long.parseLong(parts[11].substring(0, parts[11].length()-1)) );

					return row;
				}
			});

			// The schema is encoded in a string
			String schemaString = "st bytes pck sip dip proto app sp dp nh sif dif";

			// Generate the schema based on the string of schema
			List<StructField> fields = new ArrayList<StructField>();
			for (String fieldName: schemaString.split(" ")) {
				fields.add(DataTypes.createStructField(fieldName, DataTypes.LongType, true));
			}
			StructType schema = DataTypes.createStructType(fields);

			SQLContext sqlc = new SQLContext(DumpData.javaContext);

			sendData = sqlc.createDataFrame(dataRDD, schema);

		}

		return sendData;

	}

	/**
	 * Send an alert of the new ftp traffic and update the stored data
	 * @param data to be sent to support
	 */
	public void sendAlert(Dataset<Row> data) {


		Dataset<Row> sendData = data;

		Dataset<Row> set = loadStoredData(); 
		if (set != null) {
			sendData = data.except(set);
			data = sendData.union(set).persist(StorageLevel.MEMORY_ONLY());
		}

		if (sendData.count() == 0) {
			return;
		}

		sendData = sendData.cache();

		sendEmail(sendData);
		
		data.first();

		if (DataProcessing.deleteDirectory(new File(path))) {
			System.out.println("The file has been deleted successfully");
		}
		
		data.toJavaRDD().coalesce(1).saveAsTextFile(path);
	}

	/**
	 * Send email using Outlook from my eseye email to my personal email for testing
	 * @param data
	 */
	private void sendEmail(Dataset<Row> data) {
		// Recipient's email ID needs to be mentioned.
		String to = "o.dawelbeit@gmail.com";

		// Sender's email ID needs to be mentioned
		String from = "oramadan@eseye.com";

		// Assuming you are sending email from my outlook
		String host = "outlook.office365.com";

		// Setting up Username and Passeword
		final String username = "oramadan@eseye.com";
		final String password = "Koga8264";

		// Get system properties
		Properties properties = System.getProperties();

		// Setup mail server
		properties.setProperty("mail.smtp.host", host);
		properties.setProperty("mail.smtp.auth", "true");
		properties.setProperty("mail.smtp.starttls.enable", "true");
		properties.setProperty("mail.smtp.port", "587");

		// Get the default Session object.
		Session session = Session.getInstance(properties,
				new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});
		try{
			// Create a default MimeMessage object.
			MimeMessage message = new MimeMessage(session);

			// Set From: header field of the header.
			message.setFrom(new InternetAddress(from));

			// Set To: header field of the header.
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));

			// Set Subject: header field
			message.setSubject("FTP Daily Reports3!");

			String[] colNames = data.columns();

			System.out.println("The number of rows are " + data.count());

			// Convert to a html table ------------------------------------------------------------
			String messageText = convertToHtml(colNames, data.collectAsList());
			//--------------------------------------------------------------------------------------------

			// Send the actual HTML message, as big as you like
			message.setContent(messageText, "text/html" );

			// Send message
			Transport.send(message);
			System.out.println("Sent message successfully....");
		}catch (MessagingException mex) {
			mex.printStackTrace();
		}
	}

	/**
	 * Convert the tables of the ftp traffic to a HTML text
	 * @param colNames
	 * @param rows
	 * @return
	 */
	private String convertToHtml(String[] colNames, List<Row> rows) {
		String messageText = "<table style=\"width:100%; border:1px solid black; border-collapse:collapse;\">";

		messageText += "<tr>";
		for (String colName : colNames) {
			messageText += "<th style=\"border:1px solid black; border-collapse:collapse;"
					+ "padding: 5px\">" + colName + "</th>";
		}
		messageText += "</tr>";

		for (Row row : rows) {
			messageText += "<tr>";
			for (int i = 0; i < row.length(); i++) {
				messageText += "<td style=\"border:1px solid black; border-collapse:collapse;"
						+ "padding: 5px\">";
				messageText += row.get(i);
				messageText += "</td>";
			}
			messageText += "</tr>";
		}

		messageText += "</table>";

		return messageText;
	}
}
