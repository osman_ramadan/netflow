/**
 * 
 */
package ftp.alert.system;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.commons.lang3.time.DateUtils;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.sql.DataFrameReader;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.apache.spark.storage.StorageLevel;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.impl.StdSchedulerFactory;


/**
 * 
 * FTP Alerting System that filters FTP traffic from a certain IP ranges
 * and alert it to the support team
 * @author Osman
 *
 */

public class FTPAlert implements Serializable,  ServletContextListener {

	private static Logger logger = Logger.getLogger(FTPAlert.class);
	private static JavaSparkContext javaContext;
	private static DataFrameReader sqlConnection;
	private static final long serialVersionUID = 7512475727019187900L;
	private static final File path = new File("data2");
	private static final File original_path = new File("data");
	private static final String MYSQL_DRIVER = "com.mysql.jdbc.Driver";
	private static final SimpleDateFormat formatter = new SimpleDateFormat("yy MM dd HH");


	/**
	 * Query the ftp traffic and filter them
	 */
	public static void filterFtp() {

		// Get a the hourly table of last hour
		String table =  formatter.format(DateUtils.addHours(new Date(), -1)).replace(" ", "");
		
		// Query the table from the database for the specified range of ips
		Dataset<Row> ftpConnections = sqlConnection.option("dbtable", 
				"(select * from h1_"+ table + " where app = '30003' and (sip between "
						+ "184062976 and 184063999 or dip between "
						+ "184062976 and 184063999) limit 0, 100000) as ftp_connections" ).load();

		logger.info("Sending... Alert");

		// Copy the stored (alerted ips) in a temporary path until the filtering is done
		try {
			copyFolder(original_path, path);
		} catch (IOException e) {
			logger.error("Copying files error", e);
		}
		
		// Delete the original path
		deleteDirectory(original_path);
		
		// Send alert
		sendAlert(ftpConnections);

	}




	/**
	 * A method that will update the Ftp traffic stored locally 
	 * If it has been more than a week for IP it will be deleted
	 */
	public static void updateFtp() {

		// Copy the stored (alerted ips) in a temporary path until the filtering is done
		try {
			copyFolder(original_path, path);
		} catch (IOException e) {
			logger.error("Copying files error", e);
		}

		// Delete the original path
		deleteDirectory(original_path);
		
		// Load an stored data from the temporary path
		Dataset<Row> data = loadStoredData();
		
		// If no stored data retrun back
		if (data == null) return;
		
		// cache data in the memory for processing
		data = data.cache();

		// Get the date of last week
		final Date lastWeek = DateUtils.addDays(new Date(), -7);
		
		// Show a sample of the data in the logs
		data.show();

		if (data != null) {

			// Filter so that a row is valid only if the start time is greater/more recent than last week
			Dataset<Row> filteredData = data.filter((Row row) -> row.getLong(0) > (lastWeek.getTime()/1000));
			
			// Show a sample of the filteredData in the logs
			filteredData.show();

			// Save the processed in the original path
			filteredData.toJavaRDD().coalesce(1).saveAsTextFile(original_path.getAbsolutePath());

			// Delete the old data as HDFS can not overwrite
			deleteDirectory(new File(path.getAbsolutePath()));

		}

	}


	/**
	 * Load the stored data in the resource folder
	 * @return
	 */
	@SuppressWarnings("serial")
	private static Dataset<Row> loadStoredData() {

		Dataset<Row> sendData = null;

		if(Files.exists(Paths.get(path.getAbsolutePath()))) {

			JavaRDD<Row> dataRDD = javaContext.textFile(path.getAbsolutePath()).map(new Function<String, Row>() {

				public Row call(String line) throws Exception {
					String[] parts = line.split(",");

					Row row = RowFactory.create(
							Long.parseLong(parts[0].substring(1)),
							Long.parseLong(parts[1]),
							Long.parseLong(parts[2]),
							Long.parseLong(parts[3]),
							Long.parseLong(parts[4]),
							Long.parseLong(parts[5]),
							Long.parseLong(parts[6]),
							Long.parseLong(parts[7]),
							Long.parseLong(parts[8]),
							Long.parseLong(parts[9]),
							Long.parseLong(parts[10]),
							Long.parseLong(parts[11].substring(0, parts[11].length()-1)) );

					return row;
				}
			});

			sendData = createDataFrame(dataRDD);

		}

		return sendData;

	}

	/**
	 * Convert JavaRDD to Dataset by adding a schema
	 * @param data
	 * @return
	 */
	private static Dataset<Row> createDataFrame (JavaRDD<Row> data) {

		// The schema is encoded in a string
		String schemaString = "st bytes pck sip dip proto app sp dp nh sif dif";

		// Generate the schema based on the string of schema
		List<StructField> fields = new ArrayList<StructField>();
		for (String fieldName: schemaString.split(" ")) {
			fields.add(DataTypes.createStructField(fieldName, DataTypes.LongType, true));
		}
		StructType schema = DataTypes.createStructType(fields);

		SQLContext sqlc = new SQLContext(javaContext);

		return sqlc.createDataFrame(data, schema);

	}

	/**
	 * Send an alert of the new ftp traffic and update the stored data
	 * @param data to be sent to support
	 */
	public static void sendAlert(Dataset<Row> data) {

		Dataset<Row> sendData = data;

		logger.info("The number of data before filtering " + data.count());

		Dataset<Row> set = loadStoredData(); 
		
		if (set != null) {
			// Get the list of source ips
			final List<Long> sips = set.toJavaRDD().map((Row row) -> (long) row.get(3)).collect();
			
			// Convert all the elements in the data set to type long 
			JavaRDD<Row> rddData = data.toJavaRDD().map((Row row) -> RowFactory.create(
					new Long(new Long((int) row.get(0))), ((BigDecimal) row.get(1)).longValue(),
					((BigDecimal) row.get(2)).longValue(), (long) row.get(3), (long) row.get(4),
					new Long((int) row.get(5)), (long) row.get(6),
					new Long((int) row.get(7)), new Long((int) row.get(8)), (long) row.get(9),
					new Long((int) row.get(10)), new Long((int) row.get(11))));
			
			// Filter the data so as not to send a traffic for an ip that has been already alerted in the past week
			sendData = createDataFrame(rddData.filter((Row row) -> !sips.contains((Long) row.get(3))
					&& !sips.contains((Long) row.get(4))));
			
			// Add the new traffic to the data that is stored locally
			data = sendData.union(set).persist(StorageLevel.MEMORY_ONLY());
		}

		logger.info("The number of send data " + sendData.count());

		logger.info("The number of data after filtering " + data.count());
		
		// If no data to send return
		if (sendData.count() == 0) {
			return;
		}

		// cache sendData in memory
		sendData = sendData.cache();
		
		// send email
		sendEmail(sendData);
		
		// This just to load the data after performing union operation
		data.first();

		// Save the new alerted ip address locally
		data.toJavaRDD().coalesce(1).saveAsTextFile(original_path.getAbsolutePath());
		
		// Delete the temporary directory when done
		deleteDirectory(new File(path.getAbsolutePath()));
	}

	@SuppressWarnings("unused")
	private static void saveAsTextFile(JavaRDD<Row> data) {
		List<String> lines = data.map((Row row) -> row.toString()).collect();
		try {
			Files.write(path.toPath(), lines, Charset.forName("UTF-8"));
		} catch (IOException e) {
			logger.error("Saving data error", e);
		}
	}

	/**
	 * Send email using Outlook from my eseye email to my personal email for testing
	 * @param data
	 */
	private static void sendEmail(Dataset<Row> data) {
		// Recipient's email ID needs to be mentioned.
		String to = "support@eseye.com";
		
		// Sender's email ID needs to be mentioned
		String from = "oramadan@eseye.com";

		// Assuming you are sending email from my outlook
		String host = "192.168.243.12";

		// Setting up Username and Passeword
		//final String username = "oramadan@eseye.com";
		//final String password = "Koga8264";

		// Get system properties
		Properties properties = System.getProperties();

		// Setup mail server
		properties.setProperty("mail.smtp.host", host);
		properties.setProperty("mail.smtp.port", "25");

		// Get the default Session object.
		Session session = Session.getInstance(properties, null);
		try{
			// Create a default MimeMessage object.
			MimeMessage message = new MimeMessage(session);

			// Set From: header field of the header.
			message.setFrom(new InternetAddress(from));

			// Set To: header field of the header.
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));

			// Set Subject: header field
			message.setSubject("FTP Hourly Reports!");

			String[] colNames = data.columns();

			logger.info("The number of rows are " + data.count());

			// Convert to a html table ------------------------------------------------------------
			String messageText = convertToHtml(colNames, data.collectAsList());
			//--------------------------------------------------------------------------------------------

			// Send the actual HTML message, as big as you like
			message.setContent(messageText, "text/html" );

			// Send message
			Transport.send(message);
			logger.info("Sent message successfully....");
		}catch (MessagingException mex) {
			logger.error("Sending message failed", mex);;
		}
	}

	/**
	 * Force deletion of directory
	 * @param path
	 * @return
	 */
	private static boolean deleteDirectory(File path) {
		if (path.exists()) {
			File[] files = path.listFiles();
			for (int i = 0; i < files.length; i++) {
				if (files[i].isDirectory()) {
					deleteDirectory(files[i]);
				} else {
					files[i].delete();
				}
			}
		}
		return (path.delete());
	}


	private static void copyFolder(File src, File dest) throws IOException{

		if(src.isDirectory()){

			//if directory not exists, create it
			if(!dest.exists()){
				dest.mkdir();
				logger.info("Directory copied from " 
						+ src + "  to " + dest);
			}

			//list all the directory contents
			String files[] = src.list();

			for (String file : files) {
				//construct the src and dest file structure
				File srcFile = new File(src, file);
				File destFile = new File(dest, file);
				//recursive copy
				copyFolder(srcFile,destFile);
			}

		}else{
			//if file, then copy it
			//Use bytes stream to support all file types
			InputStream in = new FileInputStream(src);
			OutputStream out = new FileOutputStream(dest); 

			byte[] buffer = new byte[1024];

			int length;
			//copy the file content in bytes 
			while ((length = in.read(buffer)) > 0){
				out.write(buffer, 0, length);
			}

			in.close();
			out.close();
			logger.info("File copied from " + src + " to " + dest);
		}
	}


	/**
	 * Convert the tables of the ftp traffic to a HTML text
	 * @param colNames
	 * @param rows
	 * @return
	 */
	private static String convertToHtml(String[] colNames, List<Row> rows) {
		String messageText = "<table style=\"width:100%; border:1px solid black; border-collapse:collapse;\">";

		messageText += "<tr>";
		for (String colName : colNames) {
			messageText += "<th style=\"border:1px solid black; border-collapse:collapse;"
					+ "padding: 5px\">" + colName + "</th>";
		}
		messageText += "</tr>";

		for (Row row : rows) {
			messageText += "<tr>";
			for (int i = 0; i < row.length(); i++) {
				messageText += "<td style=\"border:1px solid black; border-collapse:collapse;"
						+ "padding: 5px\">";
				messageText += row.get(i);
				messageText += "</td>";
			}
			messageText += "</tr>";
		}

		messageText += "</table>";

		return messageText;
	}

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		logger.info("class : context destroyed");

	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {

		javaContext = new JavaSparkContext(new SparkConf()
				.setAppName("Netflow").setMaster("local[*]"));

		File save_path = new File("login.txt");

		logger.info(save_path.getAbsolutePath());

		List<String> userDetails = null;;
		try {
			userDetails = Files.readAllLines(save_path.toPath());

		} catch (IOException e) {
			logger.error("Saving login details failed", e);
		}

		SQLContext sqlContext = new SQLContext(javaContext);

		String MYSQL_CONNECTION_URL = "jdbc:mysql://" + userDetails.get(2) + "?"
				+ "user=" + userDetails.get(0) + "&password=" + userDetails.get(1);
		Map<String, String> options = new HashMap<>();
		options.put("driver", MYSQL_DRIVER);
		options.put("url", MYSQL_CONNECTION_URL);

		sqlConnection = sqlContext.read().format("jdbc").options(options);

		try {
			// Grab the Scheduler instance from the Factory
			Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();

			// and start it off
			scheduler.start();

			scheduler.triggerJob(JobKey.jobKey("FliterJob"));


		} catch (SchedulerException se) {
			logger.error("Initializing Quartz Scheduler error", se);
		}

		logger.info("FTP alerting system : context Initialized");

	}
}

