/**
 * 
 */
package ftp.alert.system;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * @author Osman
 *
 */
public class SchedulerJob implements Job {

	/* (non-Javadoc)
	 * @see org.quartz.Job#execute(org.quartz.JobExecutionContext)
	 */
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {

		FTPAlert.updateFtp();
		FTPAlert.filterFtp();

	}

}
