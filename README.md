#Netflow Analyser#

Netflow Analyser is a toolbox of machine learning and data analysis algorithms to detect network anomalies and classify them.
Netflow Analyser works on Apache Spark engine for large-scale data processing and distributed computing. The framework is centred on a data structure called the Resilient Distributed Dataset (RDD), a read-only multiset of data items distributed over a cluster of machines, that is maintained in a fault-tolerant way.

### Introduction ###

* Feature-based algorithms:
Clustering to detect data anomalies -
Dimensionality Reduction to reduce the number of features and only focus on important ones -
Entropy based on information theory to identify good clustering from bad one -
Multiclass Logsitic regression model to allow live classification and anomaly detection
* Volume-based algorithms:
Wavelet analysis using Fast Wavelet Transform to detect spikes and sudden changes to the volume of data -
Statistical Process Control using various control charts to detect large usage that does not correspond to seasonal changes
* Version 1.0.1


### Installation ###

Netflow Analyser requires Java v7+ to run.
To build the application from the source code, Eclipse or Maven is required